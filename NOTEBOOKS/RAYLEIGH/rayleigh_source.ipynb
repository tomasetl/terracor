{
 "cells": [
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "45e2793e",
   "metadata": {},
   "outputs": [],
   "source": [
    "## Distributed python packages\n",
    "import os\n",
    "import numpy as np\n",
    "import matplotlib.pyplot as plt\n",
    "import cartopy.crs as ccrs\n",
    "import xarray as xr\n",
    "\n",
    "from math import radians, log\n",
    "from sys import exit\n",
    "\n",
    "##Own librairies\n",
    "from readWW31 import read_dpt\n",
    "from source_microseism import loop_SDF, download_ww3_local, open_bathy\n",
    "\n",
    "__author__ = \"Lisa Tomasetto\"\n",
    "__copyright__ = \"Copyright 2024, UGA\"\n",
    "__credits__ = [\"Lisa Tomasetto\"]\n",
    "__version__ = \"2.0\"\n",
    "__maintainer__ = \"Lisa Tomasetto\"\n",
    "__email__ = \"lisa.tomasetto@univ-grenoble-alpes.fr\"\n",
    "__status__ = \"Production\""
   ]
  },
  {
   "cell_type": "markdown",
   "id": "dba86927",
   "metadata": {},
   "source": [
    "# Rayleigh Source of microseisms\n",
    "\n",
    "This program aims at modelizing the ambient noise source in the secondary microseismic range for Rayleigh waves.\n",
    "It is based on Ardhuin et al. (2011) article.\n",
    "We will compute the equivalent source for **the power spectrum of the vertical displacement** $S_{DF} \\text{ in m.s}$.\n",
    "\n",
    "$ S_{DF}(f_s) \\approx \\frac{2\\pi f_s C}{\\rho_s^2 \\beta^5} F_{p3D}(k_2 \\approx 0, f_s)$\n",
    "\n",
    "where:\n",
    "- $f_s$ is the seismic frequency in Hz (twice the ocean wave frequency)\n",
    "- C is the amplitude response functions for the normal modes, from Longuet-Higgins (1950). $C = \\sum_{i=1}^{4} c_i^2$\n",
    "- $\\rho_s$ is the rock density of the crust. Here $\\rho_s = 2830 \\text{ kg.m}^{-3}$\n",
    "- $\\beta$ is the shear wave velocity. Here $\\beta = 2.8 \\text{ km.s}^{-1}$\n",
    "- $F_{p3D}(k_2 \\approx 0, f_s)$ the spectral density of the pressure field at the ocean surface or directional wave spectra in $\\text{Pa}^2.\\text{m}^2.\\text{s}.$\n",
    "\n",
    "In our case Rayleigh waves are dominant in the periodic band from 5s to 12s so we will integrate this over the corresponding frequency band.\n",
    "\n",
    "$ S_{DF} = \\int_{0.083}^{0.200} S_{DF}(f_s)  df_s $\n",
    "\n",
    "The values of the Rayleigh waves site effect coefficient can be found in Longuet-Higgins (1950), the values are tabulated in longuet_higgins.txt file.\n",
    "The $F_{p3D}$ can be retrieved using oceanic waves model resources from the Ifremer institute in Brest, France. We provide a cell downloading the files directly. \n",
    "\n",
    "References: \n",
    "* Ardhuin, F., Stutzmann, E., Schimmel, M., & Mangeney, A. (2011). Ocean wave sources of seismic noise. Journal of Geophysical Research: Oceans, 116(C9).\n",
    "* Longuet-Higgins, M. S. (1950). A theory of the origin of microseisms. Philosophical Transactions of the Royal Society of London. Series A, Mathematical and Physical Sciences, 243(857), 1-35.\n",
    "* The WAVEWATCH III® Development Group (WW3DG), 2019: User manual and system documentation of WAVEWATCH III® version 6.07. Tech. Note 333, NOAA/NWS/NCEP/MMAB, College Park, MD, USA, 326 pp. + Appendices."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "6fc8fc63",
   "metadata": {},
   "source": [
    "## Parameters\n",
    "\n",
    "This implementation is depending on a few parameters. Some are described above in the formula of Rayleigh wave noise source but others are for the user to choose."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "c87fec42",
   "metadata": {},
   "source": [
    "### Physical Constants\n",
    "Default values are given below. Please change with your own values if needed. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "06d5dbc0",
   "metadata": {},
   "outputs": [],
   "source": [
    "# seismic waves\n",
    "VS_CRUST= 2.8  # km/s\n",
    "RHO_S = 2830  # kg/m3\n",
    "F1 = 1/12  # frequency to integrate from\n",
    "F2 = 1/2  # frequency to integrate to\n",
    "#p1 = \n",
    "#p2 = \n",
    "parameters = [VS_CRUST, RHO_S, F1, F2]"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "7d04d5cf",
   "metadata": {},
   "source": [
    "### Dates\n",
    "Then the dates the user wants to focus on, loops on hours, days, months and years are available setting an empty bracket symbol '[]'."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "9c0e2bc1",
   "metadata": {},
   "outputs": [],
   "source": [
    "# dates\n",
    "YEAR = 2008  # year of interest\n",
    "MONTH = [3]  # loop if array, compute all months of the year if empty list []\n",
    "DAY = np.arange(8, 19)  # loop if array, compute all days of the month if empty list []\n",
    "HOUR = []  # loop if array, compute every 3 hours of the day if empty list []\n",
    "\n",
    "date_vec = [YEAR, MONTH, DAY, HOUR]"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "bcac7c46",
   "metadata": {},
   "source": [
    "### Spatial Extent"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "a67cf49b",
   "metadata": {},
   "outputs": [],
   "source": [
    "# extent\n",
    "lat_min = -78 # -78 min\n",
    "lat_max = 80 # 80 max\n",
    "lon_min = -180 # -180 min\n",
    "lon_max = 180 # 180 max\n",
    "\n",
    "extent = [lon_min, lon_max, lat_min, lat_max]"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "62a307a5",
   "metadata": {},
   "source": [
    "## Additional Files\n",
    "See detailed information in cells below.\n",
    "### Paths to Files"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "3be747cb",
   "metadata": {},
   "outputs": [],
   "source": [
    "# ftp path of WW3 data\n",
    "ftp_path_to_files = \"ftp://ftp.ifremer.fr/ifremer/dataref/ww3/GLOBMULTI_ERA5_GLOBCUR_01/GLOB-30M/%d/FIELD_NC/\"%YEAR\n",
    "\n",
    "# local path for WW3 data\n",
    "ww3_local_path = \"./data/%d\"%YEAR  # path where the data will be downloaded\n",
    "\n",
    "# Longuet-Higgins site effect coefficients\n",
    "longuet_higgins_file = './longuet_higgins.txt'\n",
    "\n",
    "# bathymetry default\n",
    "file_bathy = \"./ww3.07121700.dpt\"  #0.5x0.5 degree grid bathymetry\n",
    "\n",
    "paths = [file_bathy, ww3_local_path, longuet_higgins_file]"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "bd74efef",
   "metadata": {},
   "source": [
    "## Download WW3 Files\n",
    "For the model files go to ftp://ftp.ifremer.fr/ifremer/dataref/ww3/GLOBMULTI_ERA5_GLOBCUR_01/GLOB-30M/, then choose the year(s) and month(s) corresponding files. Two files are downloaded, the significant waveheight file, extension hs.nc (or no extension) and the directional wave spectra file, extension p2l.nc (for both). "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "633c073c",
   "metadata": {},
   "outputs": [],
   "source": [
    "download_ww3_local(YEAR, MONTH, ftp_path_to_files, ww3_local_path)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "e23c3f71",
   "metadata": {},
   "source": [
    "## Bathymetry file \n",
    "   The bathymetry (or waterlevel) is necessary to compute the site effect for a given phase.\n",
    "\n",
    "Two bathymetry grids are available with this notebook: \n",
    "- (default) \"ww3.07121700.dpt\": a 0.5°x0.5° bathymetry file corresponding to WW3 hindcast resolution.\n",
    "\n",
    "- (```refined_bathymetry = True```) a 1 arcmin resolution ETOPOv2 bathymetry netcdf file.\n",
    "\n",
    "==> Available here: https://www.ngdc.noaa.gov/thredds/catalog/global/ETOPO2022/60s/60s_bed_elev_netcdf/catalog.html?dataset=globalDatasetScan/ETOPO2022/60s/60s_bed_elev_netcdf/ETOPO_2022_v1_60s_N90W180_bed.nc\n",
    "- [WARNING] use this refined bathymetry on small grids otherwise memory errors might occur (typically 30° lat x 30° lon)\n",
    "\n",
    "If you wish to use your own bathymetry file:\n",
    "- latitude in ° 1D array should be named ```zlat```.\n",
    "- longitude in ° 1D array should be named ```zlon```.\n",
    "- depth in meters 2D grid should be named ```dpt1```."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "933f2f93",
   "metadata": {},
   "outputs": [],
   "source": [
    "dpt1, zlon, zlat = open_bathy(file_bathy, refined_bathymetry=False, extent=extent)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "42f85f36",
   "metadata": {},
   "source": [
    "### Save and Plot\n",
    "If you want to save the 3-hourly matrix as a netcdf file set the save variable to True.\n",
    "```save = True ```\n",
    "\n",
    "If you want to plot the maps and save them as .png files set the plot_type variable to:\n",
    "* ```plot_type = 'hourly'``` for plots every 3-hours (WW3 resolution)\n",
    "* ```plot_type = 'daily'``` for summed daily plots\n",
    "* ```plot_type = 'monthly'``` for summed monthly plots\n",
    "* ```plot_type = 'yearly'``` for summed yearly plots"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "29df312e",
   "metadata": {},
   "outputs": [],
   "source": [
    "save = True  # save matrix as netcdf\n",
    "plot_type = 'hourly' # plot Force 'hourly', 'daily', 'monthly', 'yearly'"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "b108a5f8",
   "metadata": {},
   "source": [
    "## Main Loop\n",
    "The next cell computes ambient noise Rayleigh waves source maps depending on the previously defined parameters.\n",
    "\n",
    "[FUNCTIONS] All functions are available in the script entitled source_microseism.py"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "3b375736",
   "metadata": {},
   "outputs": [],
   "source": [
    "loop_SDF(paths, dpt1, zlon, zlat,\n",
    "         date_vec = date_vec,\n",
    "         extent = extent,\n",
    "         parameters=parameters,\n",
    "         plot_type=plot_type,\n",
    "         save = save)"
   ]
  }
 ],
 "metadata": {
  "interpreter": {
   "hash": "b41f9438e40016795b0de681de09c65fe57767a8cd45ef97e8dca2a5fde64cd1"
  },
  "kernelspec": {
   "display_name": "Python 3.8.13 ('mypy3.8')",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.18"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
