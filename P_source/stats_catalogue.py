#!/usr/bin/env/python

__author__ = "Lisa Tomasetto"
__copyright__ = "Copyright 2022, UGA"
__credits__ = ["Lisa Tomasetto"]
__version__ = "1.0"
__maintainer__ = "Lisa Tomasetto"
__email__ = "lisa.tomasetto@univ-grenoble-alpes.fr"
__status__ = "Production"


""" This script reads catalogue text files and selects events above a given threshold on the force and between a certain area interval."""

########################################################################################################
## Libraries
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from sys import exit, argv
import glob
import cartopy.crs as ccrs
from cartopy.mpl.gridliner import LONGITUDE_FORMATTER, LATITUDE_FORMATTER
from datetime import datetime, timedelta
from calendar import monthrange

## Own Libs

#prevent SettingWithCopyWarning message from appearing
#pd.options.mode.chained_assignment = None
########################################################################################################
path_clog = "/Users/tomasetl/Documents/code/ocean_source/pr_oceanswell/ocean_ampli/catalogue/"

thresh = 2e12
year = 2021

########################################################################################################

def seasonality(path_clog, year):
    ##MAY 
    filename = "catalog-%d_%02d.txt"%(year, 5)
    filepath = path_clog + filename
    print(filepath)
    dset_may = pd.read_csv(filepath, sep= '\t', header = None, names = ['date', 'lon', 'lat', 'area', 'force', 'SNR'], index_col = False)

    ## Plot event maps per month
    X_may = np.array(dset_may.lon)
    Y_may = np.array(dset_may.lat)
    colors_may = np.array(dset_may.force)
    sizes_may = np.array(dset_may.area*1e-10)

    ##DECEMBER
    filename = "catalog-%d_%02d.txt"%(year, 12)
    filepath = path_clog + filename
    print(filepath)
    dset_dec = pd.read_csv(filepath, sep= '\t', header = None, names = ['date', 'lon', 'lat', 'area', 'force', 'SNR'], index_col = False)

    ## Plot event maps per month
    X_dec = np.array(dset_dec.lon)
    Y_dec = np.array(dset_dec.lat)
    colors_dec = np.array(dset_dec.force)
    sizes_dec = np.array(dset_dec.area*1e-10)


    fig = plt.figure(figsize = (10, 5))
    ax = plt.axes(projection=ccrs.PlateCarree())
    ax.coastlines()
    im1 = ax.scatter(X_may,Y_may, c=colors_may, s=sizes_may, alpha=0.8, cmap='hot', label='may')
    im2 = ax.scatter(X_dec, Y_dec, c=colors_dec, s=sizes_dec, alpha=0.8, cmap='viridis', label='december')
    plt.legend()
    ax.set(xlim=(-180, 180))
    ax.set(ylim=(-90, 90))
    #cbar = fig.colorbar(im1)
    #cbar.set_label("Force (N)")
    gl = ax.gridlines(crs=ccrs.PlateCarree(), draw_labels=True, linewidth=1, color='gray', alpha=0.5, linestyle='--')
    #gl.top_labels = False
    gl.right_labels = False
    #gl.xlocator = mticker.FixedLocator([-180, -90, -45, 0, 45, 90, 180])
    gl.xformatter = LONGITUDE_FORMATTER
    gl.yformatter = LATITUDE_FORMATTER
    plt.title("Events catalogue year %d"%(year))
    plt.show()
    #plt.savefig("map_catalog_%d_%d.png"%(year, i)) 

def stats_yearly(path_clog, year):
    df = pd.DataFrame()
    nb_events = np.zeros(12)
    for i in range(12):
        filename = "catalog-%d_%02d.txt"%(year, i+1)
        filepath = path_clog + filename
        print(filepath)
        dset = pd.read_csv(filepath, sep= '\t', header = None, names = ['date', 'lon', 'lat', 'area', 'force', 'SNR'], index_col = False)
        nb_events[i] = len(dset.date)
        df = pd.concat([df, dset], sort=False)

        ## Plot event maps per month
        X = np.array(dset.lon)
        Y = np.array(dset.lat)
        colors = np.array(dset.force)
        sizes = np.array(dset.area*1e-10)

        fig = plt.figure(figsize = (10, 5))
        ax = plt.axes(projection=ccrs.PlateCarree())
        ax.coastlines()
        im = ax.scatter(X,Y, c=colors, s=sizes, alpha=0.8)
        ax.set(xlim=(-180, 180))
        ax.set(ylim=(-90, 90))
        cbar = fig.colorbar(im)
        cbar.set_label("Force (N)")
        gl = ax.gridlines(crs=ccrs.PlateCarree(), draw_labels=True, linewidth=1, color='gray', alpha=0.5, linestyle='--')
        #gl.top_labels = False
        gl.right_labels = False
        #gl.xlocator = mticker.FixedLocator([-180, -90, -45, 0, 45, 90, 180])
        gl.xformatter = LONGITUDE_FORMATTER
        gl.yformatter = LATITUDE_FORMATTER
        plt.title("Events catalogue year %d month %d"%(year, i))
        #plt.show()
        plt.savefig("map_catalog_%d_%d.png"%(year, i)) 
    plt.close('all')

    plt.figure()
    plt.bar(np.arange(1, 13, 1),nb_events)
    plt.xlabel("Month number")
    plt.ylabel("Number of events")
    plt.title("%d"%year)
    plt.savefig("./catalogue/time_distribution_%d.png"%year)

    df.plot.scatter(x="area", y="force", c="SNR", s=50, cmap = "viridis", loglog=True)
    plt.savefig("./catalogue/area_force_%d.png"%year)

    df.plot.scatter(x="area", y="SNR", c="force", s=50, cmap="inferno", logx=True)
    plt.savefig("./catalogue/area_SNR_%d.png"%year)

    df.plot.scatter(x="SNR", y="force", c = "area", s=50, cmap="viridis", logy=True)
    plt.savefig("./catalogue/SNR_force_%d.png"%year)

    df.plot.scatter(x="date", y="force",  color= 'royalblue', legend=True, ylabel = "Force (N)", alpha=0.5)
    plt.savefig("./catalogue/time_force_%d.png"%year)

    df.plot.scatter(x="date", y="SNR", color='peru', legend = False, ylabel = "SNR", alpha=0.5)
    plt.savefig("./catalogue/time_SNR_%d.png"%year)


    dset = df.dropna(axis='columns', how='all')
    dset = dset[dset["force"] > thresh]
    dset.to_csv("./events_%d.txt"%year, header = True, sep=' ', index=False)

def dataframe_glob(path_clog, start_year, end_year):
    DF = pd.DataFrame()
    nb_years = end_year-start_year + 1
    nb_events = np.zeros((2, nb_years*12))
    years = np.arange(start_year, end_year+1, 1)

    for i, year in enumerate(years):
        for m in range(12):
            m += 1
            filename = "catalog-%d_%02d.txt"%(year, m)
            filepath = path_clog + filename
            print(filepath)
            dset = pd.read_csv(filepath, sep= '\t', header = None, names = ['date', 'lon', 'lat', 'area', 'force', 'SNR'], index_col = False)
            dset.date = dset.date.str.slice(start=0, stop = -3)
            dset.date = pd.to_datetime(dset.date, format="%Y-%m-%d-%H")
            DF = pd.concat([DF, dset], sort=False)
    DF.to_csv('catalogue_global_%d-%d.txt'%(start_year, end_year), sep='\t', header=True, index=False)


def distribution_force(df_path):
    dset = pd.read_csv(df_path, sep= '\t', header = 0, index_col = False, dtype={'lon': 'float64', 'lat': 'float64', 'area': 'float64', 'force': 'float64', 'SNR': 'float64'})
    force = np.array(dset.force)
    log_f = np.log(force)
    dset.date = pd.to_datetime(dset.date)
    print(dset.date)

    dset.hist(column='force', log=True, bins=200)
    plt.show()

def el_nino(df_path, soi_path):
    dset = pd.read_csv(df_path, sep= '\t', header = 0, index_col = False, dtype={'lon': 'float64', 'lat': 'float64', 'area': 'float64', 'force': 'float64', 'SNR': 'float64'})
    dset.date = pd.to_datetime(dset.date)

    soi = pd.read_csv(soi_path, sep='  ', header= 2, index_col=False,skipfooter=9)
    print(soi.YEAR)
    t = []
    data = []
    for y in soi.YEAR:
        for i, m in enumerate(soi.keys()[1::]):
            t.append(datetime(y, i+1, 1))
            data.append(float(soi[soi.YEAR==y][m]))
    mean = np.median(dset.force)
    print(mean)
    test = (dset.force - mean)/mean
    fig, ax = plt.subplots()
    color = 'tab:green'
    ax.set_xlabel('date')
    ax.set_ylabel('SOI standardized')
    ax.plot(t, data, 'g')
    ax.tick_params(axis='y', labelcolor=color)
    ax1 = ax.twinx()
    color = 'tab:blue'
    ax1.set_ylabel('force (N)')
    ax1.plot(dset.date, dset.force, 'b', alpha=0.3)
    ax1.tick_params(axis='y', labelcolor=color)
    plt.show()
if __name__ == "__main__":
    el_nino('./catalogue_global_2010-2021.txt', './soi_standardize.txt')