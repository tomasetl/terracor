#!/usr/bin/env python3

import math
import numpy as np
from numpy.lib.scimath import sqrt as csqrt
import matplotlib.pyplot as plt

def subfcn_liquid_solid(p, mi, mt):
    """Reflection and Transmition coefficients for P and S waves between two media"""
    p2 = np.array(p)**2
    # vp, density of incident medium, vp, vs, density of transmitted medium
    (vp1, rho1, vp2, vs2, rho2) = (mi[0], mi[-1], mt[0], mt[1], mt[2])
    q1 = csqrt(1/vp1**2 - p2)  # vertical slowness
    q2p = csqrt(1/vp2**2 - p2)
    q2s = csqrt(1/vs2**2 - p2)

    a = rho2*q1*((1-2*p2*vs2**2)**2 + 4*vs2**4*p2*q2p*q2s)
    b = rho1*q2p
    D = a + b
    Rpp = (a-b)/D
    Tpp = 2*rho1*q1*(1-2*(p*vs2)**2)*np.reciprocal(D)
    Tps = 4*rho1*q1*q2p*p*(vs2**2)*np.reciprocal(D)
    return Rpp, Tpp, Tps

def bathy(z, f, p, m):
    """Bathymetry secondary microseismic excitation coefficients(for P,S amplitude).

    Input:
    z: thickness of water layer
    f: seismic frequency in Hz
    p: slowness, if not specified return p values integral to 1/vp_crust
    m: [vp_water, rho_water, vp_crust, vs_crust, rho_crust]
     default values are [1.5 km/s, 1.0 g/cm³, 5.54 km/s, 3.2 km/s, 2.5 g/cm³]
    if z in m|km, v should be in m/s|km/s. All rhos must keep the same units.

    Output:
    cP, cS: excitation coefficients of P|S waves in shape of [[p,]f, z]
    author: TOMASETTO Lisa, lisa.tomasetto@gmail.com
    based on LI Lei, ll.ynyf@gmail.com modified by Pierre Boue 23/11/2020
    example:
    z = np.linspace(0, 25000, 5001)
    f = 1/8
    cP, cS = bathy(z, f, [], [])
    x = f*z/1500
    plt.figure()
    plt.plot(x, abs(cP[0]), x, abs(cS[0]))
    plt.show()

    """
    ##
    (rhow, rhoc) = (1000, 2500)  # in kg/m³
    (vpw, vpc, vsc) = (1500, 5540, 3200)  # in m/s
    if len(m) > 0:
        if (len(m) >= 1 and m[0] > 0):
            vpw = m[0]
        if (len(m) >= 2 and m[1] > 0):
            rhow = m[1]
        if (len(m) >= 3 and m[2] > 0):
            vpc = m[2]
        if (len(m) >= 4 and m[3] > 0):
            vsc = m[3]
        if (len(m) >= 5 and m[4] > 0):
            rhoc = m[4]
        print('m not empty')

    elif np.max(z) < 50:  # guess in km
        (vpw, rhow, vpc, vsc, rhoc) = (1.5, 1, 5.54, 3.2, 2.5)
        print('[rho_w, rho_c] = [1, 2.5] g/cm³')
        print('[vpw, vpc, vsc] = [1.5, 5.54, 3.2] km/s')
    if len(p) == 0:
        p = np.linspace(0, 0.995, 200)/vpc
        m = np.array([vpw, rhow, vpc, vsc, rhoc])
        a = np.arcsin(vpw*p)
        cP = np.zeros((np.size(f), np.size(z)), dtype="complex_")
        cS = np.zeros((np.size(f), np.size(z)), dtype="complex_")
        for i in range(np.size(f)):
            #print(np.size(f))
            if np.size(f) == 1:
                c_P, c_S = bathy(z, f, p, m)
            else:
                c_P, c_S = bathy(z, f[i], p, m)
            #print('c_P=c_S)', c_P == c_S)
            #print('c_P', c_P.shape)
            #print('cP(i)', cP.shape)
            #print('abs(c_P)**2', (np.trapz((abs(c_P)**2), x=a, axis=0)).shape)
            cP[i, :] = cP[i, :] + np.trapz(abs(c_P)**2, x=a, axis=0)
            cS[i, :] = cS[i, :] + np.trapz(abs(c_S)**2, x=a, axis=0)
        (cP, cS) = (csqrt(cP), csqrt(cS))
        return cP, cS
    Rpp, Tpp, Tps = subfcn_liquid_solid(p, [vpw, rhow], [vpc, vsc, rhoc])
    #print('Rpp', Rpp.shape)
    #print('Tpp', Tpp.shape)
    #print('Tps', Tps.shape)
    qw = csqrt(1/vpw**2 - p**2)
    #print('qw', qw.shape)
    #print('f', f)
    #print('z', z.shape)
    #print('test', (np.dot(f,z)).shape)
    if np.size(f) == 1:
        phi = 4*np.pi*f*(z.flatten('F')).T
    else:
        phi = 4*np.pi*np.dot(f.flatten('F'), (z.flatten('F')).T)
    #print('phi', phi.shape)
    cP = np.empty((np.size(p), np.size(f), np.size(z)), dtype="complex_")
    #print('cP', cP.shape)
    cP[:] = np.nan
    cS = cP.copy()
    for i in range(np.size(p)):
        C = 1*np.reciprocal(1 + Rpp[i]*np.exp(1j*phi*qw[i]))
        #print('C', C.shape)
        cP[i, :, :] = np.dot(Tpp[i], C)  # dot ?
        cS[i, :, :] = np.dot(Tps[i], C)  # dot ?
    #print('diff', Tpp-Tps)
    #print('cP = cS', cP == cS)
    (cP, cS) = (np.squeeze(cP), np.squeeze(cS))
    return cP, cS

"""
z = np.linspace(0, 25000, 5001)
f = 1/8
(cP, cS) = bathy(z, f, [], [])
x = f*z/1500
plt.figure()
plt.plot(x, abs(cP[0]), x, abs(cS[0]))
plt.show()"""
