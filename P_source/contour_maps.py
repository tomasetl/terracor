#!/usr/bin/env python3

__author__ = "Lisa Tomasetto"
__copyright__ = "Copyright 2022, UGA"
__credits__ = ["Lisa Tomasetto"]
__version__ = "1.0"
__maintainer__ = "Lisa Tomasetto"
__email__ = "lisa.tomasetto@univ-grenoble-alpes.fr"
__status__ = "Production"

""" This program uses the wavewatch 3 model to calculate an equivalent vertical force. Parameters are given trough a parameter.py file. The calculation is based on Nishida and Takagi (2016) supplementary material document."""

## Libraries
import os
import time
from calendar import monthrange
import matplotlib.pyplot as plt
import matplotlib as mpl
import numpy as np
import cartopy.crs as ccrs
from cartopy.mpl.gridliner import LONGITUDE_FORMATTER, LATITUDE_FORMATTER
import matplotlib.ticker as mticker
from datetime import datetime
from math import *
from obspy.geodetics.base import gps2dist_azimuth
import cv2
from copy import deepcopy
import scipy.ndimage as ndi


## Own Libs
from bathy_G14 import bathy
from read_hs_p2l import read_WWNC, read_WWNCf
from readWW31 import read_dpt
from parameters import *


####################################################################################################
SMALL_SIZE = 14
MEDIUM_SIZE = 16
BIGGER_SIZE = 18

plt.rc('font', size=SMALL_SIZE)          # controls default text sizes
plt.rc('axes', titlesize=SMALL_SIZE)     # fontsize of the axes title
plt.rc('axes', labelsize=MEDIUM_SIZE)    # fontsize of the x and y labels
plt.rc('xtick', labelsize=SMALL_SIZE)    # fontsize of the tick labels
plt.rc('ytick', labelsize=SMALL_SIZE)    # fontsize of the tick labels
plt.rc('legend', fontsize=SMALL_SIZE)    # legend fontsize
plt.rc('figure', titlesize=BIGGER_SIZE)  # fontsize of the figure title
####################################################################################################

def grad(phi,v=2):
    #Apparently backward euler is enough for this task
    #central differences for gradient image
    if v==1:
        nr,nc = phi.shape
        gx = np.zeros((nr,nc))
        gy = np.zeros((nr,nc))
        gx[1:] = phi[1:] - phi[:-1]
        gy[:,1:] = phi[:,1:] - phi[:,:-1]
    
    else:
        nr,nc = phi.shape
        padx = np.array([np.zeros(nc)]) 
        pady = np.array([np.zeros(nr)])

        paddedx1 = np.concatenate((phi[1:,:],padx))
        paddedx2 = np.concatenate((padx,phi[:-1,:]))
        gx = (paddedx1-paddedx2)/2.

        paddedy1 = np.hstack((phi[:,1:],pady.T))
        paddedy2 = np.hstack((pady.T,phi[:,:-1]))
        gy = (paddedy1-paddedy2)/2.
        
    return gx,gy

def div(gx,gy,v=2):
    gxx,_ = grad(gx,v=2)
    _,gyy = grad(gy,v=2)
    
    return gxx+gyy

def G(Phi,v=2):
    
    #we compute here the function G
    nr,nc = Phi.shape
    g_phix,g_phiy = grad(Phi,v)
    n_phi = np.sqrt(g_phix**2+g_phiy**2)
    eps = 10E-10
    #to avoid division by 0
    n_phi = np.maximum(eps*np.ones((g_phix.shape[0],g_phix.shape[1])),n_phi)
    g_phix, g_phiy = g_phix/n_phi, g_phiy/n_phi

    K = - n_phi*div(g_phix, g_phiy,v)
    return K

def mean_curv(phi0,tau,Tmax,I0,v=2):
    phi = deepcopy(phi0)
    for i in range(int(Tmax//tau)):
        phi = phi - tau*G(phi,v)
    return phi

def inter(a,b):
    return list(set(a)&set(b))
##########################################################################################################

## Coefficient of amplification
#Calculation of the coefficient of amplification 
T_seismic = np.array([3.209, 3.530, 3.883, 4.271, 4.698, 5.168, 5.685, 6.253,
             6.879, 7.567, 8.323, 9.156])  
file_bathy = '%sww3.07121700.dpt'%fpath
[zlat, zlon, dpt1, var] = read_dpt(file_bathy)
msin = np.array([np.sin(np.pi/2 - np.radians(zlat))]).T
ones = np.ones((1, len(zlon)))
dA = radius**2*res_mod**2*np.dot(msin,ones)
#print(dA.shape)
#plt.figure()
#im = plt.pcolor(dA)
#plt.colorbar(im)
#plt.show()
#exit()

## Generate cP matrix
#coeff = np.empty((len(dpt1), len(dpt1[0]), len(T_seismic)))
#for i, T in enumerate(T_seismic):
    ## compute amplification for Tp and rp following G14
    #(cP, cS) = bathy(dpt1/1e3, 1/T, rp, layers)
    #cP = abs(cP.reshape(len(dpt1), len(dpt1[0]), order='F').copy())
    #cS = abs(cS.reshape(len(dpt1), len(dpt1[0]), order='F').copy())
    #bathy_ampli_P = cP/theta_lim
    #coeff[:, :, i] = bathy_ampli_P
#np.save('cP.npy', coeff)
#exit()
# load from saved numpy matrix
cP = np.load('cP.npy')

# Loop on dates and load PSD from WW3 model
F0 = []
date = []

for iyear in np.array([YEAR]):
    if isinstance(MONTH, int):
        MONTH = np.array([MONTH])
    elif not len(MONTH):
        MONTH = np.arange(1, 13)
    else:
        MONTH = np.array(MONTH)
    for imonth in MONTH:
        daymax = monthrange(iyear,imonth)[1]
        if imonth < 10:
            filepath = '%sWW3-GLOB-30M_%d0%d'%(fpath, iyear, imonth)
        else:
            filepath = '%sWW3-GLOB-30M_%d%d'%(fpath, iyear, imonth)
        filename_p2l = '%s_p2l.nc'%(filepath)
        print(filename_p2l)
        try:
            day = np.array(DAY)
            if day[0] > day[-1]:
                index = np.squeeze(np.argwhere(day==daymax))
                if imonth == MONTH[0]:
                    day = day[:index+1]
                elif imonth == MONTH[-1]:
                    index = np.squeeze(np.argwhere(day==monthrange(iyear, imonth-1)[1]))
                    day = day[index+1:]
                else:
                    day = np.arange(1,daymax+1)
        except:
            try:
                day = int(np.squeeze(day))
            except:
                day= np.arange(1,(monthrange(iyear,imonth)[1])+1)
        for iday in day:
            if isinstance(HOUR, int):
                HOUR = np.array([HOUR])
            elif not len(HOUR):
                HOUR = np.arange(0,24,3)
            else:
                HOUR = np.array(HOUR)
            for ih in HOUR:
                timerange= str(iyear)+'-' + '%02d' % imonth+'-' + '%02d' % iday+'-' +'%02d' % ih+'-' +'%02d' %+(ih+3)
                # load model for given hour
                (lat, lon, freq, p2l, unit1, scale) = read_WWNCf(filename_p2l, [iyear, imonth, iday, ih], [], [])
                nf = len(freq) 
                xfr = np.exp(np.log(freq[-1]/freq[0])/(nf-1))  # determines the xfr geometric progression factor
                df = freq*0.5*(xfr-1/xfr)  # ocean frequency interval in wave model
                freq = 2*freq  #ocean to seismic waves freq
                index_freq = np.squeeze(np.argwhere((f1<=freq)&(freq<=f2)))  # select frequency range of interest
                if unit1 == 'log10(Pa2 m2 s+1E-12':
                    p2l = np.exp(lg10*p2l)  - (1E-12-1E-16)
                if unit1 == 'log10(Pa2 m2 s+1E-12)':
                    p2l = np.exp(lg10*p2l)  - (1E-12-1E-16)
                elif unit1 == 'log10(m4s+0.01':
                    p2l = np.exp(lg10*p2l) - 0.009999  # depending on the unit add a correction coefficient from Gualtieri code
                ## integral over frequency
                df = df[index_freq]
                Fp = p2l[:, :,index_freq]
                freq = freq[index_freq]
                #print(Fp.shape)
                n_freq = len(index_freq)
                #print(n_freq)
                for i in range(n_freq):
                    T = 1/freq[i]
                    ind_cp = np.argmin(abs(T_seismic-T))
                    coeff = np.square(cP[:, :, ind_cp]).T
                    try:
                        Fp[:, :, i] = df[i]*coeff*Fp[:, :, i]  # multiply by df
                    except:
                        Fp = Fp[:, :-6, :] 
                        Fp[:, :, i] = df[i]*coeff*Fp[:, :, i]
                I_freq = np.sum(Fp, axis = 2)  # integral over frequency
                F = np.empty(I_freq.shape)
                if iyear > 2017:
                    lat = lat[:-6]
                for i, lati in enumerate(lat):
                    for j, longi in enumerate(lon):
                        F[j, i] = 2*np.pi*np.sqrt(dA[i, j]*I_freq[j, i])  # force = 2*pi*sqrt( \int Fp df dS) where dS = R^2 sin(colat) d_lat d_lon

                ## Contour and events  (from Ruohan Zhang's code)
                ##### Convert to matrix suitable for level set calculations
                F_blur = cv2.blur(np.nan_to_num(F), (5, 5))
                T_lamda0 = max(map(max, F_blur))*0.25
                if T_lamda0 > 0.5e10:
                    T_lamda = 0.5e10
                elif T_lamda0 < 0.4e10 :
                    T_lamda = 0.4e10
                else :
                    T_lamda = T_lamda0
                print('T_lamda = ',T_lamda)


                press0 = F_blur.copy()
                ind0 =np.where(press0==0)
                
                press1 = press0.copy()
                press1[ind0] = min(map(min,press0))
                press1 = press1- T_lamda 
                
                event_press =press1/max(map(max,press1)) #0.4*np.ones((press1.shape))- 
                
                #Tmax = 2
                #tau = 0.01 
                Tmax = 10
                tau = 100
                phi = mean_curv(event_press,tau,Tmax,None)
                phi_0 =phi.copy()
                
                level=0
                if plot:
                    ## Figure
                    plt.figure(figsize=(10,5), dpi=150)
                    ax = plt.axes(projection=ccrs.PlateCarree())
                    ax.coastlines()
                    ax.set_extent((lon_min, lon_max, lat_min, lat_max))
                    ax.contour(lon, lat, phi_0.T,[level],linewidths=1, colors="red", transform=ccrs.PlateCarree())
                    im = ax.pcolormesh(lon, lat, F.T, shading='auto', vmin = 0.1e10, vmax = 4e10, rasterized = True)
                    #im = ax.imshow(np.rot90(F), vmin = 0e10, vmax = 3e10, extent=[min(lon), max(lon), min(lat), max(lat)])
                    cbar = plt.colorbar(im, fraction=.046, pad=0.06, shrink=0.7)
                    cbar.set_label('Force (N)')
                    gl = ax.gridlines(crs=ccrs.PlateCarree(), draw_labels=True, linewidth=1, color='gray', alpha=0.5, linestyle='--')
                    gl.top_labels = False
                    gl.right_labels = False
                    gl.xformatter = LONGITUDE_FORMATTER
                    gl.yformatter = LATITUDE_FORMATTER
                    plt.title('Equivalent Force %d %d %d hour: %d, P wave'%(iyear, imonth, iday, ih))
                    #plt.show()
                    plt.savefig('./images/source_contour_%d%d%dT%d.pdf'%(iyear, imonth, iday, ih), dpi=200, transparent=True)
                    plt.close('all')
                ## Catalog creation
                phi_01 = phi.copy()
                phi_01[phi_01>0]=1
                phi_01[phi_01<=0]=0
                
                img = (phi_01 * 255).astype(np.uint8)
                
                ########      Binarization    #############
                ret, binary = cv2.threshold(img,1,255,cv2.THRESH_BINARY)  
                
                ###############    Boundary extraction, containing the coordinates of the boundary values 
                contours, hierarchy = cv2.findContours( binary,cv2.RETR_TREE,cv2.CHAIN_APPROX_NONE)
                
                ########################################################################################
                #########     The events that crosses 180° is split into 2 or more, need to be concatenated into 1     
                #########
                left_event = []
                right_event = []
                for i in range( len(contours) ):
                    if np.any(contours[i] == 0):   # 0 means left boundary
                        left_event.append(i)
                    if np.any(contours[i] == 719) :  # right boundary
                        right_event.append(i)
                left_event =np.array(left_event)
                right_event=np.array(right_event)    
                same_event = [] ## is the same boundary event
                try:
                    if left_event == right_event:
                        continue
                except:
                    print(".any() or .all() error")
                if len(left_event)>= len(right_event) :
                    boundary_num = len(left_event)  ### number of boundary event
                    for i in range(boundary_num):   
                        left_y0 = np.unique( contours[left_event[i]][:,0,:][:,1] ) 
                        for j in range(len(right_event)):
                            right_y0= np.unique( contours[right_event[j]][:,0,:][:,1] )
                            conbin_y = np.concatenate( [left_y0,right_y0],axis=0) 
                            conbin_y0 =np.unique( conbin_y )   
                            if len(conbin_y ) != len(conbin_y0 ):
                                print('Boundary Event: '+ str(left_event[i])+' and '+ str(right_event[j]) )
                                same_event.append([left_event[i],right_event[j]])
                elif len(left_event)< len(right_event) :
                    boundary_num = len(right_event)  
                    for i in range(boundary_num):   
                        right_y0 = np.unique( contours[right_event[i]][:,0,:][:,1] ) 
                        for j in range(len(left_event)):
                            left_y0= np.unique( contours[left_event[j]][:,0,:][:,1] )
                            conbin_y = np.concatenate( [left_y0,right_y0],axis=0) 
                            conbin_y0 =np.unique( conbin_y )  
                            if len(conbin_y ) != len(conbin_y0 ):
                                print('Boundary Event: '+ str(left_event[j]) +' and '+ str(right_event[i]) )
                                same_event.append([left_event[j],right_event[i]])
                                       
                ########################################
                new_same = []
                lr1=[] # The event has only one branch on the left side and right side
                
                for i in range(len(same_event)):
                    #print(same_event.shape[0])
                    ab0=0
                    for j in range(len(same_event)):
                        if i==j:
                            continue
                        lst3=inter(same_event[i],same_event[j])
                        ab0=ab0+len(lst3)
                        #print('len(lst3)',len(lst3))
                    if  ab0 ==0:    
                        new_same.append(same_event[i])
                        lr1.append(i)
                #  Remove only one on each side
                same_event=[i for num,i in enumerate(same_event) if num not in lr1]        
                
                ####  The event has More Than one branch on the left side OR right side
                sum_in0 = len(same_event)
                abc= len(same_event)
                while abc !=0 :    
                    i=0
                    same_e0=[]
                    len_same_event0 = len(same_event)
                    for j in range(len(same_event)):
                        if i==j:
                            continue
                        lst3=inter(same_event[i],same_event[j])
                        print(lst3)
                        if lst3:
                            print(str(i)+' and '+str(j)+' are same events')
                            same_e0.append(i)
                            same_e0.append(j)
                            sum_in0 -=2
                            ab0 = np.concatenate(  [ same_event[i] , same_event[j] ],axis=0)
                            ab1 = np.unique(ab0)
                            ab1 = list(ab1)
                            
                            same_event = np.delete(same_event,[i,j],axis=0)
                            same_event =list(same_event)
                            same_event.append(np.array(ab1))
                            break
                    abc = len_same_event0 - len(same_event)            
                
                
                for i in range(len(same_event)):
                    new_same.append(same_event[i])
                print('Same boundary events: '+ str(new_same) )
                num_of_same=[]
                for i in range(len(new_same)):
                    for j in range(len(new_same[i])):
                        num_of_same.append(new_same[i][j])
                #num_of_same  
                
                ######   separate each event
                img2=[0 for i in range(len(contours))]
                for i in range(len(contours)):   
                    img2[i]=np.zeros((binary.shape)) 
                    for a in range(binary.shape[1]):  
                        for b in range(binary.shape[0]): 
                            result = cv2.pointPolygonTest(contours[i], (a,b),False ) 
                            if result>=0:
                                img2[i][b,a]=1
    
                ########################################################################################
                ########################################################################################    
                ####   Separate each microseismic event and calculate the corresponding energy and centroid
                
                msin = np.array([np.sin(np.pi/2 - np.radians(lat))]).T
                ones = np.ones((1, len(lon)))
                dA = radius**2*res_mod**2*np.dot(msin,ones)
                
                Single_F = np.nan_to_num(F)  ###  nan convert to 0  
                all_event = np.arange(0,len(contours),1)
                ############    number of events
                num_event0 = len(contours)   
                A_event0 =np.zeros(num_event0)
                #A_density0 = np.zeros(num_event0)
                A_area0 = np.zeros(num_event0)
                A_arearest0 = np.zeros(num_event0)
                A_rest0 = np.zeros(num_event0)
                cent_lat0=np.zeros(num_event0)
                cent_lon0=np.zeros(num_event0)
                SNR_0 = np.zeros(num_event0)
                for i in range( num_event0 ): 
                    event = img2[i]* Single_F
                    ind_lon,ind_lat = np.where(img2[i]!=0)
                    ind_lon_rest, ind_lat_rest = np.where(img2[i]==0)
                    lat_no_0 = lat[ind_lat]
                    lon_no_0 = lon[ind_lon]
                    
                    A_event0[i] = np.sum(Single_F[ind_lon,ind_lat])  # energy of event
                    A_area0[i] = np.sum( dA.T[ind_lon,ind_lat])
                    A_rest0[i] = np.sum(Single_F[ind_lon_rest, ind_lat_rest])
                    A_arearest0[i] = np.sum(dA.T[ind_lon_rest, ind_lat_rest])
                    
                    SNR_0[i] = (A_event0[i]*A_arearest0[i])/(A_rest0[i]*A_area0[i])
                    #print(SNR_0[i])
                    
                    img =  np.nan_to_num(event )
                    cy, cx = ndi.center_of_mass(img**4) ### 4 power, making the center closer to the high-energy 
                    cent_lat0[i] = lat[int(np.floor(cx))]
                    cent_lon0[i] = lon[int(np.floor(cy))] 
                    
                if len(num_of_same)==0:
                    single_event= all_event
                else :
                    print('ooooo')
                    single_event= np.delete(all_event, num_of_same, axis=0)
                    
                num_event = len(single_event)+len(new_same) # single events and boundary events
                print('Number of Events: '+ str(num_event))

                A_event =np.zeros(num_event)
                A_area = np.zeros(num_event)
                cent_lat=np.zeros(num_event)
                cent_lon=np.zeros(num_event)
                SNR = np.zeros(num_event)
                A_event[0:len(single_event)]=A_event0[single_event]  
                A_area[0:len(single_event)]=A_area0[single_event]
                cent_lat[0:len(single_event)]=cent_lat0[single_event]
                cent_lon[0:len(single_event)]=cent_lon0[single_event]
                SNR[0:len(single_event)]=SNR_0[single_event]
                
                jj = 0
                
                for i in range( len(single_event),num_event ):
                    print(new_same[jj])
                    A_event[i] = np.sum( A_event0[new_same[jj]] )   
                    A_area[i] = np.sum( A_area0[new_same[jj]] )
                    cent_lat[i]= np.mean(cent_lat0[new_same[jj]])
                    SNR[i]=np.mean(SNR[new_same[jj]])
                    b_lon0 = cent_lon0[new_same[jj]]
                    temp_lon = ( np.mean( b_lon0[b_lon0>0] ) + np.mean( b_lon0[b_lon0<0] ) )/2
                    if temp_lon>=0 : 
                        cent_lon[i] = -180+temp_lon
                    else :
                        cent_lon[i] = 180+temp_lon
                    jj +=1 
                    
                ###  rearrange events from large to small   
                small_to_big = np.sort(A_event)
                new_lon=[]
                new_lat=[]
                new_event = []
                new_area = []
                new_snr = []
                for i in range(len(A_event)-1,-1,-1):
                    ind_i = np.where(A_event==small_to_big[i])
                    new_lon.append(cent_lon[ind_i[0][0]])
                    new_lat.append(cent_lat[ind_i[0][0]])
                    new_event.append(A_event[ind_i[0][0]])
                    new_area.append(A_area[ind_i[0][0]])
                    new_snr.append(SNR[ind_i[0][0]])
                    
                ######   save txt
                f = open('./catalogue/catalog'+str(iyear)+'-'+str(imonth)+'.txt','a')
                #f.write("\n") 
                for i in range(num_event):
                    f.write(timerange+'\t')
                    f.write("%7.1f\t" % new_lon[i]) 
                    f.write("%6.1f\t" % new_lat[i]) 
                    f.write("%16.1f\t" % new_area[i])
                    f.write("%.1f\t" % new_event[i])
                    f.write("%.1f\t" % new_snr[i])
                    f.write("\n")
f.close()
