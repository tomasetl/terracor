#!/usr/bin/env python3
from math import radians, log
import numpy as np
"""Parameters for ocean site effect maps and clustering computation"""

# seismic waves
WAVE_TYPE = 'P'  # P or S
T_P = 10  # dominant period
rp = []  # ray parameter/slowness in s/km, if empty return integral to 1/Vp_crust
layers = [1.5, 1.0, 5.54, 3.2, 2.5]  # water crust layers properties [Vp_w, rho_w,Vp_c, Vs_c, rho]"""
theta_lim = radians(15.71)  # P waves take-off angle

# dates
YEAR = 2014 # loop if vector
MONTH = [12]# loop if vector
DAY =  [10]  # loop if vector, compute all days of the month if empty
HOUR = []  # loop if vector

# ww3
fpath = '/Users/tomasetl/Documents/code/ocean_source/data/weather_bomb/'
path_in = '/Users/tomasetl/Documents/code/ocean_source/pr_oceanswell/ocean_ampli/'
res_mod = radians(0.5)  # angular resolution of the model
radius = 6.371*1e6
lg10 = log(10)

# extent
lat_min = -90
lat_max = 90
lon_min = -180
lon_max = 180

# single vertical force
f1 = 0.  # frequency to integrate from
f2 = 1. # frequency to integrate to
plot = True # plot PSD integrated over frequency

