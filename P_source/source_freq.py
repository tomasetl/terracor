#!/usr/bin/env python3

__author__ = "Lisa Tomasetto"
__copyright__ = "Copyright 2022, UGA"
__credits__ = ["Lisa Tomasetto"]
__version__ = "1.0"
__maintainer__ = "Lisa Tomasetto"
__email__ = "lisa.tomasetto@univ-grenoble-alpes.fr"
__status__ = "Production"

""" This program uses the wavewatch 3 model to calculate an equivalent vertical force. Parameters are given trough a parameter.py file. The calculation is based on Nishida and Takagi (2016) supplementary material document."""

## Libraries
import os
import time
from calendar import monthrange
import matplotlib.pyplot as plt
import matplotlib as mpl
import numpy as np
import cartopy.crs as ccrs
from cartopy.mpl.gridliner import LONGITUDE_FORMATTER, LATITUDE_FORMATTER
import matplotlib.ticker as mticker
from datetime import datetime
from math import *
from obspy.geodetics.base import gps2dist_azimuth
import cv2
from copy import deepcopy
import scipy.ndimage as ndi


## Own Libs
from bathy_G14 import bathy
from read_hs_p2l import read_WWNC, read_WWNCf
from readWW31 import read_dpt
from parameters import *


####################################################################################################
SMALL_SIZE = 14
MEDIUM_SIZE = 16
BIGGER_SIZE = 18

plt.rc('font', size=SMALL_SIZE)          # controls default text sizes
plt.rc('axes', titlesize=SMALL_SIZE)     # fontsize of the axes title
plt.rc('axes', labelsize=MEDIUM_SIZE)    # fontsize of the x and y labels
plt.rc('xtick', labelsize=SMALL_SIZE)    # fontsize of the tick labels
plt.rc('ytick', labelsize=SMALL_SIZE)    # fontsize of the tick labels
plt.rc('legend', fontsize=SMALL_SIZE)    # legend fontsize
plt.rc('figure', titlesize=BIGGER_SIZE)  # fontsize of the figure title
####################################################################################################

def grad(phi,v=2):
    #Apparently backward euler is enough for this task
    #central differences for gradient image
    if v==1:
        nr,nc = phi.shape
        gx = np.zeros((nr,nc))
        gy = np.zeros((nr,nc))
        gx[1:] = phi[1:] - phi[:-1]
        gy[:,1:] = phi[:,1:] - phi[:,:-1]
    
    else:
        nr,nc = phi.shape
        padx = np.array([np.zeros(nc)]) 
        pady = np.array([np.zeros(nr)])

        paddedx1 = np.concatenate((phi[1:,:],padx))
        paddedx2 = np.concatenate((padx,phi[:-1,:]))
        gx = (paddedx1-paddedx2)/2.

        paddedy1 = np.hstack((phi[:,1:],pady.T))
        paddedy2 = np.hstack((pady.T,phi[:,:-1]))
        gy = (paddedy1-paddedy2)/2.
        
    return gx,gy

def div(gx,gy,v=2):
    gxx,_ = grad(gx,v=2)
    _,gyy = grad(gy,v=2)
    
    return gxx+gyy

def G(Phi,v=2):
    
    #we compute here the function G
    nr,nc = Phi.shape
    g_phix,g_phiy = grad(Phi,v)
    n_phi = np.sqrt(g_phix**2+g_phiy**2)
    eps = 10E-10
    #to avoid division by 0
    n_phi = np.maximum(eps*np.ones((g_phix.shape[0],g_phix.shape[1])),n_phi)
    g_phix, g_phiy = g_phix/n_phi, g_phiy/n_phi

    K = - n_phi*div(g_phix, g_phiy,v)
    return K

def mean_curv(phi0,tau,Tmax,I0,v=2):
    phi = deepcopy(phi0)
    for i in range(int(Tmax//tau)):
        phi = phi - tau*G(phi,v)
    return phi

def inter(a,b):
    return list(set(a)&set(b))
##########################################################################################################

## Coefficient of amplification
#Calculation of the coefficient of amplification 
T_seismic = np.array([3.209, 3.530, 3.883, 4.271, 4.698, 5.168, 5.685, 6.253,
             6.879, 7.567, 8.323, 9.156])  
file_bathy = '%sww3.07121700.dpt'%fpath
[zlat, zlon, dpt1, var] = read_dpt(file_bathy)
msin = np.array([np.sin(np.pi/2 - np.radians(zlat))]).T
ones = np.ones((1, len(zlon)))
dA = radius**2*res_mod**2*np.dot(msin,ones)
print(dA.shape)
#plt.figure()
#im = plt.pcolor(dA)
#plt.colorbar(im)
#plt.show()

# load from saved site effect matrix
cP = np.load('cP.npy')

# Loop on dates and load PSD from WW3 model
F0 = []
date = []

for iyear in np.array([YEAR]):
    if isinstance(MONTH, int):
        MONTH = np.array([MONTH])
    elif not len(MONTH):
        MONTH = np.arange(1, 13)
    else:
        MONTH = np.array(MONTH)
    for imonth in MONTH:
        daymax = monthrange(iyear,imonth)[1]
        if imonth < 10:
            filepath = '%sWW3-GLOB-30M_%d0%d'%(fpath, iyear, imonth)
        else:
            filepath = '%sWW3-GLOB-30M_%d%d'%(fpath, iyear, imonth)
        filename_p2l = '%s_p2l.nc'%(filepath)
        print(filename_p2l)
        try:
            day = np.array(DAY)
            if day[0] > day[-1]:
                index = np.squeeze(np.argwhere(day==daymax))
                if imonth == MONTH[0]:
                    day = day[:index+1]
                elif imonth == MONTH[-1]:
                    index = np.squeeze(np.argwhere(day==monthrange(iyear, imonth-1)[1]))
                    day = day[index+1:]
                else:
                    day = np.arange(1,daymax+1)
        except:
            try:
                day = int(np.squeeze(day))
            except:
                day= np.arange(1,(monthrange(iyear,imonth)[1])+1)
        for iday in day:
            if isinstance(HOUR, int):
                HOUR = np.array([HOUR])
            elif not len(HOUR):
                HOUR = np.arange(0,24,3)
            else:
                HOUR = np.array(HOUR)
            for ih in HOUR:
                timerange= str(iyear)+'-' + '%02d' % imonth+'-' + '%02d' % iday+'-' +'%02d' % ih+'-' +'%02d' %+(ih+3)
                # load model for given hour
                (lat, lon, freq, p2l, unit1, scale) = read_WWNCf(filename_p2l, [iyear, imonth, iday, ih], [], [])
                nf = len(freq) 
                xfr = np.exp(np.log(freq[-1]/freq[0])/(nf-1))  # determines the xfr geometric progression factor
                df = freq*0.5*(xfr-1/xfr)  # ocean frequency interval in wave model
                
                freq = 2*freq  #ocean to seismic waves freq
                index_freq = np.squeeze(np.argwhere((f1<=freq)&(freq<=f2)))  # select frequency range of interest
                if unit1 == 'log10(Pa2 m2 s+1E-12':
                    p2l = np.exp(lg10*p2l)  - (1E-12-1E-16)
                if unit1 == 'log10(Pa2 m2 s+1E-12)':
                    p2l = np.exp(lg10*p2l)  - (1E-12-1E-16)
                elif unit1 == 'log10(m4s+0.01':
                    p2l = np.exp(lg10*p2l) - 0.009999  # depending on the unit add a correction coefficient from Gualtieri code
                ## integral over frequency
                df = df[index_freq]
                Fp = p2l[:, :,index_freq]
                freq = freq[index_freq]
                #print(Fp.shape)
                n_freq = len(index_freq)
                for i in range(n_freq):
                    T = 1/freq[i]
                    ind_cp = np.argmin(abs(T_seismic-T))
                    coeff = np.square(cP[:, :, ind_cp]).T
                    try:
                        Fp[:, :, i] = 2*np.pi*np.sqrt(dA.T*coeff*Fp[:, :, i])  # multiply by df
                    except:
                        Fp = Fp[:, :-6, :] 
                        Fp[:, :, i] = 2*np.pi*np.sqrt(dA.T*coeff*Fp[:, :, i])
                    if iyear > 2017:
                        lat = lat[:-6]
                
                    if plot:
                        ## Figure
                        plt.figure(figsize=(10,5), dpi=150)
                        ax = plt.axes(projection=ccrs.PlateCarree())
                        ax.coastlines()
                        #ax.set_extent((lon_min, lon_max, lat_min, lat_max))
                        #ax.contour(lon, lat, phi_0.T,[level],linewidths=1, colors="red", transform=ccrs.PlateCarree())
                        im = ax.pcolor(lon, lat, Fp[:, :, i].T, shading='auto') #, vmin = 0, vmax = 3e10)
                        cbar = plt.colorbar(im, fraction=.046, pad=0.06, shrink=0.7)
                        cbar.set_label('Force (N.s)')
                        gl = ax.gridlines(crs=ccrs.PlateCarree(), draw_labels=True, linewidth=1, color='gray', alpha=0.5, linestyle='--')
                        gl.top_labels = False
                        gl.right_labels = False
                        gl.xformatter = LONGITUDE_FORMATTER
                        gl.yformatter = LATITUDE_FORMATTER
                        plt.title('Equivalent Force %d %d %d hour: %d, P wave, freq: %.3f (Hz)'%(iyear, imonth, iday, ih, freq[i]))
                        plt.show()
                        #plt.savefig('./images/source_contour_%d%d%dT%d.png'%(iyear, imonth, iday, ih), dpi=150)
                        plt.close('all')
                    np.savez("model_%d%02d%02dT%02d.npz"%(iyear, imonth, iday, ih), F = Fp, df = df, fq = freq, lat=lat, lon = lon)
