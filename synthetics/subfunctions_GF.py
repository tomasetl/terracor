#!/usr/bin/env python3

__author__ = "Lisa Tomasetto"
# inspired by the code global_REF.py from Ruohan ZHANG
__copyright__ = "Copyright 2022, UGA"
__credits__ = ["Lisa Tomasetto"]
__version__ = "1.0"
__maintainer__ = "Lisa Tomasetto"
__email__ = "lisa.tomasetto@univ-grenoble-alpes.fr"
__status__ = "Production"

"""This script gathers subfunctions for the synth_microseism.py script"""

##########################################################################

## Librairies
import numpy as np
import time
import pandas as pd
from scipy.interpolate import interp1d, interpn
import os
import argparse
from pyproj import Geod
from scipy.fftpack import fft,fftfreq,rfft,irfft,ifft
import scipy
from obspy.taup import TauPyModel
import matplotlib.pyplot as plt
import scipy.signal as signal
import cartopy.crs as ccrs

def read_synth(path_synth):
    S = np.load(path_synth)
    t_synth = S['time']
    N = len(t_synth)
    dt = t_synth[1]-t_synth[0]
    duration = N*dt
    fe = 1/float(dt)
    return N, fe, duration

def PSD_mod(file_model, N, fe, maxlag):
    goodnumber = int(N + maxlag)
    fre = fftfreq(goodnumber, 1/fe)

    M = np.load(file_model)
    F = M['F']
    lon = M['lon']
    lat = M['lat']
    freq = M['fq']
    df = M['df']
    ##grid
    
    ind_lon = np.squeeze(np.argwhere((lon>=-60) & (lon<30)))
    ind_lat = np.squeeze(np.argwhere((lat>=-30) & (lat<90)))
    lat = lat[ind_lat]
    lon = lon[ind_lon]
    Ftrunc = F[ind_lon, :, :]
    F = Ftrunc[:, ind_lat, :]
    print(F.shape)
    
    path = '/Users/tomasetl/Documents/code/kernel/'
    filename = 'kernel_KZ_MY_7_dirac.npz'
    lonmin = np.min(lon)
    lonmax = np.max(lon)
    latmin = np.min(lat)
    latmax = np.max(lat)
    extent = (lonmin, lonmax, latmin, latmax)
    lon_contour, lat_contour, ker = contour(path, filename, extent)
    
    plt.close('all')
    fig, ax = plt.subplots(figsize=(16, 9))
    im1 = ax.pcolormesh(lon, lat, F[:, :, 0].T, alpha = 0.9, cmap='viridis', shading='auto')
    im2 = ax.contour(lon_contour, lat_contour, ker, cmap = 'gist_gray', shading='auto')
    plt.title("source")
    
    bar1 = fig.colorbar(im1)
    bar2 = fig.colorbar(im2)
    plt.xlabel('longitude (°)')
    plt.ylabel('latitude (°)')
    bar1.set_label('source value')
    bar2.set_label('contour kernel')
    plt.grid()
    plt.show()
    
    ## Add limit bounds points for interpolation 
    F_0 = np.zeros((len(lon), len(lat)))
    F_1 = np.zeros((len(lon), len(lat)))
    freq = np.insert(freq,0, 0)
    freq = np.append(freq,1)
    F = np.dstack((F, F_1))
    F = np.dstack((F_0, F))
    
    if0 = np.squeeze(np.argwhere(fre == 0.0))
    if1 = np.squeeze(np.argwhere(fre == 1.0))

    f_new = fre[if0:if1]
    F_new = np.zeros((len(lon), len(lat), len(fre)))
    f_out = interp1d(freq, F, axis=2, kind='slinear')
    spectrum = f_out(f_new)
    F_new[:, :, if0:if1 ] = spectrum
    F_new[:, :, (goodnumber - if1):(goodnumber-if0)] = np.flip(spectrum, axis=2)
    return np.square(F_new), lon, lat, fre

def gaus2d(x=0, y=0, mx=-34, my=62, sx=2, sy=2):
    return 1. / (2. * np.pi * sx * sy) * np.exp(-((x - mx)**2. / (2. * sx**2.) + (y - my)**2. / (2. * sy**2.)))

def custom_source(N, fe, kind, maxlag):
    lon = np.arange(-35, -25)
    lat = np.arange(58, 68)
    n , m = len(lon), len(lat)
    ind_lat_src = np.argwhere(lat == 63.0)
    ind_lon_src = np.argwhere(lon == -30.0)
    path = '/Users/tomasetl/Documents/code/kernel/'
    filename = 'kernel_KZ_MY_7_dirac.npz'
    lonmin = np.min(lon)
    lonmax = np.max(lon)
    latmin = np.min(lat)
    latmax = np.max(lat)
    extent = (lonmin, lonmax, latmin, latmax)
    
    goodnumber = int(N+maxlag)
    F = np.zeros((n,m, goodnumber))
    if kind == 'gauss':
        x, y = np.meshgrid(lon, lat) # get 2D variables instead of 1D
        z = gaus2d(x, y, mx = -30, my = 63)
        for k in range(goodnumber):
            F[:, :, k] = z
    
    elif kind == 'uniform':
        F = np.ones((n, m, goodnumber))
    
    elif kind == 'point':
        F[ind_lon_src, ind_lat_src, :] = 1
    
    fre = fftfreq(N+maxlag, 1/fe)

    lon_contour, lat_contour, ker = contour(path, filename, extent)

    plt.close('all')
    fig, ax = plt.subplots(figsize=(16, 9))
    im1 = ax.pcolormesh(lon, lat, F[:, :, 0].T, alpha = 0.9, cmap='viridis', shading='auto')
    im2 = ax.contour(lon_contour, lat_contour, ker, cmap = 'gist_gray')
    plt.title("source")
    
    bar1 = fig.colorbar(im1)
    bar2 = fig.colorbar(im2)
    plt.xlabel('longitude (°)')
    plt.ylabel('latitude (°)')
    bar1.set_label('source value')
    bar2.set_label('contour kernel')
    plt.grid()
    plt.show()

    return F, lon, lat, fre

def read_DF(sta_file, staA, staB):
    df = pd.read_csv(sta_file, sep='\t', header=None, index_col=False,
    names=['idA', 'idB', 'latA', 'latB', 'lonA', 'lonB', 'elevA', 'elevB', 'depthA', 'depthB', 'dist'],
    dtype={'idA':str, 'idB':str, 'latA':float, 'latB':float, 'lonB':float, 'latB':float})
    condition = np.logical_and((df.idA == staA),(df.idB==staB))
    df_select = df[condition]
    print(df_select)
    idA = np.array(df_select.idA)
    idB = np.array(df_select.idB)
    distAB = np.array(df_select.dist)
    latA = np.array(df_select.latA)
    lonA = np.array(df_select.lonA)
    latB = np.array(df_select.latB)
    lonB = np.array(df_select.lonB)
    nb_pairs = len(idA)
    return latA, lonA, latB, lonB, nb_pairs, distAB

def distance_to_source(lon, lat, lonsta, latsta):
    R = 6371e3
    geoid = Geod(ellps="WGS84")
    (lat_grid, lon_grid) = np.meshgrid(lat, lon)
    lonSTA = np.ones((len(lon), len(lat)))*lonsta
    latSTA = np.ones((len(lon), len(lat)))*latsta
    (az, baz, distance_in_metersS) = geoid.inv(lon_grid, lat_grid, lonSTA, latSTA, radians=False)
    distanceS = np.round(180.0*distance_in_metersS/(R*np.pi)).astype(int)
    return distanceS

def matrix_GF(lon, lat, N, path_synth, distanceS, maxlag):
    goodnumber = int(N + maxlag)
    S = np.load(path_synth)
    WF = S['WF']
    synth = np.zeros((len(lon), len(lat), goodnumber))
    S_synth = np.empty((len(lon), len(lat), goodnumber)).astype(complex)
    for i in range(len(lon)):
        for j in range(len(lat)):
            distS = distanceS[i, j]
            if distS == 180:
                distS = 179
            synth[i, j, maxlag:goodnumber] = WF[distS]
    S_synth = scipy.fft.fft(synth, n = goodnumber, axis = 2)
    return S_synth


def matrix_GFconj(lon, lat, N, path_synth, distanceS, maxlag):
    goodnumber = int(N + maxlag)
    S = np.load(path_synth)
    WF = S['WF']
    synth = np.zeros((len(lon), len(lat), goodnumber))
    S_synth = np.zeros((len(lon), len(lat), goodnumber)).astype(complex)
    for i in range(len(lon)):
        for j in range(len(lat)):
            distS = distanceS[i, j]
            if distS == 180:
                distS = 179
            synth[i, j, 0:N] = WF[distS]
    S_synth = fft(synth, n = goodnumber, axis = 2)
    return S_synth.real-1j*S_synth.imag

def matrix_dirac(lon, lat, N, distanceS, phase, fe, maxlag):
    goodnumber = int(N + maxlag)
    M = np.load('%s_arr.npz'%phase)
    dist = M['arr'][:, 0]
    arr = M['arr'][:, 1]
    f_P = interp1d(dist, arr)
    ind_arr = f_P(distanceS)
    ind_arr = ind_arr*fe
    synth = np.zeros((len(lon), len(lat), goodnumber))
    S_synth = np.zeros((len(lon), len(lat), goodnumber)).astype(complex)
    for i in range(len(lon)):
        for j in range(len(lat)):
            if ind_arr[i, j] == -1:
                continue
            else:
                synth[i, j, maxlag:goodnumber] = signal.unit_impulse(N, int(ind_arr[i, j]))
    S_synth = fft(synth, axis = 2, overwrite_x=True)
    return S_synth

def matrix_dirac_conj(lon, lat, N, distanceS, phase, fe, maxlag):
    goodnumber = int(N + maxlag)
    M = np.load('%s_arr.npz'%phase)
    dist = M['arr'][:, 0]
    arr = M['arr'][:, 1]
    f_P = interp1d(dist, arr)
    ind_arr = f_P(distanceS)
    ind_arr = ind_arr*fe
    synth = np.zeros((len(lon), len(lat), goodnumber))
    S_synth = np.zeros((len(lon), len(lat), goodnumber)).astype(complex)
    for i in range(len(lon)):
        for j in range(len(lat)):
                if ind_arr[i, j] == -1:
                    continue
                else:
                    synth[i, j, 0:N] = signal.unit_impulse(N, int(ind_arr[i, j]))
    S_synth = fft(synth, axis = 2, overwrite_x=True)
    S_synth.imag *= -1
    return S_synth


def matrix_pulse(lon, lat, N, distanceS, phase, fe, maxlag):
    goodnumber = int(N + maxlag)
    M = np.load('%s_arr.npz'%phase)
    dist = M['arr'][:, 0]
    arr = M['arr'][:, 1]
    f_P = interp1d(dist, arr)
    ind_arr = f_P(distanceS)
    ind_arr = ind_arr*fe
    synth = np.zeros((len(lon), len(lat), goodnumber))
    S_synth = np.zeros((len(lon), len(lat), goodnumber)).astype(complex)
    for i in range(len(lon)):
        for j in range(len(lat)):
            if ind_arr[i, j] == -1:
                continue
            else:
                synth[i, j, maxlag:goodnumber] = signal.fftconvolve(signal.unit_impulse(N, int(ind_arr[i,j])), signal.ricker(N, 4.0), mode='same')
    S_synth = fft(synth, n = goodnumber, axis = 2)
    return S_synth

def matrix_pulse_conj(lon, lat, N, distanceS, phase, fe, maxlag):
    goodnumber = int(N + maxlag)
    M = np.load('%s_arr.npz'%phase)
    dist = M['arr'][:, 0]
    arr = M['arr'][:, 1]
    f_P = interp1d(dist, arr)
    ind_arr = f_P(distanceS)
    ind_arr = ind_arr*fe
    synth = np.zeros((len(lon), len(lat), goodnumber))
    S_synth = np.zeros((len(lon), len(lat), goodnumber)).astype(complex)
    for i in range(len(lon)):
        for j in range(len(lat)):
            if ind_arr[i, j] == -1:
                continue
            else:
                synth[i, j, 0:N] = signal.fftconvolve(signal.unit_impulse(N, int(ind_arr[i,j])), signal.ricker(N, 4.0), mode='same')
    S_synth = fft(synth, axis = 2, overwrite_x=True)
    S_synth.imag *= -1
    return S_synth

def ctp_xcorr(trace01, trace02,maxlag): # cross-correlation
    lentrace=len(trace01)
    goodnumber = int(maxlag+lentrace)
    tr2 = np.zeros(goodnumber)
    tr2[0:lentrace] = trace02
    tr2 = scipy.fftpack.fft(tr2,overwrite_x=True)
    tr2.imag *= -1
    tr1 = np.zeros(goodnumber)
    tr1[maxlag:int(maxlag+lentrace)]= trace01
    tr2 *= scipy.fftpack.fft(tr1,overwrite_x=True)
    tr2[np.isnan(tr2)] = 0.0+0.0j;
    tr2[np.isinf(tr2)] = 0.0+0.0j;
    return (scipy.fftpack.ifft(tr2,overwrite_x=True)[0:2*maxlag+1].real)

def contour(path, filename, extent):
    (lonmin, lonmax, latmin, latmax) = extent
    path_file = path + filename
    M = np.load(path_file)
    lon = M['x'][0]
    lat = M['y'].T[0]
    ker = M['z']
    ind_lon = np.squeeze(np.argwhere((lon>= lonmin ) & (lon<= lonmax)))
    ind_lat = np.squeeze(np.argwhere((lat>=latmin) & (lat<= latmax)))
    lat = lat[ind_lat]
    lon = lon[ind_lon]
    ztrunc = ker[ind_lat, :]
    z = ztrunc[:, ind_lon]
    return lon, lat, z

if __name__ == "__main__":
    """
    N = 7200
    trace1 = signal.unit_impulse(N, 2428)
    plt.plot(trace1)
    plt.show()
    plt.plot(trace2)
    plt.show()
    trace2 = signal.unit_impulse(N, 4488)
    maxlag = 7200
    corr = ctp_xcorr(trace1, trace2, maxlag)
    plt.plot(corr)
    plt.show()

    exit()
    """
    model = TauPyModel(model='prem')
    dist = np.arange(181)

    arrival_P = np.empty((181, 2))
    arrival_PP = np.empty((181, 2))
    for i, d in enumerate(dist):
        arrP = model.get_travel_times(source_depth_in_km=0, distance_in_degree=d, phase_list=['P'])
        arrPP = model.get_travel_times(source_depth_in_km=0, distance_in_degree=d, phase_list=['PP'])
        arrival_P[i, 0] = d
        arrival_PP[i, 0] = d
        try:
            arrival_P[i, 1] = arrP[0].time
        except:
            arrival_P[i, 1] = -1
        try:
            arrival_PP[i, 1] = arrPP[0].time
        except:
            arrival_PP[i, 1] = -1
    plt.plot(arrival_P[:, 0], arrival_P[:,1], 'k+')
    plt.plot(arrival_PP[:, 0], arrival_PP[:, 1], 'r+')
    plt.show()
    print(arrival_P)

    np.savez("P_arr.npz", arr = arrival_P)
    np.savez("PP_arr.npz", arr = arrival_PP)