#!usr/bin/env/python3 

__author__ = "Lisa Tomasetto"
# inspired by a code from Ruohan ZHANG
__copyright__ = "Copyright 2022, UGA"
__credits__ = ["Lisa Tomasetto"]
__version__ = "1.0"
__maintainer__ = "Lisa Tomasetto"
__email__ = "lisa.tomasetto@univ-grenoble-alpes.fr"
__status__ = "Production"


""" This script aims at generating Green's Functions for secondary microseism body waves modelling from axisem and target specific body waves phases."""

#######################################################################################################
# Libraries
import matplotlib.pyplot as plt
import numpy as np
import obspy
from obspy.signal.trigger import classic_sta_lta, plot_trigger, z_detect, carl_sta_trig, delayed_sta_lta
from obspy.taup import TauPyModel
import math
from scipy import signal

def seismograms_for_mt(st, az, m_rr, m_tt, m_pp, m_rt, m_rp, m_tp):
    # Convert to radian.
    az = np.deg2rad(az)
    # shortcuts to the data.
    TSS = st.select(channel="TSS")[0].data
    ZSS = st.select(channel="ZSS")[0].data
    RSS = st.select(channel="RSS")[0].data
    TDS = st.select(channel="TDS")[0].data
    ZDS = st.select(channel="ZDS")[0].data
    RDS = st.select(channel="RDS")[0].data
    ZDD = st.select(channel="ZDD")[0].data
    RDD = st.select(channel="RDD")[0].data
    ZEP = st.select(channel="ZEP")[0].data
    REP = st.select(channel="REP")[0].data
    
    # Apply formula from Minson and Dreger, 2008.
    Z = m_tt * (ZSS / 2 * np.cos(2 * az) - ZDD / 6 + ZEP / 3) + \
        m_pp * (-ZSS / 2 * np.cos(2 * az) - ZDD / 6 + ZEP / 3) + \
        m_rr * (ZDD / 3 + ZEP / 3) + \
        m_tp * (ZSS * np.sin(2 * az)) + \
        m_rt * (ZDS * np.cos(az)) + \
        m_rp * (ZDS * np.sin(az))
    """    
    R = m_tt * (RSS / 2 * np.cos(2 * az) - RDD / 6 + REP / 3) + \
        m_pp * (-RSS / 2 * np.cos(2 * az) - RDD / 6 + REP / 3) + \
        m_rr * (RDD / 3 + REP / 3) + \
        m_tp * (RSS * np.sin(2 * az)) + \
        m_rt * (RDS * np.cos(az)) + \
        m_rp * (RDS * np.sin(az))
    """
    """    
    T = m_tt * (TSS / 2 * np.sin(2 * az)) - \
        m_pp * (TSS / 2 * np.sin(2 * az)) - \
        m_tp * (TSS * np.cos(2 * az)) + \
        m_rt * (TDS * np.sin(az)) - \
        m_rp * (TDS * np.cos(az))
    """    
    tr_z = obspy.Trace(data=Z, header=st[0].stats)
    tr_z.stats.channel = "BHZ"
    #tr_r = obspy.Trace(data=R, header=st[0].stats)
    #tr_r.stats.channel = "BHR"
    #tr_t = obspy.Trace(data=T, header=st[0].stats)
    #tr_t.stats.channel = "BHT"
    
    return obspy.Stream(traces=[tr_z])

# Parameters
model = TauPyModel(model="prem")

origin_time = "2014-12-09T00:00:00.000" # Origin time of the source
src_lat = 0 # source latitude
src_lon = 0 # source longitude
src_depth = 0 # source depth
src_force = 1,0,0  # source force mechanism, Frr, Ftt, Fpp
rcv_lat = 0 # receiver latitude
dd = 1 # distance step
dist_range = np.arange(dd, 180+dd, dd)


#m_rr, m_tt, m_pp, m_rt, m_rp, m_tp = (-1.0/np.sqrt(6), -1.0/np.sqrt(6), 2.0/np.sqrt(6), 0.0, 0.0, 0.0)
data_synth = np.zeros((len(dist_range), 7200))
for i,d in enumerate(dist_range):
    pb_dist = [10, 45, 90, 135 ,180]
    print(f"{d} / 180", end= "\r", flush=True)
    ## Green's Function not used for now
    #gf = obspy.read("http://service.iris.edu/irisws/syngine/1/query?model=prem_i_2s&greensfunction=1&sourcedistanceindegrees=%d&sourcedepthinmeters=0&format=miniseed&dt=0.5"%d)
                          #"model=ak135f_1s&sourcelatitude=%d&sourcelongitude=%d&"%(src_lat, src_lon)+
                          #"sourcedepthinmeters=%d&sourceforce=1,0,0&sourcewidth=0&"%src_depth+
                          #"receiverlatitude=%d&receiverlongitude=%d&components=Z&"%(rcv_lat, d)+
                          #"&units=displacement&format=miniseed&nodata=404")
    #st_gf = seismograms_for_mt(gf, az=-90.0, m_rr=m_rr, m_tt=m_tt, m_pp=m_pp, m_rt=m_rt, m_rp=m_rp, m_tp=m_tp)
    st_synth = obspy.read("http://service.iris.edu/irisws/syngine/1/query?"+
                          "model=ak135f_1s&sourcelatitude=%d&sourcelongitude=%d&"%(src_lat, src_lon)+
                          "sourcedepthinmeters=%d&sourceforce=1,0,0&sourcewidth=0&"%src_depth+
                          "receiverlatitude=%d&receiverlongitude=%d&components=Z&"%(rcv_lat, d)+
                          "&units=displacement&format=miniseed&nodata=404")

    tr = st_synth[0]
    tr.decimate(factor=5, strict_length=False, no_filter=True)
    start = tr.stats.starttime
    end = tr.stats.endtime
    tr = tr.trim(start, start + 7200)
    fe = tr.stats.sampling_rate
    npts = 7200
    dt = tr.stats.delta
    tr_test = tr.data[0:7200]
    time = np.arange(0, npts*dt, dt)
    #print(len(time))
    #exit()
    try:
        arr_P = model.get_travel_times(source_depth_in_km=0, distance_in_degree=d, phase_list=['P'])
        ind_arr_P = int(arr_P[0].time*fe)
        t_P = arr_P[0].time
    except:
        print("no P phase for distance %02d"%d)
        t_P = 0
    try:
        arr_PP = model.get_travel_times(source_depth_in_km=0, distance_in_degree=d, phase_list=['PP'])
        ind_arr_PP = int(arr_PP[-1].time*fe)
        t_PP = arr_PP[-1].time
    except:
        print("no PP phase for distance %02d"%d)
        continue
    
    if d in pb_dist:
        print('--%d--'%d)
        plt.plot(time, tr_test)
        plt.axvline(x=t_P, color='red', label='arrival P')
        plt.axvline(x=t_PP, color='orange', label='arrival PP')
        plt.show()
    try:
        DT = t_PP-t_P
    except: 
        DT = 1000
    width = int((DT+100)*fe)
    tukey = signal.windows.tukey(width, alpha=0.1, sym=True)

    if d in pb_dist:
        plt.plot(tukey, label='tukey window')
        plt.legend()
        plt.show()

    dirac = np.zeros(npts)
    index = int((DT/2+t_P)*fe)
    dirac[index] = 1
    window = signal.fftconvolve(dirac, tukey, mode='same')

    if d in pb_dist:
        plt.plot(time, dirac, 'k', label='dirac')
        plt.plot(time, window, 'r', label='tukey')
        plt.axvline(x=t_P, color='red', label='arrival P')
        plt.axvline(x=t_PP, color='orange', label='arrival PP')
        plt.legend()
        plt.show()

    tr_test = tr_test*window

    if d in pb_dist:
        plt.plot(time, tr.data[0:7200], 'k', label='raw trace')
        plt.plot(time, tr_test, 'b', label='tapered')
        plt.axvline(x=t_P, color='red', label='arrival P')
        plt.axvline(x=t_PP, color='orange', label='arrival PP')
        plt.legend()
        plt.show()
        
    data_synth[i, :] = tr_test[0:7200]
np.savez("../waveforms/GF_4Hz.npz", WF = data_synth, dist = dist_range, time=time)

plt.figure()
plt.pcolormesh(data_synth.T, vmin = -1e-19, vmax = 1e-19, cmap='seismic')
plt.show()