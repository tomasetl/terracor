#!/usr/bin/env python3

__author__ = "Lisa Tomasetto"
__copyright__ = "Copyright 2024, UGA"
__credits__ = ["Lisa Tomasetto"]
__version__ = "1.0"
__maintainer__ = "Lisa Tomasetto"
__email__ = "lisa.tomasetto@univ-grenoble-alpes.fr"
__status__ = "Developpement"

"""Functions to generate synthetic cross-correlations from secondary microseisms sources."""

##########################################################################

## Librairies
import numpy as np
import matplotlib.pyplot as plt
import h5py 
import xarray as xr
import scipy 
import time as ttime
import itertools
import multiprocessing

from scipy.fftpack import fftfreq,ifft
from datetime import datetime
from pyproj import Geod
from tqdm import tqdm

## Constants
radius_earth = 6371e3  # Earth's Radius in meters

## Functions
### Open AxiSEM archive
def open_axisem(dist, path_file_axisem='/bettik/tomasetl/axicorr/taper_vertforce_iasp91_1.s_256c_3600.s.h5', comp='Z'):
    """ Reads an AxiSEM .h5 archive in a 1D model given its path and a distance,
    and returns sampling frequency, time vector and trace at the given distance.
    Input:
    path_file_axisem: path of axisem .h5 archive
    dist : distance of interest
     
    Output:
    fe_iasp: sampling frequency 
    time_iasp: time vector of the archive
    trace_synth: synthetic trace at the given distance."""
    dist = np.round(dist, 1)
    h5_file = h5py.File(path_file_axisem)
    trace_synth = h5_file['L']['SYNTH%03d.00'%(dist*10)][comp][:].astype(np.single)
    h5_file.close()
    return trace_synth

def get_synthetic_info(path_file_axisem='/bettik/tomasetl/axicorr/taper_vertforce_iasp91_1.s_256c_3600.s.h5', comp='Z'):
    """
        Function to get synthetic info from a specified h5 file and return fe, time, and N.
        Parameters:
        - path_file_axisem: str, default='/bettik/tomasetl/axicorr/taper_vertforce_iasp91_1.s_256c_3600.s.h5'
        - comp: str, default='Z'
        Returns:
        - fe: float
        - time: numpy.ndarray
        - N: int
    """
    h5_file = h5py.File(path_file_axisem)
    fe = h5_file['_metadata']['fe'][()]
    dist = 0.1
    trace = h5_file['L']['SYNTH%03d.00'%(dist*10)][comp][:].astype(np.single)
    time = np.arange(0, len(trace)*1/fe, 1/fe).astype(np.single)
    N = len(trace)
    h5_file.close()
    return fe, time, N

def open_test(N, fe, lon_source, lat_source):

    lon = np.arange(-180, 180, 0.5).astype(np.single)
    lat = np.arange(-78, 80, 0.5).astype(np.single)
    index_lon = np.argwhere(lon == lon_source).astype(np.single)
    index_lat = np.argwhere(lat == lat_source).astype(np.single)

     ## Spectrum Output
    lenght_spectrum_hermitian = int(2*N)  # must be Hermitian
    frequency = np.squeeze(fftfreq(lenght_spectrum_hermitian, 1/fe)).astype(np.single)
    ## Build PSD
    psd = np.zeros((len(lon), len(lat), lenght_spectrum_hermitian)).astype(np.single)
    psd[index_lon, index_lat, :] = 1
    psd = xr.DataArray(psd, coords={'lon':lon, 'lat': lat, 'freq': frequency})

    return psd

def open_model(path_file_WW3, date_vect, N, fe, lon_slice=slice(-180, 180), lat_slice=slice(-78, 80), normalization_factor = 10e20):
    """ Reads the WW3 model for ambient noise sources at a specific time and date. 
    Returns the PSD of the model on the whole grid.
    Input:
    path_file_WW3: path of WW3 model archive in N.s^{1/2}
    date_vect: date and time vector [YEAR, MONTH, DAY, HOUR]

    Output:
    Returns Hermitian PSD of the given model in N^2.s with dimensions (dim_lon, dim_lat, 2*dim_freq+1).
    """

    year = date_vect[0]
    month = date_vect[1]
    day = date_vect[2]
    hour = date_vect[3]

    ## Open WW3 model 
    ds = xr.open_dataset(path_file_WW3) #, chunks={'time': 1, 'lon': 30, 'lat': 40}, parallel=True,combine='by_coords')  # mfdataset  
    ww3_data = ds.force.sel(time=datetime(year, month, day, hour), lon=lon_slice, lat= lat_slice).astype(np.single)/normalization_factor

    ww3_data = ww3_data.dropna(dim='lon', how='all').dropna(dim='lat', how='all')
    del ds
    ww3_data = ww3_data.where(np.isfinite(ww3_data))

    ## Spectrum Output
    lenght_spectrum_hermitian = int(2*N)  # must be Hermitian
    frequency = np.squeeze(fftfreq(lenght_spectrum_hermitian, 1/fe)).astype(np.single)


    ## Interpolate over frequency range
    force_0 = xr.DataArray(np.zeros((len(ww3_data.lon), len(ww3_data.lat), 1)).astype(np.single), coords={'lon':ww3_data.lon, 'lat': ww3_data.lat, 'freq': [0]})
    force_2 = xr.DataArray(np.zeros((len(ww3_data.lon), len(ww3_data.lat), 1)).astype(np.single), coords={'lon':ww3_data.lon, 'lat': ww3_data.lat, 'freq': [1.]})
    ww3_data = xr.concat([force_0, ww3_data, force_2], dim='freq').astype(np.single)
    force_spectrum = ww3_data.interp(freq=frequency[np.logical_and(frequency>=0., frequency <= 1.)], method="slinear", kwargs={"fill_value": 0.}).astype(np.single)
    del ww3_data

    return force_spectrum**2  # in N^2.s

def distance_to_station(lon, lat, lon_s=0, lat_s=90, radius_earth=6371e3):
    """ Computes the distance of every point of the model to station of coordinates (lonS, latS)
    Input:
    lon: longitudes of the grid
    lat: latitude of the grid
    lon_s: station longitude 
    lat_s: station latitude
    radius_earth : the radius of the Earth
    
    Output:
    matrix of dimensions dim(lon) x dim(lat) 
    """
    geoid = Geod(ellps="WGS84")  # set reference ellipsoid
    (lat_grid, lon_grid) = np.meshgrid(lat, lon)  # grid used

    lonSTA = np.ones(lon_grid.shape)*lon_s
    latSTA = np.ones(lat_grid.shape)*lat_s
    (_, _, distance_in_metersS) = geoid.inv(lon_grid, lat_grid, lonSTA, latSTA, radians=False)
    distanceS = 180.0*distance_in_metersS/(radius_earth*np.pi)
    distanceS = xr.DataArray(distanceS.astype(np.single), coords={'lon': lon.astype(np.single), 'lat': lat.astype(np.single)}, name = 'distance', attrs={'units': '°'})
    return distanceS.astype(np.single)

def matrix_GF(lon, lat, N, path_file_axisem, distance_s, conjugate = False):
    """
    Generates a synthetic seismogram (Green's Functions) matrix in frequency domain
    using the given lon, lat, N, path_file_axisem, distance_s, and optional conjugate flag.
    Returns the synthetic seismogram matrix.

    Input:
    lon: longitude of the grid
    lat: latitude of the grid
    N: number of samples
    path_file_axisem: path to the axisem tapered file
    distance_s: distance matrix
    conjugate: conjugate flag

    Output:
    synth: synthetic seismogram
    """
    length_spectrum_hermitian = int(2*N)
    synth = np.zeros((len(lon), len(lat), length_spectrum_hermitian)).astype(np.single)
    S_synth = np.zeros((len(lon), len(lat), length_spectrum_hermitian)).astype(np.csingle)
    distance_s = np.round(distance_s, decimals=1).astype(np.single)
    distance = np.arange(0, 180.1, 0.1).astype(np.single)
    for dist in distance:
        index_lon, index_lat = np.where(distance_s.data == dist)
        if len(index_lon)>0:
            trace = open_axisem(dist, path_file_axisem, comp='Z')
        else: 
            continue
        if conjugate:
            synth[index_lon, index_lat, 0:N] = trace.data
        else:
            synth[index_lon, index_lat, N:2*N] = trace.data
    S_synth = scipy.fft.fft(synth.data, n = length_spectrum_hermitian, axis = 2, overwrite_x=True)
    S_synth = S_synth[:, :, 0:N//2+1]
    if conjugate:
        return S_synth.real-1j*S_synth.imag
    else:
        return S_synth

def compute_model_chunk(lon_inf, lon_sup, lat_inf, lat_sup, N, fe, date_vect, file_model, lon_staA, lat_staA, lon_staB, lat_staB, path_file_axi):
            """
            Compute correlation function between stations A and B for a chunk of the www3 model source.   

            Parameters:
            lon_inf (float): Lower longitude bound.
            lon_sup (float): Upper longitude bound.
            lat_inf (float): Lower latitude bound.
            lat_sup (float): Upper latitude bound.
            N (int): Number of points.
            fe (float): Sampling frequency.
            date_vect (array): Vector of dates.
            file_model (str): Model file.
            lon_staA (float): Longitude of station A.
            lat_staA (float): Latitude of station A.
            lon_staB (float): Longitude of station B.
            lat_staB (float): Latitude of station B.
            path_file_axi (str): Path to Axisem file.

            Returns:
            np.ndarray: Computed correlation function as a single precision complex array.
            """
            psd_model = open_model(path_file_WW3=file_model, date_vect=date_vect, N=N, fe=fe, lon_slice=slice(lon_inf, lon_sup), lat_slice=slice(lat_inf, lat_sup))
            psd_model.data = psd_model.data.astype(np.csingle)
            lon = psd_model.lon.astype(np.single)
            lat = psd_model.lat.astype(np.single)
            ## Distances
            distance_staA = distance_to_station(lon, lat, lon_s = lon_staA, lat_s = lat_staA)
            distance_staB = distance_to_station(lon, lat, lon_s = lon_staB, lat_s = lat_staB)
            ## Green's Functions spectrum
            psd_model *= matrix_GF(lon=lon, lat=lat, N=N, path_file_axisem=path_file_axi, distance_s=distance_staA, conjugate=True).astype(np.csingle)
            psd_model *= matrix_GF(lon=lon, lat=lat, N=N, path_file_axisem=path_file_axi, distance_s=distance_staB, conjugate=False).astype(np.csingle)
            del distance_staA, distance_staB, lon, lat 

            ## Sum along coordinates latitude and longitude
            return psd_model.sum(dim=['lon', 'lat']).data.astype(np.csingle)


def ccf_computation(coords_staA, coords_staB, path_file_axi, path_model, date_vect, normalization_factor = 10e20, comp='Z'):
    """
    A function to compute the cross-correlation function between two seismic stations. 
    It takes the coordinates of the stations, the path to the AxiSEM archive, the path to the model, 
    a vector of dates, a normalization factor, and a component parameter. 
    It returns the computed cross-correlation function and the corresponding time array.
    """
    t1 = ttime.perf_counter()
    ## Coordinates of stations
    lon_staA = coords_staA[0].astype(np.single)
    lat_staA = coords_staA[1].astype(np.single)

    lon_staB = coords_staB[0].astype(np.single)
    lat_staB = coords_staB[1].astype(np.single)

    ## Open AxiSEM archive
    fe, time, N = get_synthetic_info(path_file_axi)
    freq = np.squeeze(fftfreq(2*N, 1/fe)).astype(np.single)
    corr_f = np.zeros((2*N)).astype(np.csingle)

    ## Open WW3 PSD
    YEAR = date_vect[0]
    MONTH = date_vect[1]

    path_model += '/%d/'%YEAR
    file_model = path_model + 'model_%d%02d.nc'%(YEAR, MONTH)
    paramlist = []

    lon_slice = np.arange(-180, 181, 30)
    lat_slice = np.arange(-80, 81, 40)

    ## Cut into subgroups to avoid memory saturation
    for i in range(len(lon_slice)-1):
        for j in range(len(lat_slice)-1):
            lon_inf = lon_slice[i]
            lat_inf = lat_slice[j]
            lon_sup = lon_slice[i+1]
            lat_sup = lat_slice[j+1]
            paramlist += [(lon_inf, lon_sup, lat_inf, lat_sup, N, fe, date_vect, file_model, lon_staA, lat_staA, lon_staB, lat_staB, path_file_axi)]

    corr_f = np.zeros((2*N)).astype(np.csingle)
    results = np.zeros((len(paramlist), 2*N), dtype=np.csingle)
    with multiprocessing.Pool() as pool:
        results = pool.starmap(compute_model_chunk, paramlist)

    for result in results:
        corr_f[0:N//2+1] += result.data
        
    corr_f[3*N//2::] = np.flip(np.conj(corr_f[0:N//2])).astype(np.csingle)

    # fig,ax = plt.subplots(2, sharex=True)
    # ax[0].plot(freq, np.abs(corr_f), 'purple')
    # ax[0].set_xlabel('Frequency')
    # ax[0].set_ylabel('Amplitude')
    # ax[1].plot(freq, np.angle(corr_f), 'purple')
    # ax[1].set_xlabel('Frequency')
    # ax[1].set_ylabel('Phase')
    # plt.show()

    ## Correlation in Time Domain
    corr = ifft(corr_f.data)[0:2*N+1].real.astype(np.single)
    time_corr = np.arange(-N, N).astype(np.single)*1/fe
    corr = xr.DataArray(corr, dims=['time'], coords={'time': time_corr}, name='synthetic correlation')
    del corr_f    
    return corr, time_corr


def read_DF(sta_file, staA, staB):
    df = pd.read_csv(sta_file, sep='\t', header=None, index_col=False,
    names=['idA', 'idB', 'latA', 'latB', 'lonA', 'lonB', 'elevA', 'elevB', 'depthA', 'depthB', 'dist'],
    dtype={'idA':str, 'idB':str, 'latA':float, 'latB':float, 'lonB':float, 'latB':float})
    condition = np.logical_and((df.idA == staA),(df.idB==staB))
    df_select = df[condition]
    print(df_select)
    idA = np.array(df_select.idA)
    idB = np.array(df_select.idB)
    distAB = np.array(df_select.dist).astype(np.single)
    latA = np.array(df_select.latA).astype(np.single)
    lonA = np.array(df_select.lonA).astype(np.single)
    latB = np.array(df_select.latB).astype(np.single)
    lonB = np.array(df_select.lonB).astype(np.single)
    nb_pairs = len(idA)
    return latA, lonA, latB, lonB, nb_pairs, distAB
