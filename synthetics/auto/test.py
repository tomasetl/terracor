#!/usr/bin/env python3

__author__ = "Lisa Tomasetto"
__copyright__ = "Copyright 2022, UGA"
__credits__ = ["Lisa Tomasetto"]
__version__ = "1.0"
__maintainer__ = "Lisa Tomasetto"
__email__ = "lisa.tomasetto@univ-grenoble-alpes.fr"
__status__ = "Production"

"""This scripts reads dictionnary containing the station pairs name at each timestep and launches bash scripts to run synthetic cross-correlations"""
###########################################################################
## Librairies
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import h5py
import time
import os

# from obspy.signal import filter
from obspy.taup import TauPyModel
from datetime import datetime
## Own Librairies
from synthetic_functions import *


# Read Metadata of Real Data
## File name
path_data = '/bettik/tomasetl/weather_bomb/KZ_MY/C1_04_xcorr___coher/'
file_corr_h5 = 'xcorr___000.h5'
path_file_data = path_data + file_corr_h5

## Open 
hf_data = h5py.File(path_file_data, 'r')
## Time windows
date1 = hf_data['md_c']['date1'][:]
date2 = hf_data['md_c']['date2'][:]
datetime1 = np.empty((len(date1))).astype(object)
datetime2 = np.empty((len(date2))).astype(object)

for i,d in enumerate(date1):
    datetime1[i] = datetime.fromtimestamp(int(d))
    datetime2[i] = datetime.fromtimestamp(int(date2[i]))
    print("number of dates:", len(datetime1))
    ## Time vector
    time_real = hf_data['md_c']['t'][()]
    tau = hf_data['md_c']['tau'][()]
    fe = 1/tau

    # Stations Info
    ## Stations ID
    id = hf_data['md']['id'][:]
    print("number of stations: ", len(id))
    ## Stations Coordinates
    lon_sta = hf_data['md']['lon'][:]
    lat_sta = hf_data['md']['lat'][:]
    depth = hf_data['md']['depth'][:]
    elev = hf_data['md']['elev'][:]

    ## Is this station pair computed at this date ?
    cc_nstack = np.squeeze(hf_data['cc_nstack'][:])

    ## Path Axisem

    path_axi = '/bettik/tomasetl/axicorr/'
    file_axi = 'taper_vertforce_iasp91_1.s_256c_3600.s.h5'
    path_file_axi = path_axi + file_axi

    ## Path model PSD
    path_model = '/summer/nocoda/tomasetl/python/MODEL_SYNTH'

    # ## Loop over stations

    for i in range(len(id)):
         idA = id[i][0]
         idB = id[i][1]
         print(idA, idB)
    

    YEAR = 2014
    MONTH = 12
    DAY = 9
    HOUR = 15

    date_vect = [YEAR, MONTH, DAY, HOUR]

    dset = pd.read_csv('./pairs_P.txt', sep='\t', names=['idA', 'idB', 'latA', 'latB', 'lonA', 'lonB', 'elevA', 'elevB', 'depthA', 'depthB', 'dist'])

    dset = dset[dset['idB']=='MY.KOM.00'].reset_index(drop=True)
    print(dset)

    for i in range(len(dset)):
        t1 = time.perf_counter()
        staA = dset['idA'][i]
        staB = dset['idB'][i]
        lonA = dset['lonA'][i]
        latA = dset['latA'][i]
        lonB = dset['lonB'][i]
        latB = dset['latB'][i]
        distAB = dset['dist'][i]
        t1 = time.perf_counter()
        corr, time_corr = ccf_computation(coords_staA=[lonA, latA], coords_staB=[lonB, latB], path_file_axi=path_file_axi, path_model=path_model, date_vect=date_vect)
        t2 = time.perf_counter()

        print("Computation time for station pair: ", staA, staB, " elapsed: ", t2-t1)
        print("cpu", os.cpu_count())
        ## Filter
        ## Expected Arrival Time
        model = TauPyModel(model="iasp91")
        arrivals = model.get_travel_times(source_depth_in_km=0, distance_in_degree=distAB, phase_list=["P"])
        t_P = arrivals[0].time
        
        # Plot
        plt.plot(time_corr, corr)
        plt.axvline(t_P, color='r')
        plt.show()
        plt.close()
    # ## Open Real CCF

    # path = '/bettik/tomasetl/weather_bomb/KZ_MY/C1_04_xcorr___coher/'
    # file_corr_h5 = 'xcorr___000.h5'
    # path_file = path + file_corr_h5

    # hf = h5py.File(path_file, 'r')
    # date1 = hf['md_c']['date1'] 
    # date2 = hf['md_c']['date2']

    # date1_np = np.empty((len(date1))).astype(object)
    # date2_np = np.empty((len(date2))).astype(object)

    # for i, date in enumerate(date1):
    #     date1_np[i] = datetime.datetime.fromtimestamp(date)
    #     date2_np[i] = datetime.datetime.fromtimestamp(date)
    
    # ## Select Time Window
    # index_time = np.squeeze(np.argwhere(np.logical_and(date1_np >= datetime.datetime(YEAR, MONTH, DAY, HOUR), datetime.datetime(YEAR, MONTH, DAY, HOUR+3) >= date2_np)))

    # ## Real CCF
    # corr_real = np.mean(hf['cc'][staA][staB]['ZZ'][index_time][:], axis = 0)*1e-12
    # ## Filter
    # corr_real = filter.bandpass(corr_real, freqmin=0.08, freqmax=0.5, df = 4, corners=4, zerophase=True)
    # time_real = hf['md_c']['t'][:]

    # plt.plot(time_real, corr_real, color='b')
    # corr.plot()
    # plt.axvline(x=t_P, color='r', linestyle='--', label = 'P iasp91')
    # plt.show()
