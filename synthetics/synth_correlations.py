#!/usr/bin/env/python3

__author__ = "Lisa Tomasetto"
__copyright__ = "Copyright 2022, UGA"
__credits__ = ["Lisa Tomasetto"]
__version__ = "1.0"
__maintainer__ = "Lisa Tomasetto"
__email__ = "lisa.tomasetto@univ-grenoble-alpes.fr"
__status__ = "Production"


""" This script aims at generating synthetic correlations for secondary microseism body waves modelling,
to be coupled with the WW3 model by Ifremer"""

#######################################################################

##Librairies
import numpy as np
import time
import pandas as pd
from scipy.interpolate import interp1d, interpn
import os
import argparse
import matplotlib.pyplot as plt
#from pyproj import Geod
import scipy
from scipy.signal import hilbert, butter, filtfilt
from scipy.fftpack import fft,fftfreq,rfft,irfft,ifft

## Own Libs
from subfunctions_GF import *


## Parameters
parser = argparse.ArgumentParser(description='manual to this script')
parser.add_argument('--iyear', type=int, default=32)
parser.add_argument('--imonth', type=int, default=32)
parser.add_argument("--iday", type=int, default=32)
parser.add_argument("--ih", type=int, default=32)
parser.add_argument("--stafile", type=str, default="pairs.txt")
parser.add_argument("--staA", type=str, default="KZ.KUR01.00")
parser.add_argument("--staB", type=str, default="MY.KOM.00")
args = parser.parse_args()
iyear =args.iyear
imonth =args.imonth
iday = args.iday
ih = args.ih
sta_file = args.stafile
staA = args.staA
staB = args.staB

plot = True

## Constants
R = 6371e3  ## Earth's Radius in meters

## Open Synthetics
path_synth = "../waveforms/GF_4Hz.npz"
N, fe, duration = read_synth(path_synth)
print(N, fe, duration)
time = np.arange(N)*1/fe
maxlag = int(1800*fe)

freq = np.fft.fftfreq(N+maxlag, 1/fe)
freq = np.fft.fftshift(freq)

## Open Model

filename_model = "model_%d%02d%02dT%02d.npz"%(iyear, imonth, iday, ih)
file_model = "../model/" + filename_model
S_mod, lon, lat, fre = PSD_mod(file_model, N, fe, maxlag)
S_mod = np.nan_to_num(S_mod)

## Custom Source

#S_mod, lon, lat, fre = custom_source(N, fe, 'uniform', maxlag)

index_src = np.squeeze(np.argwhere(S_mod == np.max(S_mod)))
ind_lon_src, ind_lat_src = index_src[0][0], index_src[0][1]
if plot:
    plt.figure()
    plt.plot(freq, S_mod[ind_lon_src, ind_lat_src, :])
    plt.ylabel('PSD force(N^2)')
    plt.xlabel('frequency(Hz)')
    plt.title('WW3 model')
    plt.show()

## Read station pairs info

(latA, lonA, latB, lonB, nb_pairs, distAB) = read_DF(sta_file, staA, staB)
# Make distance to station A matrix and distance to station B matrix
distanceSA = distance_to_source(lon, lat, lonA, latA)
distanceSB = distance_to_source(lon, lat, lonB, latB)
dA = distanceSA[ind_lon_src, ind_lat_src]
dB = distanceSB[ind_lon_src, ind_lat_src]

# Create Green's Function matrix

GF_Aconj = matrix_GFconj(lon, lat, N, path_synth, distanceSA, maxlag)
GF_B = matrix_GF(lon, lat, N, path_synth, distanceSB, maxlag)

#GF_Aconj = matrix_pulse_conj(lon, lat, N, distanceSA, 'P', fe, maxlag)
#GF_B = matrix_pulse(lon, lat, N, distanceSB, 'PP', fe, maxlag)

#GF_Aconj = matrix_dirac_conj(lon, lat, N, distanceSA, 'P', fe, maxlag)
#GF_B = matrix_dirac(lon, lat, N, distanceSB, 'PP', fe, maxlag)

## Free Memory
del distanceSA, distanceSB, lon, lonA, lonB, lat, latA, latB, nb_pairs

if plot:
    fig, ax = plt.subplots(2, sharex=True)
    ax[0].plot(freq, np.abs(GF_Aconj[ind_lon_src, ind_lat_src, :]), 'purple')
    ax[0].set_xlabel('frequency (Hz)')
    ax[0].set_ylabel('amplitude')

    ax[1].plot(freq, np.angle(GF_Aconj[ind_lon_src, ind_lat_src, :]), 'k')
    ax[1].set_xlabel('frequency (Hz)')
    ax[1].set_ylabel('phase')
    fig.suptitle('Spectrum of A')
    plt.show()


if plot:
    fig, ax = plt.subplots(2, sharex=True)
    ax[0].plot(freq, np.abs(GF_B[ind_lon_src, ind_lat_src, :]), 'purple')
    ax[0].set_xlabel('frequency (Hz)')
    ax[0].set_ylabel('amplitude')

    ax[1].plot(freq, np.angle(GF_B[ind_lon_src, ind_lat_src, :]), 'k')
    ax[1].set_xlabel('frequency (Hz)')
    ax[1].set_ylabel('phase')
    fig.suptitle('Spectrum of B')
    plt.show()

## Create correlation
corr_mat_f = np.zeros(GF_B.shape).astype(complex)
corr_mat_f = GF_Aconj*S_mod*GF_B
#print(corr_mat_f, corr_mat_f.shape)
#Free memory
del GF_Aconj, GF_B, S_mod

## Sum along coordinates
interm = np.zeros((317, 7200)).astype(complex)
interm = np.sum(corr_mat_f, axis =0)
## Free memory
del corr_mat_f

corr_f = np.zeros(7200).astype(complex)
corr_f = np.sum(interm, axis=0)

fig, ax = plt.subplots(2, sharex=True)
ax[0].plot(freq, np.abs(corr_f), 'purple')
ax[0].set_xlabel('frequency (Hz)')
ax[0].set_xlabel('frequency(Hz)')
ax[0].set_ylabel('amplitude')

ax[1].plot(freq, np.angle(corr_f), 'k')
ax[1].set_xlabel('frequency(Hz)')
ax[1].set_ylabel('phase')
fig.suptitle('correlation frequency domain')
plt.show()

## ifft of matrix 
corr = np.zeros(corr_f.shape).astype(complex)
corr = ifft(corr_f)[0:2*maxlag+1].real

enveloppe = abs(signal.hilbert(corr))
time = np.arange(-maxlag, maxlag)*1/fe

## Free Memory
del interm, corr_f, freq

model = TauPyModel(model='prem')
try:
    arrP = model.get_travel_times(source_depth_in_km=0, distance_in_degree=dA, phase_list=['P'])
    arrPP = model.get_travel_times(source_depth_in_km=0, distance_in_degree=dB, phase_list=['PP'])
    t_P = arrP[0].time
    t_PP = arrPP[0].time
    t_diff = t_PP-t_P
except:
    t_diff = np.nan
test = model.get_travel_times(source_depth_in_km=0, distance_in_degree=distAB, phase_list=['P'])

t_theo = test[0].time

#print(t_PP-t_P)
if plot:
    plt.close('all')
    plt.figure()
    plt.title('correlation')
    plt.xlabel('time (s)')
    plt.ylabel('amplitude')
    plt.plot(time, corr, color='darkslategray', label='correlation')
    plt.plot(time, enveloppe, color='maroon', linestyle='--', label='enveloppe')
    plt.axvline(x = t_diff, color='tan', label='time difference', alpha=0.5)
    plt.axvline(x= t_theo, color='thistle', label='theoretical time')
    plt.legend()
    plt.grid()
    plt.show()
# Compare to theoretical arrival time


#np.savez('coor_%d%02d%02dT%02d_%s.npz'%(iyear, imonth, iday, ih, staA), synth = synthA, time = t_synth, fe = fe)
