clear all
close all
function gf=get_gf_fft_numerically(time,dist,vel,Q)
    % Compute the analytical GF between two points in a 2D homogeneous medium:
    %   time = time vector on which the GF will be interpolated : i.e [0:1:100] [s]
    %   dist = distance between the two points [km] : i.e 50
    %   vel  = velocity of the medium [km/s]        : ie. 3 km/s
    %   Q    = attenuation factor                   : i.e 50
    if isnan(dist) ; dist=0; end
    nt = numel(time);
    gf = zeros(1,nt);
    t0 = dist/vel;
    rho= 1 ;
    fe=1./(time(2)-time(1));
    w= 2*pi*[0:fe/nt:fe/2 -fe/2:fe/nt:-fe/nt];
    gf1=-i/(4*rho*vel^2);
    gf2= sqrt((2*vel)/(pi*dist)); % geom : marche mais pas correct : manque dependance en omega
    gf3 =exp(-i*w*dist/vel);
    gf4 =exp(-w*dist/(2*vel*Q));
    gf5 =exp(i*pi/4);
    gf=gf1.*gf2.*gf3.*gf4.*gf5;
    m0=ceil(numel(gf)/2);
    gf(m0+1:end) = conj(gf([m0:-1:2]));
    gf=real(ifft(gf));
end


time = linspace(0,40,1001);
fs   = 1./time(2);
x =linspace(0,20,200);
y =linspace(0,10,100);
[X,Y] = meshgrid(x,y);
GFS=zeros([length(time),fliplr(size(X))]);
GF1=GFS;
GF2=GFS;

[b,a] = butter(2,[1,3]/(fs/2),'bandpass');

x1 = 8;[~,ix1]=min(abs(x-x1));
y1 = 5;[~,iy1]=min(abs(y-y1));

x2 = 12;[~,ix2]=min(abs(x-x2));
y2 = 5;[~,iy2]=min(abs(y-y2));

xS = 18;[~,ixS]=min(abs(x-xS));
yS = 7;[~,iyS]=min(abs(y-yS));

master_gf_1S =filter(b,a,get_gf_fft_numerically(time,sqrt((xS-x1)^2+(yS-y1)^2),1,100));
master_gf_2S =filter(b,a,get_gf_fft_numerically(time,sqrt((xS-x2)^2+(yS-y2)^2),1,100));
master_gf_12 =filter(b,a,get_gf_fft_numerically(time,sqrt((x1-x2)^2+(y1-y2)^2),1,100));


for ix=1:length(x)
    ix
    for iy=1:length(y)
        dS = sqrt((x(ix)-xS)^2+(y(iy)-yS)^2);
        d1 = sqrt((x(ix)-x1)^2+(y(iy)-y1)^2);
        d2 = sqrt((x(ix)-x2)^2+(y(iy)-y2)^2);
        GFS(:,ix,iy) = filter(b,a,get_gf_fft_numerically(time,dS,1,100));
        GF1(:,ix,iy) = filter(b,a,get_gf_fft_numerically(time,d1,1,100));
        GF2(:,ix,iy) = filter(b,a,get_gf_fft_numerically(time,d2,1,100));
    end
end

%%

close all

disp(size(squeeze(GF1(1,:,:))))
for it = 1:10:size(GFS,1)
    clf
    subplot(3,1,1)
    plot(time,GF1(:,ix2,iy2));hold on
    plot(time(it),GF1(it,ix2,iy2),'ro')
    subplot(3,1,2:3)
    pcolor(X,Y, squeeze(GF1(it,:,:))')
    shading flat
    axis equal
    %colormap(nori_color('bwr128'))
    caxis([-1,1]*0.01)

    hold on
    plot(x1,y1,'ro','MarkerSize',10)
    plot(x2,y2,'bo','MarkerSize',10)
    plot(xS,yS,'kd','MarkerSize',10)
    pause
end

%%

close all

C=zeros([(length(time))*2-1,fliplr(size(X))]);


for ix=1:length(x)
    ix
    for iy=1:length(y)
        C(:,ix,iy) = conv(GF1(:,ix,iy),GF2(:,ix,iy),"full");
    end
end

for it = 1:5:size(C,1)
    clf
    subplot(3,1,1)
    plot(C(:,ix2,iy2));hold on
    plot(it,C(it,ix2,iy2),'ro')
    subplot(3,1,2:3)
    pcolor(X,Y,squeeze(C(it,:,:))')
    shading flat
    axis equal
    %colormap(nori_color('bwr128'))
    caxis([-1,1]*0.01)

    hold on
    plot(x1,y1,'ro','MarkerSize',10)
    plot(x2,y2,'bo','MarkerSize',10)
    plot(xS,yS,'kd','MarkerSize',10)
    pause
end


%%

close all

CC=zeros([(length(time))*3-2,fliplr(size(X))]);
CO=zeros([(length(time))*3-2,fliplr(size(X))]);


for ix=1:length(x)
    ix
    for iy=1:length(y)
        CO(:,ix,iy) = conv(GF1(:,ix,iy),conv(GFS(:,ix,iy),flipud(master_gf_2S),'full'),"full") + ...
                         conv(GF2(:,ix,iy),conv(GFS(:,ix,iy),flipud(master_gf_1S),'full'),"full");
        CC(:,ix,iy) = conv(flipud(GF1(:,ix,iy)),conv(GFS(:,ix,iy),flipud(master_gf_2S),'full'),"full") + ...
                 conv(flipud(GF2(:,ix,iy)),conv(GFS(:,ix,iy),flipud(master_gf_1S),'full'),"full");
    end
end


%%
close all
figure('Position',[1 446 1170 420])

xx=ix1;
yy=iy1;

for it = 1:10:size(CC,1)
    clf
    subplot(3,2,1)
    plot(CO(:,ixS,iyS),'k',LineWidth=2);
    hold on
    plot(CO(:,ix1,iy1),'r',LineWidth=2);
    plot(CO(:,ix2,iy2),'b',LineWidth=2);
    gg=gca;
    plot([it it],gg.YLim,'g')
    subplot(3,2,[3 5])
    pcolor(X,Y,squeeze(CO(it,:,:))')
    caxis([-1,1]*1e-4)
    axis equal
    shading flat
    %colormap(nori_color('bwr128'))
    hold on
    plot(x1,y1,'ro','MarkerSize',10)
    plot(x2,y2,'bo','MarkerSize',10)
    plot(xS,yS,'kd','MarkerSize',10)

    subplot(3,2,2)
    plot(CC(:,ixS,iyS),'k',LineWidth=2);
    hold on
    plot(CC(:,ix1,iy1),'r',LineWidth=2);
    plot(CC(:,ix2,iy2),'b',LineWidth=2);
    gg=gca;
    plot([it it],gg.YLim,'g')
    subplot(3,2,[4 6])
    pcolor(X,Y,squeeze(CC(it,:,:))')
    caxis([-1,1]*1e-4)
    axis equal
    shading flat
    %colormap(nori_color('bwr128'))
    hold on
    plot(x1,y1,'ro','MarkerSize',10)
    plot(x2,y2,'bo','MarkerSize',10)
    plot(xS,yS,'kd','MarkerSize',10)

    pause
end


%%


close all

xx=ix1;
yy=iy1;


figure('Position',[1 446 1170 420])
[~,it] = max(abs(CO(:,xx,yy)));
subplot(3,2,1)
plot(CO(:,xx,yy));hold on
plot(it,CO(it,xx,yy),'ro')
subplot(3,2,[3 5])
pcolor(X,Y,squeeze(CO(it,:,:))')
caxis([-1,1]*1e-4)
axis equal
shading flat
%colormap(nori_color('bwr128'))
hold on
plot(x1,y1,'ro','MarkerSize',10)
plot(x2,y2,'bo','MarkerSize',10)
plot(xS,yS,'kd','MarkerSize',10)

[~,it] = max(abs(CC(:,xx,yy)));
subplot(3,2,2)
plot(CC(:,xx,yy));hold on
plot(it,CC(it,xx,yy),'ro')
subplot(3,2,[4 6])
pcolor(X,Y,squeeze(CC(it,:,:))')
caxis([-1,1]*1e-4)
axis equal
shading flat
%colormap(nori_color('bwr128'))
hold on
plot(x1,y1,'ro','MarkerSize',10)
plot(x2,y2,'bo','MarkerSize',10)
plot(xS,yS,'kd','MarkerSize',10)



%%
close all
imagesc(squeeze(mean(CO(1300,:,:).^2,1)))
%imagesc(squeeze(mean(CC,1)))
caxis([0,1]*1e-10)
axis equal


%%

