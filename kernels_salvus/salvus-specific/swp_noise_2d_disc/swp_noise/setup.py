# swp_noise
# Copyright (C) 2022 Mondaic AG
# All rights reserved

from setuptools import setup

setup(
    name="swp_noise",
    version="0.0.2",
    packages=["swp_noise"],
    install_requires=[
        "salvus",
    ],
)
