# swp_noise
# Copyright (C) 2022 Mondaic AG
# All rights reserved
"""
Utilities for the noise source models.

Importantly this contains a function generator for the forward and adjoint
source processing functions.

It is written in a way that minimizes the imports on the remote site which will
run the source processing. In particular this means minimizing the dependencies
that are actually called in the function.

Thus a bunch of things are defined in this file that could well be imported
from elsewhere. Additionally the main function is written in a way that
facilitates function serialization.
"""
import json
import math
import os
import pathlib
import typing

import h5py
import numpy as np
import toml

from salvus.flow.collections.event import field_default_components_mapping


def _physics(field: str) -> str:
    elastic_fields = {
        "displacement",
        "velocity",
        "acceleration",
        "stress",
        "strain",
    }
    return "ELASTIC" if field in elastic_fields else "ACOUSTIC"


def _spatial_type(field: str) -> str:
    if field in ["phi", "phi_t", "phi_tt"]:
        return "scalar"
    elif field in ["displacement", "velocity", "acceleration"]:
        return "vector"


def sizeof_fmt(num):
    """
    Handy formatting for human readable filesize.
    From http://stackoverflow.com/a/1094933/1657047
    """
    for x in ["bytes", "KB", "MB", "GB"]:
        if num < 1024.0 and num > -1024.0:
            return "%3.1f %s" % (num, x)
        num /= 1024.0
    return "%3.1f %s" % (num, "TB")


def field_and_emanating_components_to_indices(
    field: str, emanating_components: typing.List[str]
):
    # The dimensionality should not matter here because the indices in 3-D are
    # always just strictly more than in 2-D.
    c = field_default_components_mapping[3][field]
    return sorted([c.index(v) for v in emanating_components])


def adjust_times_correlating_wavefield(
    folder_generating_simulation: pathlib.Path,
    folder_correlating_simulation: pathlib.Path,
):
    """
    Lots of defensive programming here as it has almost no runtime cost.
    """
    meta_gen_json = folder_generating_simulation / "output" / "meta.json"
    input_corr_toml = folder_correlating_simulation / "input" / "input.toml"
    if not meta_gen_json.exists():
        raise ValueError(
            "The meta.json output of the generating simulation does not exist."
            "Did something go wrong during the simulation?"
        )
    if not input_corr_toml.exists():
        raise ValueError(
            "The input file for the correlating simulation does not exist."
        )

    with meta_gen_json.open(mode="r") as fh:
        meta_gen = json.load(fh)
    with input_corr_toml.open(mode="r") as fh:
        input_corr = toml.load(fh)

    p_gen = meta_gen["forward_run_input"]["physics"]["wave_equation"]
    p_corr = input_corr["physics"]["wave_equation"]

    # The end time of the generating sim should, by construction, be the
    # negative of the correlating sim start time.
    if (
        p_gen["end_time_in_seconds"] < 0.0
        or p_corr["start_time_in_seconds"] > 0.0
        or not np.isclose(
            -1.0 * p_gen["end_time_in_seconds"],
            p_corr["start_time_in_seconds"],
        )
    ):
        raise ValueError(
            "The start/end times of the simulations are not consistent.\n"
            f"Generating: {p_gen}\n"
            f"Correlating: {p_corr}\n"
        )

    npts_corr = (
        math.ceil(
            (p_corr["end_time_in_seconds"] - p_corr["start_time_in_seconds"])
            / p_gen["time_step_in_seconds"]
        )
        + 1
    )
    adjusted_corr_endtime = (
        p_corr["start_time_in_seconds"]
        + (npts_corr - 1) * p_gen["time_step_in_seconds"]
    )
    # By construction it should not be smaller.
    if adjusted_corr_endtime < p_corr["end_time_in_seconds"]:
        raise ValueError(
            "Adjust end time is smaller than the original one.\n"
            f"Adjusted: {adjusted_corr_endtime}\n"
            f"Generating: {p_gen}\n"
            f"Correlating: {p_corr}\n"
        )

    # Adjust and write again.
    p_corr["end_time_in_seconds"] = adjusted_corr_endtime
    p_corr["time_step_in_seconds"] = p_gen["time_step_in_seconds"]

    with input_corr_toml.open(mode="w") as fh:
        toml.dump(o=input_corr, f=fh)


def _p(msg):
    print(f">>> {msg}")


def generate_source_processing_function(
    recording_field: str,
    emanating_component_indices: typing.Sequence[int],
    path_to_adjoint_distributed_receivers: typing.Optional[str] = None,
    n_time_steps: typing.Optional[float] = None,
    spatial_weights_function: typing.Optional[typing.Callable] = None,
    temporal_weights_function: typing.Optional[typing.Callable] = None,
    chunk_size_in_MB: float = 16.0,
):
    """
    Generate the function to be passed to the task chain.

    Is is generated so outside values can be passed using the function's
    closure.

    The function is expected to be run remotely so it contains a ton of print
    statements to help diagnose if things go wrong.

    Args:
        recording_field: The field of the distributed receivers.
        emanating_component_indices: The component indices of the emanating
            components of the source. E.g. [2] for elastic displacement would
            cause the distributed source to only fire on the third (z)
            component. All other components will be set to zero for the
            distributed sources.
        path_to_adjoint_distributed_receivers: Path the adjoint distributed
            receivers. This is specified for technical reasons because they are
            generated outside the task chain where they are needed. It is also
            used to distinguish between forward and adjoint source processing
            function: If not given it is a forward source processing function.
        n_time_steps: Should only be passed for adjoint simulations. Used to
            cut the adjoint distributed receivers to length before they are
            turned into adjoint distributed sources.
        spatial_weights_function: Optional function computing space-dependent
            weights.
        temporal_weights_function: Optional function computing time dependent
            weights.
        chunk_size_in_MB: Large files will be read and written in chunks. This
            sets the chunk size.
    """

    def process_source(
        task_index: int,
        task_index_folder_map: typing.Dict[int, pathlib.Path],
        data_directory: typing.Optional[pathlib.Path],
    ):
        print("\n")
        _p("=======================================")
        _p("Starting source processing ...")
        _p("=======================================")

        is_adjoint = path_to_adjoint_distributed_receivers is not None

        _p(f"Is adjoint: {is_adjoint}")

        if not is_adjoint:
            folder_generating = task_index_folder_map[task_index - 1]
            distributed_receivers = (
                folder_generating / "output" / "receivers.h5"
            )
        else:
            distributed_receivers = path_to_adjoint_distributed_receivers

        # Read the meta.json file of the correlating run as we need to access
        # the rotation matrices of the receivers, if given.
        if is_adjoint is False:
            meta_json_generating_file = (
                folder_generating / "output" / "meta.json"
            )
        else:
            # In the adjoint run the distributed receivers come from the source
            # kernel run - the meta.json file can be assumed to be next to it.
            meta_json_generating_file = (
                pathlib.Path(path_to_adjoint_distributed_receivers).parent
                / "meta.json"
            )
        with meta_json_generating_file.open(mode="r") as fh:
            meta_json_generating = json.load(fh)
        generating_receivers = meta_json_generating["forward_run_input"][
            "output"
        ]["point_data"]["receiver"]

        folder_correlating = task_index_folder_map[task_index + 1]
        input_folder_correlating = folder_correlating / "input"

        input_file = distributed_receivers
        output_file = input_folder_correlating / "block_source.h5"

        _p("Path to distributed receiver file (input):")
        _p(f"  '{input_file}'")
        _p("Path to distributed source file (output):")
        _p(f"  '{output_file}'")

        # For non-adjoint simulations we slightly adjust the times for the
        # correlating wavefield here to be fully consistent (e.g. exact same
        # time step, consistent start/end times) with the generating
        # simulations.
        # All other simulations will use those times as well.
        # This makes it work smoothly with Salvus' auto-time step detection.
        if is_adjoint is False:
            _p("Adjusting times of the correlating wavefield ...")
            adjust_times_correlating_wavefield(
                folder_generating_simulation=task_index_folder_map[
                    task_index - 1
                ],
                folder_correlating_simulation=task_index_folder_map[
                    task_index + 1
                ],
            )

        physics = _physics(recording_field)

        _p("Opening the distributed receivers file ...")
        with h5py.File(input_file, mode="r") as fh_in:

            # Directly read all coordinates.
            coordinates_in = fh_in[f"coordinates_{physics}_point"][:]

            # And attributes.
            sampling_rate = float(
                fh_in["point"].attrs["sampling_rate_in_hertz"][0]
            )
            start_time = float(
                fh_in["point"].attrs["start_time_in_seconds"][0]
            )

            # Lazy-load the data to not force it in memory in one go.
            data_in = fh_in["point"][recording_field]
            spatial_type = _spatial_type(recording_field)

            # We want to read this file in a chunked manner if it gets big.
            bytes_per_receiver = int(data_in.nbytes / data_in.shape[0])

            _p(f"File has data from {data_in.shape[0]} receiver(s).")
            _p(
                f"Each receiver's data is approximately "
                f"{data_in.shape[0]} bytes in size."
            )
            _p(f"Chunk size in MB setting: {chunk_size_in_MB}")

            # Adapt the chunking to the length of the traces.
            step = max(
                int(
                    math.ceil(
                        chunk_size_in_MB * 1024 * 1024 / bytes_per_receiver
                    )
                ),
                1,
            )
            _p(f"Will process file in chunks of {step} receivers.")

            # We don't want to re-inject every component of the distributed
            # source field. The emanating_component_indices are the indices of
            # the components we want to keep - so we'll invert it and set the
            # inverted components to zero.
            component_indices = np.arange(data_in.shape[1])
            zero_indices = [
                i
                for i in component_indices
                if i not in emanating_component_indices
            ]

            # Either unit weights or compute via the supplied spatial weighting
            # function.
            weights = np.ones(coordinates_in.shape[0])
            if spatial_weights_function is not None:
                weights = spatial_weights_function(
                    coordinates=coordinates_in,
                    data_directory=data_directory,
                )

            # A potentially very substantial amount of distributed receivers
            # does not have to be read.
            keep_indices = np.argwhere(weights > (weights.ptp() * 1e-5))[:, 0]
            new_indices = np.arange(len(keep_indices))

            _p(f"Number of distributed receivers: {data_in.shape[0]}.")
            _p(
                f"The distributed source will have {len(keep_indices)} point "
                "sources after culling based on spatial source weights."
            )

            new_coordinates = coordinates_in[keep_indices]

            # The receivers might have rotated output - thus we might need to
            # add the tranposed/inverted rotation matrices to the distributed
            # sources.
            rotation_matrices: typing.Optional[np.ndarray] = None
            # Assumption: Either all receivers have rotation matrices, or None
            # have.
            if generating_receivers is not None and generating_receivers[
                0
            ].get("rotation_on_output", None):
                _p("Detected rotation matrices on the generating receivers.")
                # Create a receiver name map.
                receiver_map = {
                    f"{r['network_code']}.{r['station_code']}.{r['location_code']}".encode(): r  # NOQA
                    for r in generating_receivers
                }
                names_in = fh_in[f"names_{physics}_point"][:]

                # Now loop and extract the rotation matrices in the correct
                # order.
                matrices = []
                for idx in keep_indices:
                    matrices.append(
                        np.array(
                            receiver_map[names_in[idx]]["rotation_on_output"][
                                "matrix"
                            ]
                        ).T
                    )
                # Convert to an array of the proper shape.
                rotation_matrices = np.array(matrices)

            offset = 0

            time_steps = data_in.shape[2]
            if n_time_steps:
                time_steps = n_time_steps

            _p("Creating output file ...")
            # Open the new data set with the distributed sources..
            with h5py.File(output_file, mode="w") as fh:
                g = fh.create_group("noise_sources")
                data_out = g.create_dataset(
                    name="stf",
                    shape=(
                        len(new_indices),
                        data_in.shape[1],
                        time_steps,
                    ),
                    dtype=data_in.dtype,
                )

                # All kinds of meta-information.
                g.create_dataset("coordinates", data=new_coordinates),
                g.attrs["spatial_type"] = np.string_(spatial_type)
                g.attrs["sampling_rate_in_hertz"] = [sampling_rate]
                # Flip the time axis.
                g.attrs["start_time_in_seconds"] = [
                    -1.0
                    * (1.0 / sampling_rate * (time_steps - 1) + start_time)
                ]

                # Also add the rotation matrices if necessary.
                if rotation_matrices is not None:
                    _p("Attaching rotation matrices to the output ...")
                    g.create_dataset(
                        name="rotation_on_input", data=rotation_matrices
                    )

                while offset < len(keep_indices):
                    start, stop = offset, offset + step
                    _p(
                        f"Processing indices {start}-"
                        f"{min(stop, len(keep_indices))} of "
                        f"{len(keep_indices)} ..."
                    )
                    read_indices = keep_indices[start:stop]

                    # Partially read the data.
                    d = data_in[read_indices]

                    # Cut to length if necessary.
                    if n_time_steps:
                        d = d[:, :, -n_time_steps:]
                        d = d[:, :, ::-1]

                    # Apply the spatial weighting.
                    d *= weights[read_indices, np.newaxis, np.newaxis]

                    # Apply the temporal weighting if given.
                    if temporal_weights_function is not None:
                        dt = 1.0 / sampling_rate
                        time_array = np.linspace(
                            start_time,
                            start_time + (time_steps - 1) * dt,
                            time_steps,
                        )
                        # Invert the time array so the axis is the same for the
                        # forward and adjoint source processing functions.
                        if is_adjoint:
                            time_array *= -1.0

                        for j, i in enumerate(read_indices):
                            c = coordinates_in[i]
                            w = temporal_weights_function(
                                time_array=time_array.copy(),
                                coordinate=c,
                                is_adjoint=is_adjoint,
                            )
                            # Now actually apply the weighting.
                            d[j] *= w

                    # Zero indices if necessary.
                    if zero_indices:
                        d[:, zero_indices, :] = 0.0

                    # Apply a minimal taper on both sides.
                    d[:, :, 0] = 0.0
                    d[:, :, 1] = 0.0
                    d[:, :, 2] = 0.0
                    d[:, :, 3] *= 0.25
                    d[:, :, 4] *= 0.75
                    d[:, :, -5] *= 0.75
                    d[:, :, -4] *= 0.25
                    d[:, :, -3] = 0.0
                    d[:, :, -2] = 0.0
                    d[:, :, -1] = 0.0

                    # Invert one last time.
                    d = d[:, :, ::-1]

                    # Write to the open HDF5 dataset.
                    data_out[start:stop, :, :] = d

                    offset += step

            _p("=======================================")
            input_file_size = sizeof_fmt(os.stat(input_file).st_size)
            output_file_size = sizeof_fmt(os.stat(output_file).st_size)

            _p(f"Size of input distributed receiver file: {input_file_size}")
            _p(f"Size of output distributed source file: {output_file_size}")
            _p("=======================================")
            _p("DONE")
            _p("=======================================")
            print("\n")

    return process_source
