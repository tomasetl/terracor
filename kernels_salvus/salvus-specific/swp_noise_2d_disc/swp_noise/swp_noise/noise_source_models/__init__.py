# swp_noise
# Copyright (C) 2022 Mondaic AG
# All rights reserved
"""
The noise source model module.
"""
import abc
import logging
import pathlib
import typing

import numpy as np
import xarray as xr

import salvus.namespace as sn

from . import utils, spatial_weights

logger = logging.getLogger(__name__)


class BaseNoiseSourceModel(metaclass=abc.ABCMeta):
    """
    The abstract base noise source model class.

    All actual implementation of a noise source model must inherit from this
    class and implement a few key methods.
    """

    @abc.abstractmethod
    def get_distributed_receivers(
        self, generating_wavefield: sn.simple_config.simulation.Waveform
    ) -> typing.List[sn.simple_config.receiver._Base]:
        """
        Return a list of all distributed receivers (that later will be
        distributed sources) from the generating wavefield simulation object.

        In many cases the locations will be auto-derived from the mesh but
        other logic is certainly possible.

        Args:
            generating_wavefield: The generating wavefield object.
        """
        pass

    @abc.abstractmethod
    def get_source_processing_function(self):
        """
        Create and return the source processing function.
        """
        pass

    @abc.abstractmethod
    def get_adjoint_source_processing_function(
        self,
        path_to_adjoint_distributed_receivers,
        n_time_steps,
    ):
        """
        Create and return the adjoint source processing function.

        Args:
            path_to_adjoint_distributed_receivers: Path to the adjoint
                distributed receivers.
            n_time_steps: The number of time steps to keep as the adjoint
                correlating simulation is shorter than the adjoint generating
                one.
        """
        pass

    def get_data_files(
        self, directory: pathlib.Path
    ) -> typing.List[pathlib.Path]:
        """
        Function that is called that should return any data files for the task
        chain that is created.

        Overwrite for child objects where the noise processing is dependent on
        actual files.

        Args:
            directory: A temporary directory that can be used to write files
                to. They will be available to the task chain and everything
                will be cleaned up.

        Returns:
            A list of paths containing all data files necessary for the noise
            model.
        """
        return []


class BaseSpatiallyVaryingNoiseSourceModel(BaseNoiseSourceModel):
    def __init__(
        self,
        recording_field,
        emanating_components,
        receivers_from_mesh_function: typing.Callable,
        source_amplitude: typing.Optional[xr.Dataset],
        temporal_weights_function: typing.Optional[typing.Callable],
    ):
        """
        The base spatially varying noise source model.

        Unifies homogeneous and heterogeneous source models.

        Args:
            recording_field: The field that will be stored on the distributed
                receivers.
            emanating_components: The components of the distributed receivers
                that will fire again when they are turned into distributed
                sources.
            receivers_from_mesh: A function that returns a list of receiver
                coordinates from the mesh. Can be used for custom source
                distributions.
            source_amplitude: An xarray data set representing a scaling factor
                for each distributed source. If not given, a unit weight will
                be assumed everywhere.
            temporal_weights_function: Temporal weighting function for the
                noise source distribution.
        """
        self.recording_field = recording_field
        self.emanating_components = emanating_components
        self.emanating_component_indices = (
            utils.field_and_emanating_components_to_indices(
                field=recording_field,
                emanating_components=emanating_components,
            )
        )
        self.source_amplitude = source_amplitude
        self.receivers_from_mesh_function = receivers_from_mesh_function
        self.temporal_weights_function = temporal_weights_function

    def get_distributed_receivers_and_weights(
        self, generating_wavefield: sn.simple_config.simulation.Waveform
    ) -> typing.Tuple[
        np.ndarray, typing.List[sn.simple_config.receiver._Base]
    ]:
        """
        The receiver locations are generated from the mesh vertices. This
        function additionally returns the spatial weights for each receiver.

        Args:
            generating_wavefield: The generating wavefield. Amongst other
                things this contains the mesh.
        """
        mesh = generating_wavefield._get_unstructured_mesh_object()
        receiver_locations = self.receivers_from_mesh_function(mesh)

        rec_class = {
            2: sn.simple_config.receiver.cartesian.Point2D,
            3: sn.simple_config.receiver.cartesian.Point3D,
        }

        # XXX: Need better solution - Mike already implemented a general
        # solution exactly for this. Need to us it.
        if mesh.ndim == 2:
            receivers = [
                rec_class[mesh.ndim](
                    x=r[0],
                    y=r[1],
                    fields=[self.recording_field],
                    station_code=str(i),
                )
                for i, r in enumerate(receiver_locations)
            ]
        else:
            receivers = [
                rec_class[mesh.ndim](
                    x=r[0],
                    y=r[1],
                    z=r[2],
                    fields=[self.recording_field],
                    station_code=str(i),
                )
                for i, r in enumerate(receiver_locations)
            ]

        logger.debug(f"Generated {len(receivers)} distributed receivers.")

        # If a source amplitude model is passed, set the weights.
        if self.source_amplitude is not None:
            coordinates = np.array([r.location for r in receivers])
            weights = spatial_weights.get_spatial_weights(
                coordinates=coordinates,
                data_directory=self.source_amplitude,
            )
            w = weights
        else:
            w = np.ones(len(receivers))

        return receivers, w

    def get_distributed_receivers(
        self, generating_wavefield: sn.simple_config.simulation.Waveform
    ) -> typing.List[sn.simple_config.receiver._Base]:
        """
        The receiver locations are generated from the mesh vertices.

        Args:
            generating_wavefield: The generating wavefield. Amongst other
                things this contains the mesh.
        """
        return self.get_distributed_receivers_and_weights(
            generating_wavefield=generating_wavefield
        )[0]

    def get_data_files(
        self, directory: pathlib.Path
    ) -> typing.List[pathlib.Path]:
        """
        We need to ship a netcdf file for the source model to be applied on a
        remote site. Point to that file here.
        """
        if self.source_amplitude is not None:
            # Serialize the noise model.
            filename = directory / "noise_model.nc"
            self.source_amplitude.to_netcdf(filename)
            return [filename]
        else:
            return []

    def get_adjoint_source_processing_function(
        self,
        path_to_adjoint_distributed_receivers,
        n_time_steps,
    ):
        """
        Create and return the adjoint source processing function.

        Args:
            path_to_adjoint_distributed_receivers: Path to the adjoint
                distributed receivers.
            n_time_steps: The number of time steps to keep as the adjoint
                correlating simulation is shorter than the adjoint generating
                one.
        """
        return utils.generate_source_processing_function(
            recording_field=self.recording_field,
            emanating_component_indices=self.emanating_component_indices,
            path_to_adjoint_distributed_receivers=path_to_adjoint_distributed_receivers,  # NOQA
            n_time_steps=n_time_steps,
            # This class uses the xarray datasets as spatial weights.
            spatial_weights_function=spatial_weights.get_spatial_weights
            if self.source_amplitude is not None
            else None,
            temporal_weights_function=self.temporal_weights_function,
        )

    def get_source_processing_function(self):
        """
        Create and return the source processing function.
        """
        return utils.generate_source_processing_function(
            recording_field=self.recording_field,
            emanating_component_indices=self.emanating_component_indices,
            # This class uses the xarray datasets as spatial weights.
            spatial_weights_function=spatial_weights.get_spatial_weights
            if self.source_amplitude is not None
            else None,
            temporal_weights_function=self.temporal_weights_function,
        )


from . import surface  # NOQA
from . import volume  # NOQA
from . import circular  # NOQA
