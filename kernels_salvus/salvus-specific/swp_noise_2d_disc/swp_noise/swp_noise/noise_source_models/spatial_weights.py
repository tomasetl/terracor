import pathlib
import typing

import numpy as np
import scipy.interpolate
import xarray as xr


def sanity_check_polar_data(theta: np.ndarray, v: np.ndarray):
    """
    A bunch of sanity checks for polar data specifications.

    Args:
        theta: The theta values in radian.
        v: The actual values.
    """
    theta = np.array(theta)
    v = np.array(v)

    # Bunch of sanity checks.
    if len(theta) < 20:
        raise ValueError("Must have at least 20 input samples.")
    if theta.shape != v.shape:
        raise ValueError("theta and v must have identical shapes.")

    if theta.ndim != 1:
        raise ValueError("Only works for 1-D arrays.")

    if not np.all(np.isclose([theta[0], theta[-1]], [0.0, 2.0 * np.pi])):
        raise ValueError(
            "The first theta value must be 0, the last 2.0 * np.pi"
        )

    if not np.isclose(v[0], v[-1], atol=1e-7 * np.abs(v).max()):
        raise ValueError("The first and last value must be the same.")

    if not np.all(np.diff(theta) > 0.0):
        raise ValueError("theta_in must be strictly monotonous.")


def create_polar_data_set(
    theta: np.ndarray,
    v: np.ndarray,
    interpolation_kind: typing.Union[str, int],
) -> xr.Dataset:
    """
    Convert the polar data
    """
    assert interpolation_kind in [
        "linear",
        "nearest",
        "nearest-up",
        "zero",
        "slinear",
        "quadratic",
        "cubic",
        "previous",
        "next",
        "zero",
        "slinear",
        "quadratic",
        "cubic",
        1,
        2,
        3,
    ]

    sanity_check_polar_data(theta=theta, v=v)
    return xr.Dataset(
        data_vars={
            "source_amplitude": (["theta"], v),
        },
        coords={"theta": theta},
        attrs={"interpolation_kind": interpolation_kind},
    )


def interpolate_polar_data_set(ds: xr.Dataset, theta_out: np.ndarray):
    return interpolate_polar_array_1d(
        theta_in=ds.theta.data,
        v_in=ds.source_amplitude.data,
        theta_out=theta_out,
        kind=ds.attrs["interpolation_kind"],
    )


def interpolate_polar_array_1d(
    theta_in: np.ndarray,
    v_in: np.ndarray,
    theta_out: np.ndarray,
    kind: typing.Union[str, int] = "linear",
) -> np.ndarray:
    """
    1-D interpolation for an array defined in polar coordinates.

    Args:
        theta_in: The theta values of the input data in radian. The first
            sample must be zero and the last sample must be 2 * pi, e.g. that
            sample has to be duplicated. Additionally the values must be
            monotonically increasing.
        v_in: The corresponding data (or radial) values for each point in
            `theta_in`.
        theta_out: The desired output sample points in radian. Every value must
            be between 0 and 2 * pi.
        kind: Specifies the kind of interpolation. Passed on to
            `scipy.interpolate.interp1d`.
    """
    theta_in = np.array(theta_in)
    v_in = np.array(v_in)
    sanity_check_polar_data(theta=theta_in, v=v_in)

    theta_out = np.array(theta_out)
    if theta_out.min() < 0.0 or theta_out.max() > 2.0 * np.pi:
        raise ValueError(
            "Every value in theta_out must be [0.0 < value > 2 * np.pi]."
        )

    p = 8
    padded_theta_in = np.concatenate(
        [
            theta_in[-p - 1 : -1] - theta_in[-1],  # NOQA
            theta_in,
            theta_in[-1] + theta_in[1 : p + 1],  # NOQA
        ]
    )
    padded_v_in = np.concatenate([v_in[-p:], v_in, v_in[:p]])

    return scipy.interpolate.interp1d(
        x=padded_theta_in, y=padded_v_in, kind=kind
    )(theta_out)


def get_spatial_weights(
    coordinates: np.ndarray,
    data_directory: typing.Union[pathlib.Path, xr.Dataset],
):
    """
    Get the spatial weights from an xarray dataset.

    Returns one weight per coordinate.

    Args:
        coordinates: The coordinates.
        data_directory: The data directory. Either a folder with a netcdf file
            called noise_model.nc or an xarray data set.
    """
    # Load the dataset containing the noise information.
    if isinstance(data_directory, pathlib.Path):
        nm = data_directory / "noise_model.nc"
        assert nm.exists()
        ds = xr.load_dataset(nm)
    else:
        ds = data_directory

    coords_in_ds = set(ds.coords.keys())

    # Cartesian interpolation.
    if coords_in_ds in [{"x", "y"}, {"x", "y", "z"}]:
        # Figure out which coordinates to interpolate - this works for 2-D
        # volumes and 3-D side set surface projections.
        coords = ["x", "y", "z"]
        kwargs = {}
        for c in list(ds.source_amplitude.coords.keys()):
            # Random coordinate name to benefit from a vectorized point-wise
            # interpolation.
            kwargs[c] = ("random", coordinates[:, coords.index(c)])

        # Compute weights from each source.
        weights = ds.source_amplitude.interp(
            **kwargs,
            # Pretty important to be able to "extrapolate" to 0 weights.
            kwargs={"fill_value": 0.0},
        ).data
        return weights
    # Polar coordinates.
    elif coords_in_ds == {"theta"}:
        # If 2, assume cartesian coordinates and convert to polar.
        if coordinates.ndim == 2:
            x = coordinates[:, 0]
            y = coordinates[:, 1]
            coordinates = np.arctan2(y, x)

        if coordinates.ndim != 1:
            raise ValueError(
                f"Polar coordinates must be 1-D. Shape: {coordinates.shape}"
            )
        c = coordinates.copy()
        c[c < 0.0] += 2 * np.pi
        weights = interpolate_polar_data_set(ds=ds, theta_out=c)
        return weights
    else:
        raise NotImplementedError(
            f"Cannot interpolate these data set coordinates: {coords_in_ds}"
        )
