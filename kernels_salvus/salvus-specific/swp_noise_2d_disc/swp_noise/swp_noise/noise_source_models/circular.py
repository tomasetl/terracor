# swp_noise
# Copyright (C) 2022 Mondaic AG
# All rights reserved
"""
Noise source models and associated utilities for circular two-dimensional
domains.
"""
import logging
import typing

import matplotlib.pyplot as plt
import numpy as np

import salvus.namespace as sn

from ..utils.mesh_handling import _vertices
from . import BaseSpatiallyVaryingNoiseSourceModel, spatial_weights

logger = logging.getLogger(__name__)


def plot_polar_array(
    theta: np.ndarray,
    v: np.ndarray,
    show: bool = True,
    figsize: typing.Tuple[int, int] = (7, 7),
):
    """
    Plot a polar array.

    Args:
        theta: The theta value in radian.
        v: The array values belonging to the theta value.
    """
    v_max = v.max()
    v = np.array(v).copy()

    # Some processing to enable negative values.
    # -1 to +1
    if v.ptp():
        v /= np.abs(v).max()
    # 4 to 6
    offset = 5.0
    v += offset

    # Finally plot.
    _, ax = plt.subplots(subplot_kw={"projection": "polar"}, figsize=figsize)
    ax.plot(theta, v, lw=2.0)
    ax.set_rlim(0, offset + 2)
    ax.set_rticks([offset, offset + 1], labels=["0.0", f"{v_max:.2f}"])
    if show:
        plt.show()


def polar2cartesian(*, longitude: float, radius: float) -> np.ndarray:
    """
    Convert polar to cartesian coordinates.
    """
    return np.array(
        [
            np.cos(np.deg2rad(longitude)) * radius,
            np.sin(np.deg2rad(longitude)) * radius,
        ],
        dtype=np.float64,
    )


def cartesian2polar(*, x: float, y: float) -> np.ndarray:
    """
    Convert cartesian to polar coordinates
    """
    return np.array([np.rad2deg(np.arctan2(y, x)), np.sqrt(x**2 + y**2)])


class HeterogeneousCircular2D(BaseSpatiallyVaryingNoiseSourceModel):
    def __init__(
        self,
        recording_field: str,
        emanating_components: typing.List[str],
        side_set: str,
        source_amplitude_longitudes: np.ndarray,
        source_amplitude_values: np.ndarray,
        interpolation_kind: typing.Union[str, int] = "cubic",
        temporal_weights_function: typing.Optional[typing.Callable] = None,
    ):
        """
        The base spatially varying noise source model.

        Unifies homogeneous and heterogeneous source models.

        Args:
            recording_field: The field that will be stored on the distributed
                receivers.
            emanating_components: The components of the distributed receivers
                that will fire again when they are turned into distributed
                sources.
            receivers_from_mesh: A function that returns a list of receiver
                coordinates from the mesh. Can be used for custom source
                distributions.
            source_amplitude: An xarray data set representing a scaling factor
                for each distributed source. If not given, a unit weight will
                be assumed everywhere.
            interpolation_kind: Specifies the kind of interpolation. Passed on
                to `scipy.interpolate.interp1d`.
            temporal_weights_function: Temporal weighting function for the
                noise source distribution.
        """
        self.recording_field = recording_field
        self.emanating_components = emanating_components
        # In circular 2-D the components are always Z and E, in that order.
        self.emanating_component_indices = sorted(
            [["Z", "E"].index(v) for v in emanating_components]
        )
        self.temporal_weights_function = temporal_weights_function
        self.side_set = side_set

        # Store this as the source amplitude instance variable - this will be
        # automatically be sent to the remote machine.
        self.source_amplitude = spatial_weights.create_polar_data_set(
            theta=np.deg2rad(source_amplitude_longitudes),
            v=source_amplitude_values,
            interpolation_kind=interpolation_kind,
        )

    def plot(self):
        """
        Visual representation of the polar weights.
        """
        theta_out = np.linspace(0, 2 * np.pi, 2000)
        # Use the spatial weights function to take the same path that the
        # actual interpolation later takes.
        v_out = spatial_weights.get_spatial_weights(
            coordinates=theta_out, data_directory=self.source_amplitude
        )
        plot_polar_array(theta=theta_out, v=v_out)

    def get_distributed_receivers_and_weights(
        self, generating_wavefield: sn.simple_config.simulation.Waveform
    ) -> typing.Tuple[
        np.ndarray, typing.List[sn.simple_config.receiver._Base]
    ]:
        mesh = generating_wavefield._get_unstructured_mesh_object()
        receiver_locations = _vertices(mesh, side_set=self.side_set)

        # These receiver locations are in cartesian coordinates as they are
        # extracted from the mesh. There would be other ways to do this. In any
        # case - we need to use Salvus' seismological receiver classes because
        # they generate the necessary rotation matrices so we are later able to
        # only emit for example the radial component.
        receivers = []
        weights = []
        longitudes = []
        for i, loc in enumerate(receiver_locations):
            longitude, _ = cartesian2polar(x=loc[0], y=loc[1])
            longitudes.append(longitude)
            tmp = sn.simple_config.receiver.seismology.Point2D(
                longitude=longitude,
                station_code=str(i),
                fields=[self.recording_field],
            ).get_dictionary()
            # Set the location to the actual position in the mesh just to have
            # no extra error there.
            tmp["location"] = [float(j) for j in loc]
            receivers.append(tmp)

        logger.debug(f"Generated {len(receivers)} distributed receivers.")

        # In-place import to avoid circular imports.

        weights = spatial_weights.get_spatial_weights(
            coordinates=np.deg2rad(longitudes),
            data_directory=self.source_amplitude,
        )

        return receivers, weights
