# swp_noise
# Copyright (C) 2022 Mondaic AG
# All rights reserved
"""
Surface noise source models.
"""
import functools
import typing

from . import BaseSpatiallyVaryingNoiseSourceModel
from ..utils.mesh_handling import _vertices


class Homogeneous(BaseSpatiallyVaryingNoiseSourceModel):
    def __init__(
        self,
        recording_field,
        emanating_components: typing.List[str],
        side_set: str,
    ):
        """
        Spatially homogeneous surface noise source model.

        Args:
            recording_field: The field that will be stored on the distributed
                receivers.
            emanating_components: The components of the distributed receivers
                that will fire again when they are turned into distributed
                sources.
            side_set: The side set to record/emanate on.
        """
        # This is really just a thin wrapper but it makes for a nice interface.
        super().__init__(
            recording_field=recording_field,
            emanating_components=emanating_components,
            # No source amplitude => No spatial processing => homogeneous.
            source_amplitude=None,
            temporal_weights_function=None,
            receivers_from_mesh_function=functools.partial(
                _vertices, side_set=side_set
            ),
        )
        self.side_set = side_set


class Heterogeneous(BaseSpatiallyVaryingNoiseSourceModel):
    def __init__(
        self,
        recording_field,
        emanating_components,
        source_amplitude,
        side_set: str,
        temporal_weights_function: typing.Optional[typing.Callable] = None,
    ):
        """
        Spatially heterogeneous surface noise source model.

        Args:
            recording_field: The field that will be stored on the distributed
                receivers.
            emanating_components: The components of the distributed receivers
                that will fire again when they are turned into distributed
                sources.
            source_amplitude: An xarray data set representing a scaling factor
                for each distributed source.
            side_set: The side set to record/emanate on.
            temporal_weights_function: Temporal weights function.
        """
        # This is really just a thin wrapper but it makes for a nice interface.
        super().__init__(
            recording_field=recording_field,
            emanating_components=emanating_components,
            source_amplitude=source_amplitude,
            receivers_from_mesh_function=functools.partial(
                _vertices, side_set=side_set
            ),
            temporal_weights_function=temporal_weights_function,
        )
        self.side_set = side_set
