# swp_noise
# Copyright (C) 2022 Mondaic AG
# All rights reserved
"""
swp_noise - A package for full waveform correlation modeling and inversion
using Salvus.
"""
__version__ = "0.0.1"

import logging
import os
import sys

from salvus import _SalvusLoggingFormatter

# A few imports to easy usability.
from swp_noise.entities.correlation import Correlation  # NOQA
from swp_noise.entities.correlation_point import CorrelationPoint  # NOQA
from swp_noise.entities.correlation_misfit import CorrelationMisfit  # NOQA
from swp_noise.geometry import coordinate  # NOQA
from swp_noise.simulations import (  # NOQA
    SimulationLaunchParameters,
)
from swp_noise import noise_source_models  # NOQA
from swp_noise import visualization  # NOQA
from swp_noise import source_mechanism  # NOQA
from swp_noise import misfits  # NOQA
from swp_noise import utils  # NOQA


# Default log level.
__log_level = os.environ.get("SWP_NOISE_LOG_LEVEL", "INFO").upper()

# Setup the Python logger.
__logger = logging.getLogger(__name__)
__logger.setLevel(getattr(logging, __log_level))
__ch = logging.StreamHandler(stream=sys.stdout)
__ch.setLevel(getattr(logging, __log_level))
__formatter = _SalvusLoggingFormatter()
__ch.setFormatter(__formatter)
__logger.addHandler(__ch)


def set_log_level(level: str) -> None:
    """
    Set the global log level of swp noise.

    Args:
        level: Log level in accordance with Python's `logging` module.
    """
    __logger.setLevel(level.upper())
    __ch.setLevel(level.upper())
