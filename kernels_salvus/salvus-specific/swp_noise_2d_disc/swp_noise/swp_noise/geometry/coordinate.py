# swp_noise
# Copyright (C) 2022 Mondaic AG
# All rights reserved
"""
Coordinates and utilities.
"""
from dataclasses import dataclass
import typing

import numpy as np


@dataclass
class Depth:
    """
    A coordinate defined as depth. Must be a positive number.
    """

    depth: float


@dataclass
class CartesianCoordinatePair:
    """
    A 2-D point in cartesian coordinates.

    Args:
        x: X coordinate in meters (absolute).
        y: Y coordinate in meters (absolute or depth).
    """

    x: float
    y: typing.Union[float, Depth]


@dataclass
class CartesianCoordinateTriple:
    """
    A 3-D point in cartesian coordinates.

    Args:
        x: X coordinate in meters (absolute).
        y: Y coordinate in meters (absolute).
        z: Z coordinate in meters (absolute or depth).
    """

    x: float
    y: float
    z: typing.Union[float, Depth]


@dataclass
class SphericalCoordinatePair:
    """
    A 2-D point in spherical coordinates.

    Args:
        longitude: Longitude in degrees.
        z: Vertical coordinate in meters (depth or radius).
    """

    longitude: float
    z: typing.Union[float, Depth]


@dataclass
class SphericalCoordinateTriple:
    """
    A 3-D point in spherical coordinates.

    Args:
        latitude: Latitude in degrees.
        longitude: Longitude in degrees.
        z: Vertical coordinate in meters (depth or radius).
    """

    latitude: float
    longitude: float
    z: typing.Union[float, Depth]


CoordinateType = typing.Union[
    CartesianCoordinatePair,
    CartesianCoordinateTriple,
    SphericalCoordinatePair,
    SphericalCoordinateTriple,
]
"""A generic coordinate type, Cartesian or spherical, 2- or 3-D."""


def _infill_2d(
    c: typing.List[CartesianCoordinatePair], num: int = 100
) -> typing.List[CartesianCoordinatePair]:

    xs = list(map(lambda x: x.x, c))
    ys = list(map(lambda x: x.y, c))

    min_x, max_x = min(xs), max(xs)
    min_y, max_y = min(ys), max(ys)

    ixs = np.unique(np.linspace(min_x, max_x, num))
    iys = np.unique(np.linspace(min_y, max_y, num))

    return [CartesianCoordinatePair(x=x, y=y) for x in ixs for y in iys]


def _infill_3d(
    c: typing.List[CartesianCoordinateTriple], num: int = 100
) -> typing.List[CartesianCoordinateTriple]:

    xs = list(map(lambda x: x.x, c))
    ys = list(map(lambda x: x.y, c))
    zs = list(map(lambda x: x.z, c))

    min_x, max_x = min(xs), max(xs)
    min_y, max_y = min(ys), max(ys)
    min_z, max_z = min(zs), max(zs)

    ixs = np.unique(np.linspace(min_x, max_x, num))
    iys = np.unique(np.linspace(min_y, max_y, num))
    izs = np.unique(np.linspace(min_z, max_z, num))

    return [
        CartesianCoordinateTriple(x=x, y=y, z=z)
        for x in ixs
        for y in iys
        for z in izs
    ]


def infill(
    c: typing.Union[
        typing.List[CartesianCoordinatePair],
        typing.List[CartesianCoordinateTriple],
    ],
    num: int = 100,
):
    if len(c) == 0:
        return c
    if isinstance(c[0], CartesianCoordinatePair):
        return _infill_2d(c, num)
    return _infill_3d(c, num)
