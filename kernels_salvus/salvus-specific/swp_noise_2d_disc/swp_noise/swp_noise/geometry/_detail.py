# swp_noise
# Copyright (C) 2022 Mondaic AG
# All rights reserved
"""
Some implementation details.
"""
import typing

import numpy as np

from ..utils.types import Dimension
from .coordinate import (
    CartesianCoordinatePair,
    CartesianCoordinateTriple,
    CoordinateType,
    Depth,
    SphericalCoordinatePair,
    SphericalCoordinateTriple,
)


def _is_cartesian(c: CoordinateType) -> bool:
    """Query whether a coordinate is Cartesian."""

    return isinstance(c, (CartesianCoordinatePair, CartesianCoordinateTriple))


def _is_spherical(c: CoordinateType) -> bool:
    """Query whether a corodinate is spherical."""

    return isinstance(c, (SphericalCoordinatePair, SphericalCoordinateTriple))


def _dim(c: CoordinateType) -> Dimension:
    """Query the dimension of a coordinate."""

    if isinstance(c, (CartesianCoordinatePair, SphericalCoordinatePair)):
        return Dimension.DIM2
    return Dimension.DIM3


def _is_depth(c: typing.Union[float, Depth]) -> bool:
    """Query whether a coordinate is a depth coordinate."""

    return isinstance(c, Depth)


def _to_absolute_z(c: typing.Union[float, Depth], max_z: float) -> float:
    """Compute a depth's absolute value, otherwise pass through."""
    return c if not _is_depth(c) else max_z - c.depth  # type: ignore


def _to_matplotlib_form(c: CoordinateType, max_z: float) -> np.ndarray:
    """An array of absolute coordinate expressed as radians (if spherical)."""

    if _is_cartesian(c):
        if _dim(c) == Dimension.DIM2:
            return np.array([c.x, _to_absolute_z(c.y, max_z)])
        return np.array([c.x, c.y, _to_absolute_z(c.z, max_z)])

    if _dim(c) == Dimension.DIM2:
        return np.array(
            [np.deg2rad(c.longitude), _to_absolute_z(c.z, max_z) / max_z]
        )
    return np.array(
        [
            np.deg2rad(c.latitude),
            np.deg2rad(c.longitude),
            _to_absolute_z(c.z, max_z) / max_z,
        ]
    )
