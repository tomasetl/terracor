# swp_noise
# Copyright (C) 2022 Mondaic AG
# All rights reserved
"""
The I/O submodule for I/O related to simulations.
"""
import json
import pathlib
import shutil
import typing


import salvus.namespace as sn
from salvus.flow.sites.task_chain import TaskChain
from salvus.flow.sites.types import JobStatus

from ..entities.correlation_data import CorrelationData
from ..entities.source_gradient import SourceGradient
from ..entities.structure_gradient import StructureGradient


def _check_task_chain_finished(task_chain: TaskChain):
    """
    Assert that a task chain has finished.

    Args:
        task_chain: The task chain to check.
    """
    status = task_chain.update_status()
    if status != JobStatus.finished:
        raise ValueError("Can only download data from finished task chains.")


def _check_and_create_output_folder(
    output_folder: pathlib.Path, overwrite: bool
):
    """
    Check that an output folder either does not exist (and then create it) or
    assert that it can be overwritten.

    Args:
        output_folder: The folder to check.
        overwrite: If true, the folder will be emptied if it has contents.
    """
    output_folder = pathlib.Path(output_folder)
    if output_folder.exists():
        if overwrite is False:
            raise ValueError(
                f"Output folder '{output_folder}' already ", "exists."
            )
        # We do have to delete here as just overwriting a currently open HDF5
        # creates all kinds of troubles.
        shutil.rmtree(output_folder)

    output_folder.mkdir(exist_ok=True, parents=True)
    return output_folder


def _check_data_folder(data_folder: pathlib.Path):
    """
    Assert that the data folder exists.

    Args:
        data_folder: The data folder.
    """
    data_folder = pathlib.Path(data_folder)
    if not data_folder.exists():
        raise ValueError(f"Folder '{data_folder}' does not exist.")
    return data_folder


def _download_stdouterr(task_chain: TaskChain, output_folder: pathlib.Path):
    """
    Download the stdout and stderr dictionaries of a task chain.

    Args:
        task_chain: The task chain.
        output_folder: The output folder.
    """
    with (output_folder / "stdout.json").open("w") as fh:
        json.dump(obj=task_chain.stdout, fp=fh, indent=2)
    with (output_folder / "stderr.json").open("w") as fh:
        json.dump(obj=task_chain.stderr, fp=fh, indent=2)


def _write_job_info(task_chain: TaskChain, output_folder: pathlib.Path):
    """
    Write the job info JSON file.

    Args:
        task_chain: The task chain.
        output_folder: The folder where the file should be stored.
    """
    with (output_folder / "job_info.json").open("w") as fh:
        json.dump(
            obj={
                "site_name": task_chain.site.name,
                "task_chain_name": task_chain.job_name,
            },
            fp=fh,
            indent=2,
        )


def download_source_gradient_data(
    task_chain: TaskChain, output_folder: pathlib.Path, overwrite: bool = False
):
    """
    Download the results of a source gradient simulation.

    Args:
        task_chain: The task chain for the source gradient simulation.
        output_folder: Path to download to.
        OVERWRITE: aLLOW OVERWRITING.
    """
    _check_task_chain_finished(task_chain=task_chain)
    output_folder = _check_and_create_output_folder(
        output_folder=output_folder, overwrite=overwrite
    )

    adjoint_generating_dir = task_chain.task_index_folder_map[0] / "output"
    local_adjoint_generating_dir = (
        output_folder / "adjoint_generating_wavefield_output"
    )
    source_gradient_dir = task_chain.task_index_folder_map[1]
    local_source_gradient_dir = output_folder / "source_gradient"

    local_adjoint_generating_dir.mkdir(exist_ok=True)
    local_source_gradient_dir.mkdir(exist_ok=True)

    for (src, target) in [
        (
            adjoint_generating_dir / "meta.json",
            local_adjoint_generating_dir / "meta.json",
        ),
        (
            source_gradient_dir / "source_gradient.h5",
            local_source_gradient_dir / "source_gradient.h5",
        ),
    ]:
        task_chain.site.remote_get(remotepath=src, localpath=target)

    _download_stdouterr(task_chain=task_chain, output_folder=output_folder)
    _write_job_info(task_chain=task_chain, output_folder=output_folder)


def read_source_gradient_data(
    data_folder: pathlib.Path,
) -> SourceGradient:
    """
    Read the source gradient data from a folder.o

    Args:
        data_folder: Path to the source gradient.
    """
    data_folder = _check_data_folder(data_folder=data_folder)
    candidates = list(data_folder.glob("**/source_gradient.h5"))
    if not candidates:
        raise ValueError(
            "Could not find the source gradient file. Expected a "
            "'source_gradient.h5' file somewhere in the data folder."
        )
    elif len(candidates) > 1:
        raise ValueError(
            f"Found multiple source gradient file candidates: {candidates}"
        )

    sc = SourceGradient.read(candidates[0])
    sc.data_folder = data_folder

    meta_json_path = (
        data_folder / "adjoint_generating_wavefield_output" / "meta.json"
    )
    if meta_json_path.exists():
        with meta_json_path.open("r") as fh:
            sc.meta_json_adjoint_generating_run = json.load(fh)

    return sc


def download_structure_gradient_data(
    task_chain: TaskChain, output_folder: pathlib.Path, overwrite: bool = False
):
    """
    Download the results of a structure gradient simulation.

    Args:
        task_chain: The task chain for the structure gradient simulation.
        output_folder: Path to download to.
        OVERWRITE: aLLOW OVERWRITING.
    """
    _check_task_chain_finished(task_chain=task_chain)
    output_folder = _check_and_create_output_folder(
        output_folder=output_folder, overwrite=overwrite
    )

    adjoint_generating_dir = task_chain.task_index_folder_map[0] / "output"
    adjoint_correlating_dir = task_chain.task_index_folder_map[2] / "output"

    local_adjoint_generating_dir = (
        output_folder / "adjoint_generating_wavefield_output"
    )
    local_adjoint_correlating_dir = (
        output_folder / "adjoint_correlating_wavefield_output"
    )

    local_adjoint_generating_dir.mkdir(exist_ok=True)
    local_adjoint_correlating_dir.mkdir(exist_ok=True)

    # We want the meta.json files of both, the receivers of the correlating
    # wavefield, and the stdout/stderr of both.
    for (src, target) in [
        (
            adjoint_generating_dir / "meta.json",
            local_adjoint_generating_dir / "meta.json",
        ),
        (
            adjoint_generating_dir / "gradient.xdmf",
            local_adjoint_generating_dir / "gradient.xdmf",
        ),
        (
            adjoint_generating_dir / "gradient.h5",
            local_adjoint_generating_dir / "gradient.h5",
        ),
        (
            adjoint_correlating_dir / "meta.json",
            local_adjoint_correlating_dir / "meta.json",
        ),
        (
            adjoint_correlating_dir / "gradient.xdmf",
            local_adjoint_correlating_dir / "gradient.xdmf",
        ),
        (
            adjoint_correlating_dir / "gradient.h5",
            local_adjoint_correlating_dir / "gradient.h5",
        ),
    ]:
        task_chain.site.remote_get(remotepath=src, localpath=target)

    _download_stdouterr(task_chain=task_chain, output_folder=output_folder)
    _write_job_info(task_chain=task_chain, output_folder=output_folder)


def download_correlation_data(
    task_chain: TaskChain, output_folder: pathlib.Path, overwrite: bool = False
):
    """
    Download the results of a correlationo simulation.

    Args:
        task_chain: The task chain for the correlation simulation.
        output_folder: Path to download to.
        OVERWRITE: aLLOW OVERWRITING.
    """
    _check_task_chain_finished(task_chain=task_chain)
    output_folder = _check_and_create_output_folder(
        output_folder=output_folder, overwrite=overwrite
    )

    generating_wavefield_dir = task_chain.task_index_folder_map[0] / "output"
    correlating_wavefield_dir = task_chain.task_index_folder_map[2] / "output"

    local_generating_dir = output_folder / "generating_wavefield_output"
    local_correlating_dir = output_folder / "correlating_wavefield_output"

    local_generating_dir.mkdir(exist_ok=True)
    local_correlating_dir.mkdir(exist_ok=True)

    # We want the meta.json files of both, the receivers of the correlating
    # wavefield, and the stdout/stderr of both.
    for (src, target) in [
        (
            generating_wavefield_dir / "meta.json",
            local_generating_dir / "meta.json",
        ),
        (
            correlating_wavefield_dir / "meta.json",
            local_correlating_dir / "meta.json",
        ),
        (
            correlating_wavefield_dir / "receivers.h5",
            local_correlating_dir / "receivers.h5",
        ),
    ]:
        task_chain.site.remote_get(remotepath=src, localpath=target)

    _write_job_info(task_chain=task_chain, output_folder=output_folder)
    _download_stdouterr(task_chain=task_chain, output_folder=output_folder)


def read_correlation_data(data_folder: pathlib.Path) -> CorrelationData:
    """
    Read correlations from a simulation.

    Args:
        data_folder: The folder to read from.
    """
    data_folder = _check_data_folder(data_folder=data_folder)

    ed = sn.EventData.from_output_folder(
        data_folder / "correlating_wavefield_output"
    )

    with (data_folder / "generating_wavefield_output" / "meta.json").open(
        "r"
    ) as fh:
        meta_json_generating = json.load(fh)

    with (data_folder / "stdout.json").open("r") as fh:
        stdout = json.load(fh)

    with (data_folder / "stderr.json").open("r") as fh:
        stderr = json.load(fh)

    return CorrelationData(
        correlation_data=ed,
        generating_wavefield_meta_json=meta_json_generating,
        correlating_wavefield_meta_json=ed.meta_json_contents,
        stdout=stdout,
        stderr=stderr,
        data_folder=data_folder,
    )


def read_structure_gradient_data(
    data_folder: pathlib.Path,
) -> StructureGradient:
    """
    Read structure gradient simulation results from a folder

    Args:
        data_folder: The folder to read from.
    """
    data_folder = _check_data_folder(data_folder=data_folder)

    return StructureGradient.read(data_folder=data_folder)


def delete_remote_files(
    data_folder: typing.Union[pathlib.Path, str], verbosity: int = 1
):
    """
    Delete remote files from a given data folder.

    The data folder must contain a 'job_info.json' file so it can figure out
    the remote location of all files.

    Args:
        data_folder: Path to the data folder.
        verbosity: Verbosity level.
    """
    data_folder = pathlib.Path(data_folder)
    if not data_folder.exists():
        raise ValueError(f"Folder {data_folder} does not exist.")
    job_info_file = data_folder / "job_info.json"
    if not job_info_file.exists():
        raise ValueError(f"File {job_info_file} does not exist.")

    with job_info_file.open(mode="r") as f:
        ji = json.load(f)

    tc = sn.api.get_task_chain(
        site_name=ji["site_name"], job_name=ji["task_chain_name"]
    )
    tc.delete(verbosity=verbosity)
