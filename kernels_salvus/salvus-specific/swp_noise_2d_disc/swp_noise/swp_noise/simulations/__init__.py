# swp_noise
# Copyright (C) 2022 Mondaic AG
# All rights reserved
"""
The simulation module.
"""
from dataclasses import dataclass
import typing


@dataclass(init=False)
class SimulationLaunchParameters:
    site_name: str
    ranks_per_job: int
    wall_time_in_seconds_per_forward_job: typing.Optional[float] = None
    wall_time_in_seconds_per_adjoint_job: typing.Optional[float] = None
    memory_per_rank_in_MB: typing.Optional[float] = None

    def __init__(
        self,
        site_name: str,
        ranks_per_job: int,
        wall_time_in_seconds_per_forward_job: typing.Optional[float] = None,
        wall_time_in_seconds_per_adjoint_job: typing.Optional[float] = None,
        memory_per_rank_in_MB: typing.Optional[float] = None,
    ) -> None:
        """
        Collect simulation meta parameters in one class.

        Args:
            site_name: The name of the site.
            ranks_per_job: Ranks per Salvus simulation.
            wall_time_in_seconds_per_forward_job: The wall time in seconds for
                non-adjoint runs.
            wall_time_in_seconds_per_adjoint_job: The wall time in seconds for
                adjoint runs.
            memory_per_rank_in_MB: The memory in rank per megabyte.
        """
        # If one is set, both must be set.
        if (
            wall_time_in_seconds_per_forward_job is not None
            or wall_time_in_seconds_per_adjoint_job is not None
        ) and (
            wall_time_in_seconds_per_forward_job is None
            or wall_time_in_seconds_per_adjoint_job is None
        ):
            raise ValueError(
                "If a wall time is set, must set it for forward as well as "
                "adjoint runs."
            )

        if memory_per_rank_in_MB is not None:
            memory_per_rank_in_MB = float(memory_per_rank_in_MB)

        self.site_name = site_name
        self.ranks_per_job = ranks_per_job
        self.memory_per_rank_in_MB = memory_per_rank_in_MB
        self.wall_time_in_seconds_per_forward_job = (
            wall_time_in_seconds_per_forward_job
        )
        self.wall_time_in_seconds_per_adjoint_job = (
            wall_time_in_seconds_per_adjoint_job
        )


from .task_chains import run_correlation_simulation  # NOQA
from .task_chains import run_source_gradient_simulation  # NOQA
from .task_chains import run_structure_gradient_simulation  # NOQA
