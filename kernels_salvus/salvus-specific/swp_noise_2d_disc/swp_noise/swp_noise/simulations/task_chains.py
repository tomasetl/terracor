# swp_noise
# Copyright (C) 2022 Mondaic AG
# All rights reserved
"""
Main Salvus simulation module for swp_noise.
"""
import copy
import pathlib
import typing

import h5py
import voluptuous

import salvus.namespace as sn
from salvus.flow.sites.task_chain import TaskChain
from salvus.flow.utils.tempfile_manager import TemporaryDirectory

from ..entities.correlation import Correlation
from ..entities.correlation_data import CorrelationData
from ..entities.correlation_misfit import CorrelationMisfit
from ..entities.source_gradient import SourceGradient, compute_source_gradient
from ..entities.structure_gradient import StructureGradient
from ..entities.types import correlation_to_event
from ..noise_source_models import BaseNoiseSourceModel

from . import io, SimulationLaunchParameters


def create_correlation_tasks(
    *,
    simulation_launch_parameters: SimulationLaunchParameters,
    simulation_template: sn.simple_config.simulation.Waveform,
    stf: sn.simple_config.stf._Base,
    correlation: Correlation,
    simulation_history_in_s: float,
    lag_time_in_s: float,
    noise_source_model: BaseNoiseSourceModel,
    wavefield_compression: typing.Optional[sn.WavefieldCompression] = None,
    temporary_directory: pathlib.Path,
    extra_output_configuration: typing.Optional[typing.Dict] = None,
) -> typing.Tuple[
    typing.List[
        typing.Union[typing.Callable, sn.simple_config.simulation.Waveform]
    ],
    typing.List[pathlib.Path],
]:
    """
    Create the tasks required for a correlation simulation. These are intended
    to be fed into a task chain.

    Args:
        simulation_launch_parameters: The simulation meta parameters.
        simulation_template: The simulation template.
        stf: The source time function for the virtual source.
        correlation: The correlation object.
        simulation_history_in_s: How far to go back in time.
        lag_time_in_s: The desired max lag time of the correlation.
        noise_source_model: The noise source model.
        wavefield_compression: Wavefield compression for the checkpoints. If
            None, checkpoints will not be created.
        temporary_directory: Temporary directory to write files to. Most only
            exist until the task chain has been submitted.
        extra_output_configuration: Ability to store surface or volume data
            during both individual simulations.
    """
    # Do some basic validation here.
    if extra_output_configuration:
        if wavefield_compression is not None:
            raise ValueError(
                "extra_output_configuration can only be passed if no "
                "checkpoints are being requested "
                "(wavefield_compression=None)."
            )
        s = voluptuous.Schema(
            {
                "surface_data": {
                    voluptuous.Required(
                        "sampling_interval_in_time_steps"
                    ): int,
                    voluptuous.Required("fields"): [str],
                    voluptuous.Required("side_sets"): [str],
                },
                "volume_data": {
                    voluptuous.Required(
                        "sampling_interval_in_time_steps"
                    ): int,
                    voluptuous.Required("fields"): [str],
                },
            },
            extra=False,
            required=False,
        )
        s(extra_output_configuration)

        # Fill in the missing parameters.
        extra_output_configuration = copy.deepcopy(extra_output_configuration)
        for key in ["volume_data", "surface_data"]:
            if key in extra_output_configuration:
                extra_output_configuration[key][
                    "filename"
                ] = f"{key}_output.h5"
                extra_output_configuration[key]["format"] = "hdf5"
    else:
        extra_output_configuration = {}

    if simulation_launch_parameters.memory_per_rank_in_MB:
        extra_output_configuration["memory_per_rank_in_MB"] = float(
            simulation_launch_parameters.memory_per_rank_in_MB
        )

    # A bunch of sanity checks.
    if simulation_history_in_s <= 0.0:
        raise ValueError("'simulation_history_in_s' must be larger than 0.")
    elif lag_time_in_s <= 0.0:
        raise ValueError("'lag_time_in_s' must be larger than 0.")
    elif simulation_history_in_s < lag_time_in_s:
        raise ValueError(
            "'simulation_history_in_s' must not be smaller than "
            "'lag_time_in_s'."
        )

    # Copy and and remove any potential already set outputs.
    simulation_template = simulation_template.copy()
    # Unset any previously set sources.
    simulation_template.physics.wave_equation.point_source = []
    # Also get rid of any output.
    extra_output_configuration[
        "meta_data"
    ] = simulation_template.output.meta_data
    simulation_template.output = extra_output_configuration

    # We are using the lower level Salvus api here - so we'll directly apply
    # the setting to the forward simulations.
    if wavefield_compression is not None:
        interval = wavefield_compression.forward_wavefield_sampling_interval
        if interval == 1:
            v = "auto-for-checkpointing"
        else:
            v = f"auto-for-checkpointing_{interval}"

        simulation_template.output.volume_data = {
            "filename": "volume.h5",
            "fields": ["adjoint-checkpoint"],
            "sampling_interval_in_time_steps": v,
            "format": "hdf5",
        }

    event = correlation_to_event(
        correlation=correlation, source_time_function=stf
    )

    # Create the generating wavefield simulation.
    generating_wavefield = simulation_template.copy()
    # No need to set the start-time for the generating wavefield as that will
    # be determined by the source time function.
    # The end time must how far back in time any potential noise source could
    # reach.
    if hasattr(
        generating_wavefield.physics.wave_equation, "start_time_in_seconds"
    ):
        del generating_wavefield.physics.wave_equation.start_time_in_seconds
        assert not hasattr(
            generating_wavefield.physics.wave_equation, "start_time_in_seconds"
        )
    generating_wavefield.physics.wave_equation.end_time_in_seconds = (
        simulation_history_in_s
    )
    # Add the virtual source.
    generating_wavefield.add_sources(event.sources)

    # Now generate the correlating wavefield.
    correlating_wavefield = simulation_template.copy()
    correlating_wavefield.physics.wave_equation.start_time_in_seconds = (
        -simulation_history_in_s
    )
    correlating_wavefield.physics.wave_equation.end_time_in_seconds = (
        lag_time_in_s
    )
    correlating_wavefield.add_receivers(event.receivers, output_format="hdf5")

    # Store any files that might be necessary for the noise source
    # computation in a temporary directory.
    data_files_to_upload = noise_source_model.get_data_files(
        directory=temporary_directory
    )

    # Extract the distributed receiver locations from the noise source model.
    receivers = noise_source_model.get_distributed_receivers(
        generating_wavefield=generating_wavefield
    )
    generating_wavefield.add_receivers(
        receivers=receivers, output_format="hdf5"
    )

    # And finally add them as a block source to the correlating wavefield
    # object.
    correlating_wavefield.physics.wave_equation.point_source_block = {
        "filename": "PROMISE_FILE:task_2_of_2/input/block_source.h5",
        "groups": ["/noise_sources"],
    }

    source_processing_fct = noise_source_model.get_source_processing_function()

    tasks = [
        generating_wavefield,
        source_processing_fct,
        correlating_wavefield,
    ]
    data_files_to_upload = data_files_to_upload
    return tasks, data_files_to_upload


def run_correlation_simulation(
    *,
    simulation_launch_parameters: SimulationLaunchParameters,
    simulation_template: sn.simple_config.simulation.Waveform,
    stf: sn.simple_config.stf._Base,
    correlation: Correlation,
    simulation_history_in_s: float,
    lag_time_in_s: float,
    noise_source_model: BaseNoiseSourceModel,
    wavefield_compression: typing.Optional[sn.WavefieldCompression] = None,
    extra_output_configuration: typing.Optional[typing.Dict] = None,
    output_folder: pathlib.Path,
    overwrite: bool,
    interactive_tty: bool = False,
) -> CorrelationData:
    """
    Run a correlation simulation and download the results.

    Args:
        simulation_launch_parameters: The simulation meta parameters.
        simulation_template: The simulation template.
        stf: The source time function for the virtual source.
        correlation: The correlation object.
        simulation_history_in_s: How far to go back in time.
        lag_time_in_s: The desired max lag time of the correlation.
        noise_source_model: The noise source model.
        wavefield_compression: Wavefield compression for the checkpoints. If
            None, checkpoints will not be created.
        extra_output_configuration: Ability to store surface or volume data
            during both individual simulations.
        output_folder: Folder to store the results in.
        overwrite: Allow overwriting of results.
        interactive_tty: Interactive debugging of remote jobs.
    """
    output_folder = io._check_and_create_output_folder(
        output_folder=output_folder, overwrite=overwrite
    )
    with TemporaryDirectory() as tmp_dir:
        tasks, data_files_to_upload = create_correlation_tasks(
            simulation_launch_parameters=simulation_launch_parameters,
            simulation_template=simulation_template,
            stf=stf,
            correlation=correlation,
            simulation_history_in_s=simulation_history_in_s,
            lag_time_in_s=lag_time_in_s,
            noise_source_model=noise_source_model,
            wavefield_compression=wavefield_compression,
            temporary_directory=tmp_dir.name,
            extra_output_configuration=extra_output_configuration,
        )
        tc = sn.api.run_task_chain(
            site_name=simulation_launch_parameters.site_name,
            tasks=tasks,
            ranks=simulation_launch_parameters.ranks_per_job,
            data_files_to_upload=data_files_to_upload,
            interactive_tty=interactive_tty,
        )

    io.download_correlation_data(
        task_chain=tc, output_folder=output_folder, overwrite=overwrite
    )
    return io.read_correlation_data(data_folder=output_folder)


def _generate_source_gradient_function(
    path_to_generating_wavefield_receiver_file: pathlib.Path,
    ndim: int,
    field: str,
    components: typing.Optional[typing.List[str]],
):
    """
    Generate the source gradient processing function.

    Args:
        path_to_generating_wavefield_receiver_file: Path to the generating
            wavefield receiver file.
        ndim: The dimensionality.
        field: The field name.
        components: Which components to compute the source gradient for.
    """

    def generate_source_gradient(
        task_index: int, task_index_folder_map: typing.Dict[int, pathlib.Path]
    ):
        source_gradient = compute_source_gradient(
            output_generating_wavefield=path_to_generating_wavefield_receiver_file,  # NOQA
            output_adjoint_generating_wavefield=task_index_folder_map[
                task_index - 1
            ]
            / "output"
            / "receivers.h5",
            ndim=ndim,
            field=field,
            components=components,
        )

        source_gradient.write(
            task_index_folder_map[task_index] / "source_gradient.h5"
        )

    return generate_source_gradient


def create_source_gradient_tasks(
    correlation_misfit: CorrelationMisfit,
    source_gradient_components: typing.Optional[typing.List[str]] = None,
) -> TaskChain:
    """
    Create the tasks required for a source gradient simulation. These are
    intended to be fed into a task chain.

    Args:
        correlation_misfit: The correlation misfit object.
        source_gradient_components: The components the source gradient should
            be computed for.
    """
    # Step 1: Assemble the wavefield simulation.
    # But rest of the the input from the correlating wavefield.
    meta = (
        correlation_misfit.synthetic_correlation.correlating_wavefield_meta_json  # NOQA
    )["forward_run_input"]
    w = sn.simple_config.simulation.Waveform(
        mesh=f"REMOTE:{meta['domain']['mesh']['filename']}"
    )
    w.domain.dimension = meta["domain"]["dimension"]

    # Copy the physics
    physics = meta["physics"]["wave_equation"].copy()

    # Replace the sources with the adjoint source.
    del physics["point_source_block"]

    # Create the adjoint source.
    em = correlation_misfit._event_misfit
    em.write(filename="adjoint_source.h5")

    # Time reverse the adjoint source because we actually need to run it
    # backwards here.
    # XXX: Figure out a way where this does not have to modify a file on disc.
    with h5py.File("adjoint_source.h5", "r+") as f:
        adj = f["adjoint_sources"]["stf"][:]
        f["adjoint_sources"]["stf"][:] = adj[:, :, ::-1]

    physics["point_source_block"] = {
        "filename": "adjoint_source.h5",
        "groups": ["adjoint_sources"],
    }
    w.physics.wave_equation = physics

    # Add the distributed receivers from the generating wavefield.
    recs = correlation_misfit.synthetic_correlation.generating_wavefield_meta_json[  # NOQA
        "forward_run_input"
    ][
        "output"
    ][
        "point_data"
    ].copy()
    recs["filename"] = "receivers.h5"

    w.output.point_data = recs

    m = correlation_misfit.synthetic_correlation.generating_wavefield_meta_json

    # We currently only support a single field. I don't know when multiple
    # fields would even be interesting.
    fields = m["forward_run_input"]["output"]["point_data"]["receiver"][0][
        "fields"
    ]
    if len(fields) != 1:
        raise NotImplementedError

    source_gradient_function = _generate_source_gradient_function(
        path_to_generating_wavefield_receiver_file=m["forward_run_input"][
            "output"
        ]["point_data"]["filename"],
        ndim=m["forward_run_input"]["domain"]["dimension"],
        field=fields[0],
        components=source_gradient_components,
    )
    return [w, source_gradient_function]


def run_source_gradient_simulation(
    *,
    simulation_launch_parameters,
    correlation_misfit: CorrelationMisfit,
    source_gradient_components: typing.Optional[typing.List[str]] = None,
    output_folder,
    overwrite: bool,
    interactive_tty: bool = False,
) -> CorrelationData:
    """
    Run a source grdient simulation.

    Args:
        simulation_launch_parameters: The simulation meta parameters.
        correlation_misfit: The correlation misfit object.
        source_gradient_components: The components the source gradient should
            be computed for.
        output_folder: Folder to store the results in.
        overwrite: Allow overwriting of results.
        interactive_tty: Interactive debugging of remote jobs.
    """
    output_folder = io._check_and_create_output_folder(
        output_folder=output_folder, overwrite=overwrite
    )

    tasks = create_source_gradient_tasks(
        correlation_misfit=correlation_misfit,
        source_gradient_components=source_gradient_components,
    )
    tc = sn.api.run_task_chain(
        site_name=simulation_launch_parameters.site_name,
        tasks=tasks,
        ranks=simulation_launch_parameters.ranks_per_job,
        interactive_tty=interactive_tty,
    )

    io.download_source_gradient_data(
        task_chain=tc, output_folder=output_folder, overwrite=overwrite
    )
    return io.read_source_gradient_data(data_folder=output_folder)


def _assemble_adjoint_generating_wavefield_simulation(
    correlation_misfit: CorrelationMisfit, gradient_parameterization: str
):
    """
    Assemble the adjoint generating wavefield simulation.

    Args:
        correlation_misfit: The correlation misfit object.
        gradient_parameterization: Which gradient components to compute.
    """
    meta = (
        correlation_misfit.synthetic_correlation.correlating_wavefield_meta_json  # NOQA
    )
    # Create the adjoint source.
    em = correlation_misfit._event_misfit
    em.write(filename="adjoint_source.h5")
    return sn.simple_config.simulation_generator.create_adjoint_waveform_simulation(  # NOQA
        meta_json_forward_run=meta,
        adjoint_source_file=pathlib.Path("adjoint_source.h5"),
        gradient_parameterization=gradient_parameterization,
        gradient_output_format="hdf5",
    )


def _assemble_adjoint_correlating_wavefield(
    correlation_misfit: CorrelationMisfit,
    adjoint_generating_waveform_simulation,
):
    """
    Assemble the adjoint correlating wavefield.

    Args:
        correlation_misfit: The correlation misfit object.
        adjoint_generating_waveform_simulation: The adjoint generating waveform
            simulation object used as a template.
    """
    # Copy the adjoint generating simulation and derive it from that.
    w = adjoint_generating_waveform_simulation.copy()

    # The adjoint correlating must use the checkpoints of the forward
    # generating wavefield.
    meta_json = correlation_misfit.synthetic_correlation.generating_wavefield_meta_json[  # NOQA
        "forward_run_input"
    ][
        "output"
    ][
        "meta_data"
    ][
        "meta_json_filename"
    ]
    w.adjoint.forward_meta_json_filename = f"REMOTE:{meta_json}"
    # Delete the point source block - the source gradient preprocessing will
    # add it.
    del w.adjoint.point_source_block
    return w


def create_structure_gradient_tasks(
    correlation_misfit: CorrelationMisfit,
    source_gradient: SourceGradient,
    gradient_parameterization: str,
    noise_source_model: BaseNoiseSourceModel,
    temporary_directory: pathlib.Path,
) -> TaskChain:
    """
    Create the tasks required for a structure gradient simulation. These are
    intended to be fed into a task chain.

    Args:
        correlation_misfit: The correlation misfit object.
        source_gradient: The source gradient.
        gradient_parameterization: The gradient parameterization.
        noise_source_model: The noise source model.
        temporary_directory: Temporary directory - only has to exist until the
            task has been submitted.
    """
    if source_gradient.meta_json_adjoint_generating_run is None:
        raise ValueError(
            "The meta.json contents for the adjoint generating wavefield must "
            "be available."
        )

    # The first part of the gradient is pretty simple as it works the same as
    # for normal adjoint simulations and Salvus already has all the
    # infrastructure for it.
    w_adjoint_generating = _assemble_adjoint_generating_wavefield_simulation(
        correlation_misfit=correlation_misfit,
        gradient_parameterization=gradient_parameterization,
    )

    w_adjoint_correlating = _assemble_adjoint_correlating_wavefield(
        correlation_misfit=correlation_misfit,
        adjoint_generating_waveform_simulation=w_adjoint_generating,
    )

    # Store any files that might be necessary for the noise source
    # computation in a temporary directory.
    # XXX: Reuse from forward?
    data_files_to_upload = noise_source_model.get_data_files(
        directory=temporary_directory
    )

    # Get the number of time steps for the forward run.
    we = correlation_misfit.synthetic_correlation.generating_wavefield_meta_json[  # NOQA
        "forward_run_input"
    ][
        "physics"
    ][
        "wave_equation"
    ]
    n_time_steps = (
        round(
            (
                (we["end_time_in_seconds"] - we["start_time_in_seconds"])
                / we["time_step_in_seconds"]
            )
        )
        + 1
    )

    adjoint_generating_wavefield = (
        source_gradient.meta_json_adjoint_generating_run
    )
    adjoint_correlating_wavefield = w_adjoint_correlating

    adjoint_correlating_wavefield.adjoint.point_source_block = {
        "filename": "PROMISE_FILE:task_2_of_2/input/block_source.h5",
        "groups": ["/noise_sources"],
    }
    adjoint_distributed_receivers = adjoint_generating_wavefield[
        "forward_run_input"
    ]["output"]["point_data"]["filename"]

    source_processing_fct = noise_source_model.get_adjoint_source_processing_function(  # NOQA
        path_to_adjoint_distributed_receivers=adjoint_distributed_receivers,
        n_time_steps=n_time_steps,
    )

    return [
        w_adjoint_generating,
        source_processing_fct,
        w_adjoint_correlating,
    ], data_files_to_upload


def run_structure_gradient_simulation(
    *,
    simulation_launch_parameters: SimulationLaunchParameters,
    correlation_misfit: CorrelationMisfit,
    source_gradient: SourceGradient,
    gradient_parameterization: str,
    noise_source_model: BaseNoiseSourceModel,
    output_folder: pathlib.Path,
    overwrite: bool,
    interactive_tty: bool = False,
) -> StructureGradient:
    """
    Run a source gradient simulation and locally store the results.

    Args:
        simulation_launch_parameters: The simulation meta parameters.
        correlation_misfit: The correlation misfit object.
        source_gradient: The source gradient.
        gradient_parameterization: The gradient parameterization.
        noise_source_model: The noise source model.
        output_folder: Folder to store the results in.
        overwrite: Allow overwriting of results.
        interactive_tty: Interactive debugging of remote jobs.
    """
    output_folder = io._check_and_create_output_folder(
        output_folder=output_folder, overwrite=overwrite
    )
    with TemporaryDirectory() as tmp_dir:
        tasks, data_files_to_upload = create_structure_gradient_tasks(
            correlation_misfit=correlation_misfit,
            source_gradient=source_gradient,
            gradient_parameterization=gradient_parameterization,
            noise_source_model=noise_source_model,
            temporary_directory=tmp_dir.name,
        )
        tc = sn.api.run_task_chain(
            site_name=simulation_launch_parameters.site_name,
            tasks=tasks,
            ranks=simulation_launch_parameters.ranks_per_job,
            data_files_to_upload=data_files_to_upload,
            interactive_tty=interactive_tty,
        )

    io.download_structure_gradient_data(
        task_chain=tc, output_folder=output_folder, overwrite=overwrite
    )
    return io.read_structure_gradient_data(data_folder=output_folder)
