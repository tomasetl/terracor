# swp_noise
# Copyright (C) 2022 Mondaic AG
# All rights reserved
"""
Types and utilities related to mapping correlation points to Salvus sources and
receivers.
"""
import typing

import salvus.namespace as sn
from salvus.flow.simple_config import receiver, source

from ..geometry import _detail as _geom_detail
from ..geometry import coordinate as crd
from ..utils.parsing import parse_station_code
from ..utils.types import Dimension, ReceiverType
from .correlation import Correlation, CorrelationCollection
from .correlation_point import CorrelationPoint
from .. import source_mechanism


def correlation_to_event(
    correlation: Correlation, source_time_function
) -> sn.Event:
    """
    Construct a Salvus simulation Event from a Correlation object.

    Args:
        correlation: The correlation object.
    """
    return sn.Event(
        sources=[
            to_virtual_source(
                p=correlation.virtual_source,
                mechanism=correlation.virtual_source_mechanism,
                source_time_function=source_time_function,
            )
        ],
        receivers=[
            to_virtual_receiver(
                p=r, fields=correlation.virtual_receiver_fields
            )
            for r in correlation.virtual_receivers
        ],
    )


def to_correlation_collection(
    entities: typing.Union[
        Correlation, typing.List[Correlation], CorrelationCollection
    ]
) -> CorrelationCollection:
    """
    Turn an appropriate object into a CorrelationCollection.

    Args:
        entities: Either a CorrelationCollection (identity function), a
            Correlation, or a list of correlations (conversion to correlation
            collection).

    Returns:
        A correlation collection.
    """
    return (
        entities
        if isinstance(entities, CorrelationCollection)
        else CorrelationCollection(entities)
    )


def correlation_to_event_name(correlation: Correlation) -> str:
    """
    The correlation's event name is the name of its virtual source.

    Args: The correlation.
    """
    return correlation.virtual_source.name


def coordinate_and_mechanism_to_source_type(
    p: crd.CoordinateType, m: source_mechanism._BaseSourceMechanism
) -> type:
    """
    Get the correct source type for a given coordinate system and mechanism.

    Will automatically return a SideSetSource variant if "depth" is detected as
    the vertical coordinate.

    Args:
        p: The coordinate.
        m: The source mechanism

    Returns:
        The corresponding source type.
    """
    if _geom_detail._dim(p) == Dimension.DIM2:
        if _geom_detail._is_cartesian(p):
            # 2-D cartesian side set source.
            if _geom_detail._is_depth(p.y):
                if isinstance(m, source_mechanism.cartesian.Scalar2D):
                    return source.cartesian.SideSetScalarPoint2D
                elif isinstance(m, source_mechanism.cartesian.Vector2D):
                    return source.cartesian.SideSetVectorPoint2D
                else:
                    raise NotImplementedError
            # 2-D cartesian source.
            else:
                if isinstance(m, source_mechanism.cartesian.Scalar2D):
                    return source.cartesian.ScalarPoint2D
                elif isinstance(m, source_mechanism.cartesian.Vector2D):
                    return source.cartesian.VectorPoint2D
                else:
                    raise NotImplementedError
        # Seismology.
        else:
            # seismo side set.
            if _geom_detail._is_depth(p.z):
                if isinstance(m, source_mechanism.seismology.Vector2D):
                    return source.seismology.SideSetVectorPoint2D
                else:
                    raise NotImplementedError
            # seismo
            else:
                if isinstance(m, source_mechanism.seismology.Vector2D):
                    return source.seismology.VectorPoint2D
                else:
                    raise NotImplementedError
    # 3-d.
    else:
        if _geom_detail._is_cartesian(p):
            # 3-D cartesian side set source.
            if _geom_detail._is_depth(p.z):
                if isinstance(m, source_mechanism.cartesian.Scalar3D):
                    return source.cartesian.SideSetScalarPoint3D
                elif isinstance(m, source_mechanism.cartesian.Vector3D):
                    return source.cartesian.SideSetVectorPoint3D
                else:
                    raise NotImplementedError
            # 2-D cartesian source.
            else:
                if isinstance(m, source_mechanism.cartesian.Scalar3D):
                    return source.cartesian.ScalarPoint3D
                if isinstance(m, source_mechanism.cartesian.Vector3D):
                    return source.cartesian.VectorPoint3D
                else:
                    raise NotImplementedError
        # Seismology.
        else:
            # seismo side set.
            if _geom_detail._is_depth(p.z):
                if isinstance(m, source_mechanism.seismology.Vector3D):
                    return source.seismology.SideSetVectorPoint3D
                else:
                    raise NotImplementedError
            # seismo
            else:
                if isinstance(m, source_mechanism.seismology.Vector3D):
                    return source.seismology.VectorPoint3D
                else:
                    raise NotImplementedError


def coordinate_to_receiver_type(point: crd.CoordinateType) -> ReceiverType:
    """
    Get the correct receiver type for a given coordinate system.

    Will automatically return a SideSetReceiver variant if "depth" is detected
    as the vertical coordinate.

    Args:
        point: The coordinate.

    Returns:
        The corresponding receiver type.
    """

    if _geom_detail._dim(point) == Dimension.DIM2:
        if _geom_detail._is_cartesian(point):
            if _geom_detail._is_depth(point.y):
                return receiver.cartesian.SideSetPoint2D
            else:
                return receiver.cartesian.Point2D
        else:
            if _geom_detail._is_depth(point.z):
                return receiver.seismology.SideSetPoint2D
            else:
                return receiver.seismology.Point2D
    else:
        if _geom_detail._is_cartesian(point):
            if _geom_detail._is_depth(point.z):
                return receiver.cartesian.SideSetPoint3D
            else:
                return receiver.cartesian.Point3D
        else:
            if _geom_detail._is_depth(point.z):
                return receiver.seismology.SideSetPoint3D
            else:
                return receiver.seismology.Point3D


def to_virtual_source(
    p: CorrelationPoint, mechanism, source_time_function
) -> source._BaseSource:
    """
    Convert a point into a virtual source.

    Args:
        p: The correlation point.
        mechanism: The mechanism of the virtual source.
        source_time_function: The source time function.

    Returns:
        A salvus.flow.simple_config source object.
    """
    src = coordinate_and_mechanism_to_source_type(p.point, mechanism)
    kwargs = p.point.__dict__

    if src in (
        sn.simple_config.source.seismology.VectorPoint2D,
        sn.simple_config.source.seismology.VectorPoint3D,
        sn.simple_config.source.seismology.SideSetVectorPoint2D,
        sn.simple_config.source.seismology.SideSetVectorPoint3D,
    ):
        kwargs["depth_in_m"] = kwargs.pop("z")
        # Convert the depth type to a float.
        if hasattr(kwargs["depth_in_m"], "depth"):
            kwargs["depth_in_m"] = kwargs["depth_in_m"].depth

    if src in (
        sn.simple_config.source.seismology.SideSetVectorPoint2D,
        sn.simple_config.source.seismology.SideSetVectorPoint3D,
    ):
        kwargs["side_set_name"] = "r1"

    kwargs.update(mechanism.__dict__)
    return src(**kwargs, source_time_function=source_time_function)


def to_virtual_receiver(
    p: CorrelationPoint, fields: typing.List[str]
) -> receiver._BaseReceiver:
    """
    Convert a point into a virtual receiver.

    Args:
        p: The correlation point.
        max_z: The maximum vertical coordinate of the domain.
        fields: The fields that the receiver should record.

    Returns:
        A salvus.flow.simple_config receiver object.
    """
    return coordinate_to_receiver_type(p.point)(
        **_default_args_rec(p, fields=fields)
    )


def _default_args_rec(
    p: CorrelationPoint, fields: typing.List[str]
) -> typing.Dict:
    """Get the correct default arguments for a correlation receiver."""
    net, sta, loc = parse_station_code(p.name)
    args = _default_args_rec_dispatcher[coordinate_to_receiver_type(p.point)](
        p.point, fields
    )
    return {
        **args,
        "network_code": net,
        "station_code": sta,
        "location_code": loc,
    }


def _default_rec_c_2d(
    p: crd.CartesianCoordinatePair,
    fields: typing.List[str],
) -> typing.Dict:
    """
    Receiver parameters for a 2-D Cartesian Correlation Point (abs crd).
    """
    return {"x": p.x, "y": p.y, "fields": fields}


def _default_rec_c_3d(
    p: crd.CartesianCoordinateTriple,
    fields: typing.List[str],
) -> typing.Dict:
    """
    Receiver parameters for a 3-D Cartesian Correlation Point (abs crd).
    """
    return {"x": p.x, "y": p.y, "z": p.z, "fields": fields}


def _default_rec_s_3d(
    p: crd.SphericalCoordinateTriple,
    fields: typing.List[str],
) -> typing.Dict:
    """
    Receiver parameters for a 3-D Seismology Correlation Point (abs crd).
    """
    return {
        "latitude": p.latitude,
        "longitude": p.longitude,
        "depth_in_m": p.z,
        "fields": fields,
    }


def _default_rec_ss_c_2d(
    p: crd.CartesianCoordinatePair,
    fields: typing.List[str],
) -> typing.Dict:
    """
    Receiver parameters for a 2-D Cartesian Correlation Point (dep crd).
    """
    return {
        "point": (p.x, p.y.depth),
        "fields": fields,
        "side_set_name": "y1",
        "direction": "y",
    }


def _default_rec_ss_s_2d(
    p: crd.SphericalCoordinatePair,
    fields: typing.List[str],
) -> typing.Dict:
    """
    Receiver parameters for a 2-D spherical Correlation Point (dep crd).
    """
    assert isinstance(p.z, crd.Depth)
    return {
        "longitude": p.longitude,
        "radius_of_sphere_in_m": 6371000.0,
        "fields": fields,
        "depth_in_m": p.z.depth,
        "side_set_name": "r1",
    }


_default_args_rec_dispatcher: typing.Dict[
    receiver._BaseReceiver, typing.Callable
] = {
    receiver.cartesian.Point2D: _default_rec_c_2d,
    receiver.cartesian.Point3D: _default_rec_c_3d,
    receiver.seismology.Point3D: _default_rec_s_3d,
    receiver.cartesian.SideSetPoint2D: _default_rec_ss_c_2d,
    receiver.seismology.SideSetPoint2D: _default_rec_ss_s_2d,
}
