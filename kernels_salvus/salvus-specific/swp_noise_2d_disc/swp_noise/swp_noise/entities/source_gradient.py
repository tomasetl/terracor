# swp_noise
# Copyright (C) 2022 Mondaic AG
# All rights reserved
"""
The source gradient object.
"""
from dataclasses import dataclass
import json
import pathlib
import typing

import h5py
import matplotlib.pyplot as plt
import numpy as np
import scipy.interpolate
import xarray as xr

import salvus.namespace as sn
from salvus.flow.collections.event import field_default_components_mapping
from salvus.mesh.elements import side_set_mask


# List of fields to physics.
field_physics_map = {
    "elastic": {
        "displacement",
        "velocity",
        "acceleration",
        "gradient-of-displacement",
        "stress",
        "strain",
    },
    "acoustic": {"phi", "phi_t", "phi_tt", "gradient-of-phi"},
}


def field_to_physics(field: str) -> str:
    """
    A function returning the physics for any given field.

    Args:
        field: The field.
    """
    for key, value in field_physics_map.items():
        if field in value:
            return key
    else:
        raise ValueError(f"Field '{field}' not known.")


@dataclass
class SourceGradient:
    """
    An object representing a source gradient.

    Args:
        data: The raw data values of the source gradient.
        coordinates: The coordinates for each data point.
        field: The recording field.
        components: The components in order.
        data_folder: The folder where things are stored.
        meta_json_adjoint_generating_run: The meta.json file of the adjoint
            generating waveform simulation that created the source gradient.
    """

    data: np.ndarray
    coordinates: np.ndarray
    field: str
    components: typing.List[str]
    data_folder: typing.Optional[pathlib.Path] = None
    meta_json_adjoint_generating_run: typing.Optional[typing.Dict] = None

    def delete_remote_files(self, verbosity: int = 1):
        """
        Delete remote files used to compute the correlation.

        Args:
            verbosity: Verbosity level.
        """
        if not self.data_folder:
            raise ValueError("Source gradient has no data folder.")
        from ..simulations.io import delete_remote_files  # NOQA

        delete_remote_files(data_folder=self.data_folder, verbosity=verbosity)

    def get(self, component: str, ds: xr.Dataset) -> xr.Dataset:
        """
        Get the source gradient interpolated onto an xarray dataset.

        Args:
            component: The component of the source gradient to get.
            ds: The dataset template to interpolate on.
        """
        idx = self.components.index(component)
        c = self.coordinates
        interp = scipy.interpolate.LinearNDInterpolator(
            points=c, values=self.data[:, idx]
        )

        src_grad = ds.copy(deep=True)

        X, Y = np.meshgrid(
            ds.coords["x"].data, ds.coords["y"].data, indexing="ij"
        )
        src_grad["source_amplitude"] = (("x", "y"), -interp(X, Y))
        src_grad = src_grad.fillna(0)

        return src_grad

    def interpolate_source_gradient(
        self, component: str, npts_long_axis: int = 1000
    ):
        """
        Interpolate the source gradient onto a regular grid.

        Args:
            component: The component of the source gradient to get.
            npts_long_axis: The number of samples on the long axis.
        """
        idx = self.components.index(component)
        c = self.coordinates
        interp = scipy.interpolate.LinearNDInterpolator(
            points=c, values=self.data[:, idx]
        )

        range_x, range_y = c.ptp(axis=0)
        npts_x = int(min(1.0, range_x / range_y) * npts_long_axis)
        npts_y = int(min(1.0, range_y / range_x) * npts_long_axis)

        X = np.linspace(c[:, 0].min(), c[:, 0].max(), npts_x)
        Y = np.linspace(c[:, 1].min(), c[:, 1].max(), npts_y)
        X, Y = np.meshgrid(X, Y)
        return X, Y, interp(X, Y)

    def plot_source_gradient(
        self,
        component: str,
        npts_long_axis: int = 1000,
        cmap: str = "bwr_r",
        clip_relative_to_abs_max: float = 1.0,
        figsize: typing.Tuple[int, int] = (10, 6),
        return_figure: bool = False,
    ):
        """
        Plot the source gradient.

        Args:
            component: The component to plot.
            npts_long_axis: The number of samples along the long axis.
            cmap: The colormap to use. Not relevant to all types of plots.
            clip_relative_to_abs_max: Clip relative to the absolute max value.
            figsize: The size of the figure.
            return_figure: Return or show the figure.
        """
        rec = self.meta_json_adjoint_generating_run["forward_run_input"][
            "output"
        ]["point_data"]["receiver"][0]
        rot_matrix = rec.get("rotation_on_output", {})

        # 2-D cartesian.
        if not rot_matrix:
            x, y, z = self.interpolate_source_gradient(
                component=component, npts_long_axis=npts_long_axis
            )
            v = np.abs(z).max() * clip_relative_to_abs_max
            plt.figure(figsize=figsize)
            plt.pcolormesh(x, y, z, shading="auto", cmap=cmap, vmin=-v, vmax=v)
            plt.gca().set_aspect("equal")
            plt.title(f"Source gradient for component {component}")
            plt.colorbar(
                extend="both" if clip_relative_to_abs_max < 1.0 else None
            )
            if return_figure:
                return plt.gcf()
            plt.show()
        # Polar
        elif len(rot_matrix["components"]) == 2:
            idx = self.components.index(component)
            v = self.data[:, idx]

            x = self.coordinates[:, 0]
            y = self.coordinates[:, 1]
            theta = np.arctan2(y, x)

            # Must be sorted for this plot to work.
            s = sorted(
                [(_t, _v) for _t, _v in zip(theta, v)], key=lambda x: x[0]
            )
            theta = np.array([_s[0] for _s in s])
            v = np.array([_s[1] for _s in s])

            from swp_noise.noise_source_models.circular import (
                plot_polar_array,
            )  # NOQA

            plot_polar_array(
                theta=theta, v=v, show=not return_figure, figsize=figsize
            )
        else:
            raise NotImplementedError

    def project_source_gradient_on_mesh(
        self, component, noise_source_model, simulation_template
    ) -> sn.UnstructuredMesh:
        """
        Project the source gradient onto the mesh.

        Args:
            component: The component of the source gradient to project.
            noise_source_model: The noise source model.
            simulation_template: The simulation template.
        """
        idx = self.components.index(component)
        c = self.coordinates
        interp = scipy.interpolate.NearestNDInterpolator(
            x=c, y=self.data[:, idx]
        )

        mesh = simulation_template._get_unstructured_mesh_object()

        # And interpolate onto the side set elements.
        n = mesh.get_element_nodes()
        ss_elem, ss_side = mesh.side_sets[noise_source_model.side_set]
        values = np.zeros((n.shape[0], n.shape[1]))
        values[ss_elem] = interp(n[ss_elem])

        # There is probably a nicer way to do this - Set all the sides in the
        # elements that are not parts of the side set to 0.
        ssm = np.logical_not(
            np.array(side_set_mask(mesh.shape_order + 1, mesh.ndim))
        )
        for i in range(len(ss_elem)):
            elem = ss_elem[i]
            values[elem, ssm[ss_side][i]] = 0.0

        # Finally attach to a fresh copy of the mesh.
        m = mesh.copy()
        m.elemental_fields.clear()
        m.attach_field("source_amplitude", values)
        return m

    def write(self, filename: typing.Union[str, pathlib.Path]):
        """
        Write the source gradient to a file.

        Args:
            filename: Filename to write to.
        """
        with h5py.File(filename, mode="w") as f:
            f["data"] = self.data
            f["coordinates"] = self.coordinates
            f["data"].attrs["field"] = self.field
            f["data"].attrs["components"] = ",".join(self.components)

    @classmethod
    def read(cls, filename: typing.Union[str, pathlib.Path]):
        """
        Read a file to a source gradient object.

        Args:
            filename: File to read from.
        """
        with h5py.File(filename, mode="r") as f:
            data = f["data"][:]
            coordinates = f["coordinates"][:]
            field = f["data"].attrs["field"]
            components = f["data"].attrs["components"].split(",")
        return cls(
            data=data,
            coordinates=coordinates,
            field=field,
            components=components,
        )


def compute_source_gradient(
    output_generating_wavefield: pathlib.Path,
    output_adjoint_generating_wavefield: pathlib.Path,
    ndim: int,
    field: str,
    components: typing.Optional[typing.List] = None,
):
    """
    Compute a source gradient from the distributed receiver output
    of the forward and adjoint generating wavefield simulations.

    Args:
        output_generating_wavefield: Path to the generating wavefield
            simulation folder.
        output_adjoint_generating_wavefield: Path to the adjoint generating
            wavefield simulation folder.
        ndim: The dimensionality of the problem.
        field: The recorded fields of the distributed receivers.
        components: The components to compute the source gradient for.
    """
    # Open the meta.json file of the generating wavefield - we need to figure
    # out if things are being rotated or not.
    generating_meta = json.load(
        (pathlib.Path(output_generating_wavefield).parent / "meta.json").open(
            mode="r"
        )
    )
    # Get the first receiver.
    rec = generating_meta["forward_run_input"]["output"]["point_data"][
        "receiver"
    ][0]

    # Computing the source gradient on only one or a few of all available
    # components should be possible.
    if rec.get("rotation_on_output", {}):
        # Assume all receiver either have the same rotated components or not.
        all_components = rec["rotation_on_output"]["components"]
    else:
        if field not in field_default_components_mapping[ndim]:
            raise ValueError(
                f"Field {field} not known. Available "
                "fields: "
                f"{list(field_default_components_mapping[ndim].keys())}"
            )
        all_components = field_default_components_mapping[ndim][field]

    physics = field_to_physics(field=field).upper()

    if components is None:
        components = all_components
        component_indices = list(range(len(all_components)))
    else:
        # First make sure all specified components are available.
        for c in components:
            if c not in all_components:
                raise ValueError(
                    f"Component '{c}' not valid for field "
                    f"'{field}'. Available components: {all_components}"
                )

        component_indices = [all_components.index(c) for c in components]

    r1 = output_generating_wavefield
    r2 = output_adjoint_generating_wavefield

    with h5py.File(r1, mode="r") as f1, h5py.File(r2, mode="r") as f2:
        coordinates = f1[f"coordinates_{physics}_point"][:]
        # XXX: Need to map things if they have been run with different
        # number of ranks.
        c2 = f2[f"coordinates_{physics}_point"][:5]
        if not np.all(np.isclose(coordinates[:5], c2)):
            raise ValueError

        d1 = f1["point"][field][:, component_indices, :]
        d2 = f2["point"][field][:, component_indices, :]

    d2 = d2[:, :, -d1.shape[2] :]  # NOQA
    d1 *= d2

    source_gradient = d1.sum(axis=-1)

    return SourceGradient(
        data=source_gradient,
        coordinates=coordinates,
        field=field,
        components=components,
    )
