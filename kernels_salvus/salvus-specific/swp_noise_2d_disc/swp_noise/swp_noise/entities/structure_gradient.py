# swp_noise
# Copyright (C) 2022 Mondaic AG
# All rights reserved
"""
The structure gradient class.
"""
from dataclasses import dataclass
import json
import pathlib
import typing

import salvus.namespace as sn


@dataclass
class StructureGradient:
    """
    An object representing a structure gradient.

    Args:
        gradient_adjoint_generating_wavefield: The gradient from the adjoint
            generating wavefield simulation.
        gradient_adjoint_correlating_wavefield: The gradient from the adjoint
            correlating wavefield.
        meta_json_adjoint_generating_run: The meta.json from the adjoint
            generating run.
        meta_json_adjoint_correlating_run:
            The meta.json from the adjoint correlating run.
        data_folder: The folder where everything is stored.
    """

    gradient_adjoint_generating_wavefield: sn.UnstructuredMesh
    gradient_adjoint_correlating_wavefield: sn.UnstructuredMesh
    meta_json_adjoint_generating_run: typing.Optional[typing.Dict] = None
    meta_json_adjoint_correlating_run: typing.Optional[typing.Dict] = None
    data_folder: typing.Optional[pathlib.Path] = None

    @classmethod
    def read(cls, data_folder: pathlib.Path):
        """
        Read a data folder to a structure gradient object.

        Args:
            data_folder: The folder where the individual components of the
            structure gradient are stored.
        """
        from ..simulations.io import _check_data_folder  # NOQA

        data_folder = _check_data_folder(data_folder)
        adjoint_correlating_output = (
            data_folder / "adjoint_correlating_wavefield_output"
        )
        adjoint_generating_output = (
            data_folder / "adjoint_generating_wavefield_output"
        )

        with open(adjoint_correlating_output / "meta.json", "r") as fh:
            meta_corr = json.load(fh)
        with open(adjoint_generating_output / "meta.json", "r") as fh:
            meta_gen = json.load(fh)

        return cls(
            gradient_adjoint_generating_wavefield=sn.UnstructuredMesh.from_h5(
                adjoint_generating_output / "gradient.h5"
            ),
            gradient_adjoint_correlating_wavefield=sn.UnstructuredMesh.from_h5(
                adjoint_correlating_output / "gradient.h5"
            ),
            meta_json_adjoint_generating_run=meta_gen,
            meta_json_adjoint_correlating_run=meta_corr,
            data_folder=data_folder,
        )

    def get(self) -> sn.UnstructuredMesh:
        """
        Get the full structure gradient.
        """
        m = self.gradient_adjoint_correlating_wavefield.copy()
        for name, v in m.element_nodal_fields.items():
            if name in ["FemMassMatrix", "Valence"]:
                continue
            v += self.gradient_adjoint_generating_wavefield.element_nodal_fields[  # NOQA
                name
            ]

        return m

    def delete_remote_files(self, verbosity: int = 1):
        """
        Delete remote files used to compute the correlation.

        Args:
            verbosity: Verbosity level.
        """
        from ..simulations.io import delete_remote_files  # NOQA

        if not self.data_folder:
            raise ValueError("Structure gradient has no data folder.")

        delete_remote_files(data_folder=self.data_folder, verbosity=verbosity)
