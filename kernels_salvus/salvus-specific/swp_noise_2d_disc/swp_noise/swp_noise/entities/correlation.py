# swp_noise
# Copyright (C) 2022 Mondaic AG
# All rights reserved
"""
The correlation class.
"""
from dataclasses import dataclass
import typing

from .correlation_point import CorrelationPoint
from ..source_mechanism import _BaseSourceMechanism


@dataclass
class Correlation:
    """
    Construct a correlation from a virtual source and a list of virtual
    receivers.

    Represents the computation of the correlation wavefield originating at
    the virtual source, and recorded at the virtual receivers.

    Args:
        virtual_source: The virtual source.
        virtual_source_mechanism: The source mechanism of the virtual
            source.
        virtual_receivers: The virtual receivers.
        virtual_receiver_fields: The fields to record the virtual receivers
            on.
    """

    virtual_source: CorrelationPoint
    virtual_source_mechanism: _BaseSourceMechanism
    virtual_receivers: typing.List[CorrelationPoint]
    virtual_receiver_fields: typing.List[str]


@dataclass(init=False)
class CorrelationCollection:
    correlations: typing.List[Correlation]

    def __init__(self, correlations: typing.List[Correlation]):
        """
        Construct a correlation collection from a correlation or list thereof.

        Args:
            correlation: The correlation (or list thereof) from which to make a
                collection collection.
        """
        if isinstance(correlations, Correlation):
            correlations = [correlations]
        self.correlations = correlations

    @classmethod
    def from_correlation_points(
        cls,
        correlation_points: typing.List[CorrelationPoint],
        virtual_source_mechanism: _BaseSourceMechanism,
        virtual_receiver_fields: typing.List[str],
    ):
        """
        Create a correlation collection by permutationing correlation points.

        For example, given 3 correlation points will create a correlation
        collection with three correlations:

            (c0, [c1, c2])
            (c1, [c0, c2])
            (c2, [c0, c1])

        Args:
            correlation_points: The list of correlation points to permute.
            virtual_source_mechanism: The source mechanism of the virtual
                source.
            virtual_receiver_fields: The fields to record the virtual receivers
                on.

        Returns:
            A correlation collection from the permuted correlation points.
        """

        return cls(
            [
                Correlation(
                    virtual_source=vs,
                    virtual_source_mechanism=virtual_source_mechanism,
                    virtual_receivers=[
                        vr
                        for (_j, vr) in enumerate(correlation_points)
                        if _i != _j
                    ],
                    virtual_receiver_fields=virtual_receiver_fields,
                )
                for (_i, vs) in enumerate(correlation_points)
            ]
        )
