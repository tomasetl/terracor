# swp_noise
# Copyright (C) 2022 Mondaic AG
# All rights reserved
"""
The correlation point class.
"""
from dataclasses import dataclass
import swp_noise.geometry.coordinate as crd


@dataclass
class CorrelationPoint:
    """
    Construct a correlation point from a coordinate point.

    Args:
        point: The coordinate point. Can be defined in any reference frame
            supported by SWPNoise (currently: Cartesian or spherical).
        name: A name to refer to this point by.
    """

    point: crd.CoordinateType
    name: str
