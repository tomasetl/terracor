# swp_noise
# Copyright (C) 2022 Mondaic AG
# All rights reserved
"""
The inversion data container.
"""
import copy
import pathlib
import typing

from salvus.namespace import WavefieldCompression, Mapping
from salvus.opt.models import StructuredModel

import swp_noise


class InversionDataContainer:
    def __init__(
        self,
        working_dir: typing.Union[str, pathlib.Path],
        inversion_variable: str,
        simulation_launch_parameters,
        simulation_template,
        correlation_collection,
        stf,
        simulation_history_in_s,
        lag_time_in_s,
        prior_structure_model,
        prior_noise_model,
        receiver_field: str,
        source_gradient_components: str,
        misfit_function: str,
        observed_correlation_data=None,
        extra_kwargs_misfit_function=None,
        wavefield_compression=WavefieldCompression(
            forward_wavefield_sampling_interval=10
        ),
        structure_model_mapping: typing.Optional[Mapping] = None,
        structure_gradient_parameterization: typing.Optional[str] = None,
    ):

        self.simulation_launch_parameters = simulation_launch_parameters
        self.simulation_template = simulation_template
        self.correlation_collection = correlation_collection
        self.stf = stf
        self.simulation_history_in_s = simulation_history_in_s
        self.lag_time_in_s = lag_time_in_s
        self.prior_structure_model = prior_structure_model
        self.prior_noise_model = prior_noise_model
        self.wavefield_compression = wavefield_compression
        self.working_dir = pathlib.Path(working_dir)
        self.observed_correlation_data = observed_correlation_data
        self.misfit_function = misfit_function
        self.extra_kwargs_misfit_function = extra_kwargs_misfit_function
        self.receiver_field = receiver_field

        # perform a few validation checks
        if inversion_variable not in ["source", "structure"]:
            raise ValueError(
                "Invalid `inversion_variable`. "
                "Must be either `source` or `structure`."
            )

        self.inversion_variable = inversion_variable

        self.source_gradient_components = source_gradient_components

        self.structure_gradient_parameterization = (
            structure_gradient_parameterization
        )
        self.structure_model_mapping = structure_model_mapping

        if self.working_dir.is_dir():
            is_empty = not any(pathlib.Path(self.working_dir).iterdir())
            if not is_empty:
                raise ValueError(
                    f"Working directory {self.working_dir} exists, "
                    "but it is not empty."
                )
        else:
            self.working_dir.mkdir(exist_ok=False, parents=True)

    def preconditioner(self, task, iteration):
        return None

    def _prepare_inputs(
        self,
        task,
        iteration,
    ):

        if task.model == iteration.model.name:
            model = iteration.model
        else:
            model = iteration.auxiliary_models[task.model]["model"]

        sim = copy.deepcopy(self.simulation_template)

        if self.inversion_variable == "source":
            # update noise model
            noise_source_model = copy.deepcopy(self.prior_noise_model)
            noise_source_model.source_amplitude = model.model

        elif self.inversion_variable == "structure":
            # update structural model
            sim.set_mesh(
                self.structure_model_mapping.forward(
                    model=model, prior=self.prior_structure_model
                )
            )
            noise_source_model = self.prior_noise_model

        return sim, noise_source_model

    def _compute_single_misfit_and_or_gradient(
        self,
        name,
        correlation_idx,
        simulation,
        noise_source_model,
        task,
    ):

        if task not in ["misfit", "gradient", "misfit_and_gradient"]:
            raise ValueError(f"Unknown task `{task}`.")

        assert (
            name
            == self.correlation_collection[correlation_idx].virtual_source.name
        )

        print(f"Processing task `{task}` for event `{name}`.")

        corr = swp_noise.simulations.run_correlation_simulation(
            simulation_launch_parameters=self.simulation_launch_parameters,
            simulation_template=simulation,
            correlation=self.correlation_collection[correlation_idx],
            stf=self.stf,
            simulation_history_in_s=self.simulation_history_in_s,
            lag_time_in_s=self.lag_time_in_s,
            noise_source_model=noise_source_model,
            wavefield_compression=self.wavefield_compression,
            output_folder=pathlib.Path(
                self.working_dir, f"correlation_misfit__{name}"
            ),
            overwrite=True,
        )

        correlation_misfit = swp_noise.CorrelationMisfit(
            synthetic_correlation=corr,
            observed_correlation=self.observed_correlation_data[
                correlation_idx
            ],
            misfit_function=self.misfit_function,
            extra_kwargs_misfit_function=self.extra_kwargs_misfit_function,
            receiver_field=self.receiver_field,
        )

        if task == "misfit":
            return correlation_misfit.misfit_value, None, None

        src_gradient = swp_noise.simulations.run_source_gradient_simulation(
            simulation_launch_parameters=self.simulation_launch_parameters,
            correlation_misfit=correlation_misfit,
            source_gradient_components=self.source_gradient_components,
            output_folder=pathlib.Path(
                self.working_dir, f"source_gradient__{name}"
            ),
            overwrite=True,
        )

        if self.inversion_variable == "source":
            return correlation_misfit.misfit_value, src_gradient, None

        str_gradient = swp_noise.simulations.run_structure_gradient_simulation(
            simulation_launch_parameters=self.simulation_launch_parameters,
            correlation_misfit=correlation_misfit,
            source_gradient=src_gradient,
            gradient_parameterization=self.structure_gradient_parameterization,
            noise_source_model=noise_source_model,
            output_folder=pathlib.Path(
                self.working_dir, f"structure_gradient__{name}"
            ),
            overwrite=True,
        )

        return correlation_misfit.misfit_value, src_gradient, str_gradient

    def compute_misfit_and_gradient(
        self,
        task,
        iteration,
    ):

        sim, noise_source_model = self._prepare_inputs(task, iteration)

        misfits = {}
        gradients = {}
        for _i, event in enumerate(task.events):

            (
                chi,
                grad_src,
                grad_str,
            ) = self._compute_single_misfit_and_or_gradient(
                name=event,
                correlation_idx=_i,
                simulation=sim,
                noise_source_model=noise_source_model,
                task="misfit_and_gradient",
            )
            grad_src.plot_source_gradient(
                component="A", clip_relative_to_abs_max=0.25
            )

            if self.inversion_variable == "structure":
                mg = self.structure_model_mapping.adjoint(
                    mesh=grad_str.get(),
                    prior=self.prior_structure_model,
                    name=f"model_str_g_{event}",
                    event=event,
                )

                misfits[event] = chi
                gradients[event] = mg

            elif self.inversion_variable == "source":
                misfits[event] = chi
                gradients[event] = StructuredModel(
                    name=f"model_src_g_{event}",
                    model=grad_src.get(
                        component=self.source_gradient_components,
                        ds=noise_source_model.source_amplitude,
                    ),
                    fields=["source_amplitude"],
                )

        if task.name == "gradient":

            task.update_results(gradients=gradients)

        else:

            task.update_results(misfits=misfits, gradients=gradients)

    def compute_misfit(
        self,
        task,
        iteration,
    ):

        sim, noise_source_model = self._prepare_inputs(task, iteration)

        misfits = {}
        for _i, event in enumerate(task.events):
            assert self.correlation_collection[_i].virtual_source.name == event
            (
                chi,
                grad_src,
                grad_str,
            ) = self._compute_single_misfit_and_or_gradient(
                name=event,
                correlation_idx=_i,
                simulation=sim,
                noise_source_model=noise_source_model,
                task="misfit",
            )
            misfits[event] = chi

        task.update_results(
            misfits=misfits,
        )
