# swp_noise
# Copyright (C) 2022 Mondaic AG
# All rights reserved
"""
The correlation misfit class.
"""
import inspect
import typing

import salvus.namespace as sn

from .correlation_data import CorrelationData


class CorrelationMisfit:
    def __init__(
        self,
        *,
        synthetic_correlation: CorrelationData,
        observed_correlation: typing.Optional[CorrelationData] = None,
        misfit_function: typing.Union[str, typing.Callable],
        extra_kwargs_misfit_function: typing.Optional[typing.Dict] = None,
        receiver_field: str,
        max_samples_for_misfit_computation: typing.Optional[int] = None,
        temporal_weights_function: typing.Optional[typing.Callable] = None,
    ):
        """
        An object representing a misfit configuration for a given correlation.

        Args:
            synthetic_correlation: The synthetic correlation data.
            observed_correlation: The observed correlation data - not necessary
                for all misfit functions.
            misfit_function: Name of the misfit function to use or the actual
                function.
            extra_kwargs_misfit_function: Extra keyword arguments for the
                misfit function.
            receiver_field: The receiver field to compute the misfit for.
            max_samples_for_misfit_computation: The maximum number of samples
                for the misfit computation.
            temporal_weights_function: Temporal weights function.
        """
        # Make sure the function is valid if it is passed.
        if temporal_weights_function:
            sn.EventData._validate_temporal_weights_function(
                function=temporal_weights_function
            )

        if extra_kwargs_misfit_function:
            extra_kwargs = extra_kwargs_misfit_function.copy()
        else:
            extra_kwargs = None

        if isinstance(misfit_function, typing.Callable):
            # We'll do a bit of special handling here. Salvus itself, at the
            # time of writing this, has no notion of where time zero is in the
            # data during the misfit computation as its misfits don't need it.
            # For any kind of causal/acausal asymmetry measurement that is
            # vital though.
            # What we'll do here is that we inject the time of the first sample
            # if the misfit function has "time_of_first_sample_in_seconds" as
            # an argument.
            signature = inspect.signature(misfit_function)
            key = "time_of_first_sample_in_seconds"
            if key in signature.parameters:
                if extra_kwargs is None:
                    extra_kwargs = {}
                elif key in extra_kwargs:
                    raise ValueError(
                        f"The value for '{key}' will be automatically set. "
                        "Don't set it manually."
                    )

                extra_kwargs[
                    key
                ] = synthetic_correlation.correlating_wavefield_meta_json[
                    "forward_run_input"
                ][
                    "physics"
                ][
                    "wave_equation"
                ][
                    "start_time_in_seconds"
                ]

        self.synthetic_correlation = synthetic_correlation
        self.observed_correlation = observed_correlation
        self.misfit_function = misfit_function
        self.extra_kwargs_misfit_function = extra_kwargs
        self.receiver_field = receiver_field
        self.max_samples_for_misfit_computation = (
            max_samples_for_misfit_computation
        )
        self.temporal_weights_function = temporal_weights_function

        observed_event = (
            self.observed_correlation.correlation_data
            if self.observed_correlation
            else None
        )
        synthetic_event = self.synthetic_correlation.correlation_data

        if self.temporal_weights_function:
            if observed_event:
                observed_event = observed_event.shallow_copy()
                observed_event.register_temporal_weights_function(
                    self.temporal_weights_function
                )
            synthetic_event = synthetic_event.shallow_copy()
            synthetic_event.register_temporal_weights_function(
                self.temporal_weights_function
            )

        # Piggy-back on Salvus' built in misfit computation infrastructure.
        self._event_misfit = sn.EventMisfit(
            observed_event=observed_event,
            synthetic_event=synthetic_event,
            misfit_function=self.misfit_function,
            extra_kwargs_misfit_function=self.extra_kwargs_misfit_function,
            receiver_field=self.receiver_field,
            max_samples_for_misfit_computation=self.max_samples_for_misfit_computation,  # NOQA
        )

    @property
    def misfit_value(self) -> float:
        """
        Cumulative misfit across all correlations.
        """
        return self._event_misfit.misfit_value

    @property
    def misfit_per_receiver_and_component_and_weight_set(
        self,
    ) -> typing.Dict[str, typing.Dict[str, typing.List]]:
        """
        Return the misfit per receiver and component and weight set.
        """
        return (
            self._event_misfit.misfit_per_receiver_and_component_and_weight_set
        )
