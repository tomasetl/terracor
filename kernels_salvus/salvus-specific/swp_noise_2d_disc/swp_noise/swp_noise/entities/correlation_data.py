# swp_noise
# Copyright (C) 2022 Mondaic AG
# All rights reserved
"""
The main correlation data class for swp_noise.
"""
from dataclasses import dataclass
import pathlib
import typing

import matplotlib.pyplot as plt
import numpy as np

import salvus.namespace as sn


@dataclass
class CorrelationData:
    """
    An object representing correlation data.

    Args:
        correlation_data: The correlation data as a Salvus EventData object.
        generating_wavefield_meta_json: The meta.json file of the generating
            wavefield simulation. Only necessary for synthetics that are used
            for subsequent gradient simulations.
        correlating_wavefield_meta_json: The meta.json file of the correlating
            wavefield simulation. Only necessary for synthetics that are used
            for subsequent gradient simulations.
        stdout: The stdout dictionary of the task chain.
        stderr:The stderr dictionary of the task chain.
        data_folder: The folder on disc of all the information.
    """

    correlation_data: sn.EventData
    generating_wavefield_meta_json: typing.Optional[typing.Dict] = None
    correlating_wavefield_meta_json: typing.Optional[typing.Dict] = None
    stdout: typing.Optional[typing.Dict] = None
    stderr: typing.Optional[typing.Dict] = None
    data_folder: typing.Optional[pathlib.Path] = None

    def delete_remote_files(self, verbosity: int = 1):
        """
        Delete remote files used to compute the correlation.

        Args:
            verbosity: Verbosity level.
        """
        from ..simulations.io import delete_remote_files  # NOQA

        if not self.data_folder:
            raise ValueError("Structure gradient has no data folder.")

        delete_remote_files(data_folder=self.data_folder, verbosity=verbosity)

    def plot_correlation(
        self,
        index: int,
        receiver_field: str,
        temporal_weights_function: typing.Optional[typing.Callable] = None,
    ):
        """
        Plot correlations.

        Args:
            index: Virtual receiver index to plot.
            receiver_field: The receiver field to plot.
            temporal_weights_function: Optionally pass a temporal weights
                function to this visualization routine.
        """
        # Make sure the function is valid.
        if temporal_weights_function:
            sn.EventData._validate_temporal_weights_function(
                function=temporal_weights_function
            )

        ed = self.correlation_data

        receiver_name = ed.receiver_name_list[index]
        st = ed.get_waveform_data(receiver_name, receiver_field)

        weights = {}
        if temporal_weights_function:
            rec = ed.get_receiver(receiver_name=receiver_name)
            weights = temporal_weights_function(
                st=st, receiver=rec, sources=ed.sources
            )
            unweighted_st = st.copy()
            st = sn.EventData._apply_weights_to_stream(
                weights=weights, stream=st
            )

        plt.figure(figsize=(15, 3 * len(st)))
        plt.suptitle(f"Receiver name: {receiver_name}")

        for i, tr in enumerate(st):
            component = tr.stats.channel[-1]
            times = tr.times() + tr.stats.starttime.timestamp
            plt.subplot(len(st), 1, i + 1)
            plt.grid()
            plt.title(f"Component {component}")
            if component in weights:
                unweighted_tr = unweighted_st.select(component=component)[0]
                plt.plot(
                    times,
                    unweighted_tr.data,
                    color="0.8",
                    label="Unweighted Data",
                )
                if "_salvus_temporal_weights" in tr.stats:
                    plt.plot(
                        times,
                        tr.stats._salvus_temporal_weights
                        * np.abs(unweighted_tr.data).max(),
                        color="green",
                        ls="--",
                        label="Temporal Weights",
                    )

            plt.plot(times, tr.data, label="Data")
            plt.legend()
        plt.tight_layout()
        plt.show()
