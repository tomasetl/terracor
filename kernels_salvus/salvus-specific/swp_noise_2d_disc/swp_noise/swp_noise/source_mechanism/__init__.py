# swp_noise
# Copyright (C) 2022 Mondaic AG
# All rights reserved
"""
A collection of simplistic source mechanisms.
"""
from dataclasses import dataclass


@dataclass
class _BaseSourceMechanism:
    pass


# Do all of these at the end to avoid circular imports.
from . import cartesian  # NOQA
from . import seismology  # NOQA
