# swp_noise
# Copyright (C) 2022 Mondaic AG
# All rights reserved
"""
Cartesian source mechanisms.

These largely correspond to various Salvus sources.
"""
from dataclasses import dataclass
from . import _BaseSourceMechanism


@dataclass
class Scalar2D(_BaseSourceMechanism):
    """
    Scalar source mechanism in 2-D.

    Args:
        f: The scalar force in Nm.
    """

    f: float


@dataclass
class Vector2D(_BaseSourceMechanism):
    """
    Vectorial source mechanism in 2-D.

    Args:
        fx: The x component of the vectorial force vector in N.
        fy: The y component of the vectorial force vector in N.
    """

    fx: float
    fy: float


@dataclass
class Scalar3D(_BaseSourceMechanism):
    """
    Scalar source mechanism in 3-D.

    Args:
        f: The scalar force in Nm.
    """

    f: float


@dataclass
class Vector3D(_BaseSourceMechanism):
    """
    Vectorial source mechanism in 3-D.

    Args:
        fx: The x component of the vectorial force vector in N.
        fy: The y component of the vectorial force vector in N.
        fz: The z component of the vectorial force vector in N.
    """

    fx: float
    fy: float
    fz: float
