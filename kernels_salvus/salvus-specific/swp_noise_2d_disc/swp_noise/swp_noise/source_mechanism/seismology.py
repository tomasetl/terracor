# swp_noise
# Copyright (C) 2022 Mondaic AG
# All rights reserved
"""
Seismological source mechanisms.

These largely correspond to various Salvus sources.
"""
from dataclasses import dataclass
from . import _BaseSourceMechanism


@dataclass
class Vector3D(_BaseSourceMechanism):
    """
    Seismological vectorial source mechanism in 3-D.

    Args:
        fr: The radial component of the vectorial force vector in N.
        ft: The theta component of the vectorial force vector in N.
        fp: The phi component of the vectorial force vector in N.
    """

    fr: float
    ft: float
    fp: float


@dataclass
class Vector2D(_BaseSourceMechanism):
    """
    Seismological vectorial source mechanism in 2-D.

    Args:
        fr: The radial component of the vectorial force vector in N.
        fp: The phi component of the vectorial force vector in N.
    """

    fr: float
    fp: float
