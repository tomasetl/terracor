# swp_noise
# Copyright (C) 2022 Mondaic AG
# All rights reserved
"""
Visualization submodule.
"""
import typing

import matplotlib.pyplot as plt
import numpy as np
from plotly.graph_objs._figurewidget import FigureWidget
import scipy.interpolate

from salvus.mesh.elements import side_set_mask
import salvus.namespace as sn
from salvus.namespace import domain as salvus_domain

from swp_noise.entities.correlation import Correlation
from swp_noise.entities.correlation_point import CorrelationPoint
from swp_noise.geometry._detail import _to_matplotlib_form
from swp_noise.utils.utils import max_vertical_coordinate, to_list

PlotableType = typing.Union[
    Correlation,
    CorrelationPoint,
    typing.List[CorrelationPoint],
]

_virtual_source_plot_params = {
    "s": 200,
    "zorder": 10,
    "color": "darkorchid",
    "edgecolor": "0.1",
}

_virtual_receiver_plot_params = {
    "s": 50,
    "zorder": 11,
    "color": "red",
    "edgecolor": "0.1",
}


def _get_coordinates(
    points: typing.List[CorrelationPoint], domain: salvus_domain.Domain
) -> np.ndarray:
    """
    Correlation points to coordinatese.
    """
    lr = np.zeros((len(points), domain.dim))
    for _i, p in enumerate(points):
        lr[_i, :] = _to_matplotlib_form(
            p.point, max_vertical_coordinate(domain)
        )

    return lr


def plot_domain(
    domain: salvus_domain.Domain,
    entity: typing.Optional[PlotableType] = None,
    show: bool = True,
) -> typing.Optional[plt.Figure]:
    """
    Plot the domain outline, optionally with correlation points overlain.

    Args:
        domain: The domain to plot.
        entity: The correlation object to plot the correlation points of.
            Defaults to None.
        show: If True, show the plot, otherwise return the figure.

    Returns:
        A Matplotlib figure.
    """

    if isinstance(domain, salvus_domain.dim3.BoxDomain):
        d_plot = domain.plot()
    else:
        d_plot = domain.plot(return_figure=True)

    vs_data: typing.Optional[np.ndarray]
    vr_data: typing.Optional[np.ndarray]
    if isinstance(entity, Correlation):

        vs_data, vs_name = (
            np.atleast_2d(
                _to_matplotlib_form(
                    entity.virtual_source.point,
                    max_vertical_coordinate(domain),
                )
            ),
            entity.virtual_source.name,
        )

        vr_data, vr_names = (
            np.atleast_2d(_get_coordinates(entity.virtual_receivers, domain)),
            [vr.name for vr in entity.virtual_receivers],
        )

    elif entity is not None:

        cps = entity if isinstance(entity, list) else [entity]
        vs_data, vr_data, vr_names = (
            None,
            np.atleast_2d(_get_coordinates(to_list(entity), domain)),
            [cp.name for cp in cps],
        )

    else:
        vs_data, vr_data = None, None

    if isinstance(d_plot, FigureWidget):

        import plotly.graph_objects as go  # NOQA

        if vs_data is not None:
            d_plot.add_trace(
                go.Scatter3d(
                    x=vs_data[:, 0],
                    y=vs_data[:, 1],
                    z=vs_data[:, 2],
                    text=vs_name,
                    mode="markers",
                    name="Virtual Sources",
                    marker={"color": "rgb(153, 50, 204)", "size": 15},
                )
            )

        if vr_data is not None:
            d_plot.add_trace(
                go.Scatter3d(
                    x=vr_data[:, 0],
                    y=vr_data[:, 1],
                    z=vr_data[:, 2],
                    text=vr_names,
                    mode="markers",
                    name="Correlation Points"
                    if vs_data is None
                    else "Virtual Receivers",
                    marker={"color": "rgb(255, 0, 0)", "size": 15},
                )
            )

    else:

        axis = d_plot.get_axes()[0]
        if isinstance(entity, Correlation):
            axis.scatter(
                *np.squeeze(vs_data),
                **_virtual_source_plot_params,
                label="Virtual source",
            )

            axis.scatter(
                vr_data[:, 0],
                vr_data[:, 1],
                **_virtual_receiver_plot_params,
                label="Virtual receivers",
            )

            axis.legend()

        else:

            axis.scatter(
                vr_data[:, 0],
                vr_data[:, 1],
                **_virtual_receiver_plot_params,
                label="Correlation Point",
            )

    if show and isinstance(d_plot, plt.Figure):
        plt.show()
    else:
        return d_plot


def project_noise_source_model_on_mesh(
    noise_source_model, simulation_template
) -> sn.UnstructuredMesh:
    """
    Helper function projecting a surface noise source model onto a 3-D mesh to
    aid visualizations.

    Args:
        noise_source_model: The noise source model.
        simulation_template: The simulation template.
    """
    recs, weights = noise_source_model.get_distributed_receivers_and_weights(
        generating_wavefield=simulation_template
    )

    coordinates = np.array([r.location for r in recs])
    mesh = simulation_template._get_unstructured_mesh_object()

    # Get a linear interpolator.
    interp = scipy.interpolate.NearestNDInterpolator(x=coordinates, y=weights)

    # And interpolate onto the side set elements.
    n = mesh.get_element_nodes()
    ss_elem, ss_side = mesh.side_sets[noise_source_model.side_set]
    values = np.zeros((n.shape[0], n.shape[1]))
    values[ss_elem] = interp(n[ss_elem])

    # There is probably a nicer way to do this - Set all the sides in the
    # elements that are not parts of the side set to 0.
    ssm = np.logical_not(
        np.array(side_set_mask(mesh.shape_order + 1, mesh.ndim))
    )
    for i in range(len(ss_elem)):
        elem = ss_elem[i]
        values[elem, ssm[ss_side][i]] = 0.0

    # Finally attach to a fresh copy of the mesh.
    m = mesh.copy()
    m.elemental_fields.clear()
    m.attach_field("source_amplitude", values)
    return m
