# swp_noise
# Copyright (C) 2022 Mondaic AG
# All rights reserved
"""
Waveform asymmetry misfits.
"""
import typing

import numpy as np

from salvus.project.tools.data_selection import compute_window


def _compute_energy(d: np.ndarray, dt: float):
    return np.sum(np.power(d, 2)) * dt + np.finfo(float).eps


def energy_asymmetry_misfit_and_adjoint_source_no_data(
    data_synthetic: np.ndarray,
    sampling_rate_in_hertz: float,
    time_of_first_sample_in_seconds: float,
    window_taper_width_in_seconds: float,
) -> typing.Tuple[float, np.ndarray]:
    """
    Misfit and adjoint source for as a logarithmic energy ratio of the causal
    and anti-causal parts of the data.

    The time of first sample will be used together with the sampling rate to
    reconstruct the time axes. Anything t<0 will be the anti-causal part,
    anything t>0 will be the causal part.

    If path parts are no equal in length, the longer one will be cut to the
    shorter one's length.

    Args:
        data_synthetic: The synthetic data.
        sampling_rate_in_hertz: The sample rate of the data.
        time_of_first_sample_in_seconds: The time of the first sample in
            seconds.
        window_taper_width_in_seconds: The causal and anti-causal parts will be
            seperated by applying different windows. This parameter determines
            the width of the taper in seconds.
    """
    dt = 1.0 / sampling_rate_in_hertz

    time_array = np.linspace(
        time_of_first_sample_in_seconds,
        time_of_first_sample_in_seconds + (len(data_synthetic) - 1) * dt,
        len(data_synthetic),
    )

    min_extent = np.abs([time_array[0], time_array[-1]]).min()

    win_causal = compute_window(
        t=time_array,
        window_center=min_extent / 2.0,
        window_width=min_extent,
        ramp_width=window_taper_width_in_seconds,
    )
    win_a_causal = compute_window(
        t=time_array,
        window_center=-min_extent / 2.0,
        window_width=min_extent,
        ramp_width=window_taper_width_in_seconds,
    )

    energy_causal = _compute_energy(win_causal * data_synthetic, dt)
    energy_a_causal = _compute_energy(win_a_causal * data_synthetic, dt)

    # Compute the logarithmic energy ratio.
    A = np.log(energy_causal / energy_a_causal)

    misfit = 0.5 * A**2

    # Construct the adjoint source.
    de_caus = 2.0 * win_causal**2 * data_synthetic * dt
    de_a_caus = 2.0 * win_a_causal**2 * data_synthetic * dt

    adj_src = -A * (de_caus / energy_causal - de_a_caus / energy_a_causal)
    return misfit, adj_src


def energy_asymmetry_misfit_and_adjoint_source(
    data_synthetic: np.ndarray,
    data_observed: np.ndarray,
    sampling_rate_in_hertz: float,
    time_of_first_sample_in_seconds: float,
    window_taper_width_in_seconds: float,
) -> typing.Tuple[float, np.ndarray]:
    """
    Misfit and adjoint source for as a logarithmic energy ratio of the causal
    and anti-causal parts of the data.

    The time of first sample will be used together with the sampling rate to
    reconstruct the time axes. Anything t<0 will be the anti-causal part,
    anything t>0 will be the causal part.

    If path parts are no equal in length, the longer one will be cut to the
    shorter one's length.

    Args:
        data_synthetic: The synthetic data.
        data_observed: The observed data.
        sampling_rate_in_hertz: The sample rate of the data.
        time_of_first_sample_in_seconds: The time of the first sample in
            seconds.
        window_taper_width_in_seconds: The causal and anti-causal parts will be
            seperated by applying different windows. This parameter determines
            the width of the taper in seconds.
    """
    dt = 1.0 / sampling_rate_in_hertz

    time_array = np.linspace(
        time_of_first_sample_in_seconds,
        time_of_first_sample_in_seconds + (len(data_synthetic) - 1) * dt,
        len(data_synthetic),
    )

    min_extent = np.abs([time_array[0], time_array[-1]]).min()

    win_causal = compute_window(
        t=time_array,
        window_center=min_extent / 2.0,
        window_width=min_extent,
        ramp_width=window_taper_width_in_seconds,
    )
    win_a_causal = compute_window(
        t=time_array,
        window_center=-min_extent / 2.0,
        window_width=min_extent,
        ramp_width=window_taper_width_in_seconds,
    )

    syn_energy_causal = _compute_energy(win_causal * data_synthetic, dt)
    syn_energy_a_causal = _compute_energy(win_a_causal * data_synthetic, dt)

    obs_energy_causal = _compute_energy(win_causal * data_observed, dt)
    obs_energy_a_causal = _compute_energy(win_a_causal * data_observed, dt)

    # Compute the logarithmic energy ratio.
    synA = np.log(syn_energy_causal / syn_energy_a_causal)
    obsA = np.log(obs_energy_causal / obs_energy_a_causal)

    misfit = 0.5 * (synA - obsA) ** 2

    # Construct the adjoint source.
    de_caus = 2.0 * win_causal**2 * data_synthetic * dt
    de_a_caus = 2.0 * win_a_causal**2 * data_synthetic * dt

    adj_src = (obsA - synA) * (
        de_caus / syn_energy_causal - de_a_caus / syn_energy_a_causal
    )
    return misfit, adj_src
