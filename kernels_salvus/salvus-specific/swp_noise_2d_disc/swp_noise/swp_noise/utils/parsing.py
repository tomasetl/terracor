# swp_noise
# Copyright (C) 2022 Mondaic AG
# All rights reserved
"""
Parsing utilities.
"""
import typing


def parse_station_code(name: str) -> typing.Tuple[str, str, str]:
    """
    Parse a string into Salvus' internal station naming pattern.

    The returned tuple represents the network, station, and location code,
    respectively. The following rules are followed:

    The following rules are followed. If the input string is of the form:
    1. '{0}', the return value is ('XX', {1}, '').
    2. '{0}.{1}' the return value is ({0}, {1}, '').
    3. '{0}.{1}.{2} the return value is ({0}, {1}, {2}).

    If none of these patterns match (i.e. if there are more than two '.' in the
    input string) an exception will be thrown.

    Args:
        name: A candidate name to be decomposed into a station code.

    Returns:
        The name decomposed as per the above rules.
    """

    codes = name.split(".")

    if not len(name) == 0:
        if len(codes) == 1:
            return ("XX", codes[0], "")
        elif len(codes) == 2:
            return (codes[0], codes[1], "")
        elif len(codes) == 3:
            return (codes[0], codes[1], codes[2])

    raise ValueError(
        f"Name '{name}' cannot be parsed into a station code that Salvus"
        " recognizes."
    )
