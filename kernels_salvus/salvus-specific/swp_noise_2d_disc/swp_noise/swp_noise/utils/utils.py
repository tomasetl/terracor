# swp_noise
# Copyright (C) 2022 Mondaic AG
# All rights reserved
"""
Assorted utilities that did not fit in elsewher.
"""
from collections.abc import Iterable
import typing

import numpy as np
from salvus.flow import simple_config
from salvus.project import domain

T = typing.TypeVar("T")
U = typing.TypeVar("U")


def unique_rec_name_generator(
    net: str = "XX", digits: int = 4
) -> typing.Generator[str, None, None]:
    """
    Generate a sequential and unique receiver name.

    Using default values, successive calls via `next` will produce:

        XX.0000
        XX.0001
           .
        XX.9999

    The 'XX' prefix, or "network code", can be modified by passing a different
    string for `net`. The suffix will always be padded with zeros to the number
    of `digits` specified.

    Args:
        net: The network code. Defaults to "XX".
        digits: The number of digits in the each receiver name. Defaults to
            1000.

    Raises:
        ValueError: If additional names beyond `digits` is specified.

    Yields:
        A sequential and unique receiver name.
    """

    for i in range(10 ** (digits)):
        yield f"{net}.{i:0{digits}}"
    raise ValueError("Automatic receiver name generator exhausted!")


def max_vertical_coordinate(domain: domain.Domain) -> float:
    """
    Return the maximum vertical coordinate of a domain.

    Depending on the reference frame and dimension, this will be:

        2-D, Cartesian: The domain's maximum y value.
        3-D, Cartesian: The domain's maximum z value.
        2-D, spherical: The radius of the domain.
        3-D, spherical: The radius of the domain.

    Args:
        Domain: The domain to the the max vertical coordinate of.
    """

    if domain.coordinate_system == "cartesian":
        return domain.bounding_box[:, -1].max()
    return domain._radius_in_meter


def qp_qs_to_qk_qm(
    qp: np.ndarray, qs: np.ndarray, vp: np.ndarray, vs: np.ndarray
) -> typing.Tuple[np.ndarray, np.ndarray]:
    """
    Compute QKAPPA from QP, QS, VP, and VS.

    Args:
        qp: QP.
        qs: QS.
        vp: VP.
        vs: VS.

    Returns:
        QKAPPA and QMU.
    """

    c = 4 / 3 * vs**2 / vp**2
    with np.errstate(divide="ignore"):
        qp_1 = np.nan_to_num(1 / qp)
        qm_1 = np.nan_to_num(1 / qs)
    return 1 / ((qp_1 - c * qm_1) / (1 - c)), qs


def qk_qm_to_qp_qs(
    qk: np.ndarray, qm: np.ndarray, vp: np.ndarray, vs: np.ndarray
) -> typing.Tuple[np.ndarray, np.ndarray]:
    """
    Compute QP from QKAPPA, QMU, VP, and VS.

    Args:
        qk: QKAPPA.
        qs: QS.
        vp: VP.
        vs: VS.

    Returns:
        QP and QS.
    """

    c = 4 / 3 * vs**2 / vp**2
    with np.errstate(divide="ignore"):
        qk_1 = np.nan_to_num(1 / qk)
        qm_1 = np.nan_to_num(1 / qm)
    return 1 / ((1 - c) * qk_1 + c * qm_1), qm


def seconds_until_energy_loss(
    q: float,
    p: float,
    loss: float,
) -> float:
    """
    Estimate the time it takes for wavefield energy to dissipate.

    Given a `q` value and a period `p`, compute the number of seconds required
    for the energy in a wavefield to drop below `loss` percentage of the
    original energy.

    Args:
        q: The Q value (quality factor).
        p: The period at which to calculate time to dissipation.
        loss: The amount of energy that should remain.

    Returns:
        float: The number of seconds it will take for the energy to drop below
            `loss` percent.
    """

    return float(p * np.log(loss) / np.log(1 - 2 * np.pi / q))


def frequency_power_threshold(
    stf: simple_config.stf, threshold: float = 0.05
) -> float:
    """
    Get the highest frequency where the STF power drops below a threshold.

    Args:
        stf: A salvus source time function.
        threshold: The threshold in percent. Defaults to 0.05.

    Returns:
        The highest frequency where p drops below t.
    """
    try:
        f, p = stf.get_power_spectrum()
        return f[np.flatnonzero(p > (p.max() * threshold)).max()]
    except NotImplementedError:
        raise NotImplementedError(
            "Power spectral density analysis not available for stf "
            "wavelet: {stf.wavelet}."
        )


def to_list(v: typing.Union[T, typing.List[T]]) -> typing.List[T]:
    """
    Return v as a list, but if v is a list do nothing.

    Args:
        v: The v to return as a list.

    Returns:
        list(v) or v.
    """
    return v if isinstance(v, list) else [v]


def is_type(v: typing.Any, t: typing.Tuple[type, ...]) -> bool:
    """
    Determine whether v is of type t or is an iterable composed only of ts.

    Args:
        v: The value to check.
        t: The types to pass to `isinstance`.

    Returns:
        Whether v is of type t or v is an iterable composed only of ts.
    """

    if isinstance(v, t):
        return True
    if isinstance(v, Iterable):
        return all(map(lambda x: isinstance(x, t), v))
    return False
