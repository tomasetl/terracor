# swp_noise
# Copyright (C) 2022 Mondaic AG
# All rights reserved
"""
A few type definitions.
"""
from __future__ import annotations

import enum
import typing

from salvus.flow.simple_config import receiver, source

VectorSourceType = typing.Union[
    source.cartesian.VectorPoint2D,
    source.cartesian.VectorPoint3D,
    source.cartesian.SideSetVectorPoint2D,
    source.cartesian.SideSetVectorPoint3D,
    source.seismology.VectorPoint2D,
    source.seismology.VectorPoint3D,
    source.seismology.SideSetVectorPoint2D,
    source.seismology.SideSetVectorPoint3D,
]


ReceiverType = typing.Union[
    receiver.cartesian.Point2D,
    receiver.cartesian.Point3D,
    receiver.cartesian.SideSetPoint2D,
    receiver.cartesian.SideSetPoint3D,
    receiver.seismology.Point2D,
    receiver.seismology.Point3D,
    receiver.seismology.SideSetPoint2D,
    receiver.seismology.SideSetPoint3D,
]


@enum.unique
class Dimension(enum.Enum):
    """The dimension of a coordinate."""

    DIM2 = 2
    DIM3 = 3


@enum.unique
class ReferenceFrame(enum.Enum):
    """Whether a mesh is derived from spherical or Cartesian coordinates."""

    SPHERICAL = "SPHERICAL"
    CARTESIAN = "CARTESIAN"
