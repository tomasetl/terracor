# swp_noise
# Copyright (C) 2022 Mondaic AG
# All rights reserved
"""
Mesh handling utilities.
"""
import pathlib
import typing
from functools import singledispatch

import h5py
import numpy as np
import swp_noise.geometry.coordinate as crd
from salvus.mesh.unstructured_mesh import UnstructuredMesh

from .types import Dimension, ReferenceFrame


def __unknown_mesh_type():
    """Generic failure point for non-standard mesh representations."""
    raise NotImplementedError(  # pragma: no cover
        "'mesh' must be an UnstructuredMesh object or a pathlib.Path object."
    )


@singledispatch
def _reference_frame(mesh) -> None:
    """Generic function to get a mesh's reference frame."""
    __unknown_mesh_type()  # pragma: no cover


@_reference_frame.register  # type: ignore
def _(mesh: UnstructuredMesh) -> ReferenceFrame:
    """Get the mesh's reference frame if it is an in-memory object."""
    return ReferenceFrame(mesh.global_strings["reference_frame"].upper())


@_reference_frame.register  # type: ignore
def _(mesh: pathlib.Path) -> ReferenceFrame:
    """Get the mesh's reference frame if it is saved on disk."""
    with h5py.File(mesh) as fh:
        return ReferenceFrame(
            fh["MODEL"].attrs["reference_frame"].decode().upper()
        )


@singledispatch
def _dimension(mesh) -> None:
    """Generic function to get a mesh's dimension."""
    __unknown_mesh_type()  # pragma: no cover


@_dimension.register  # type: ignore
def _(mesh: UnstructuredMesh) -> Dimension:
    """Get the mesh's dimension if it is an in-memory object."""
    return Dimension(mesh.ndim)


@_dimension.register  # type: ignore
def _(mesh: pathlib.Path) -> Dimension:
    """Get the mesh's dimension if it is saved on disk."""
    with h5py.File(mesh) as fh:
        return Dimension(fh["MODEL"]["coordinates"].shape[-1])


@singledispatch
def _max_z(mesh):
    """Generic function to get the max vertical coordinate of a mesh."""
    __unknown_mesh_type()  # pragma: no cover


@_max_z.register  # type: ignore
def _(mesh: UnstructuredMesh) -> float:
    """Get the mesh's max vertical coordinate if is an in-memory object."""
    if _reference_frame(mesh) == ReferenceFrame.CARTESIAN:
        return mesh.points[:, -1].max()
    return mesh.global_variables["radius"]


@_max_z.register  # type: ignore
def _(mesh: pathlib.Path) -> float:
    """Get the mesh's max vertical coordinate if is saved on disk."""
    rf = _reference_frame(mesh)
    with h5py.File(mesh) as fh:
        if rf == ReferenceFrame.CARTESIAN:
            return fh["MODEL"]["coordinates"][:, -1].max()
        return fh["MODEL"].attrs["radius"]


@singledispatch
def _vertices(mesh, side_set: typing.Optional[str] = None):
    """Generic function to get the points from a mesh."""
    __unknown_mesh_type()  # pragma: no cover


@_vertices.register  # type: ignore
def _(
    mesh: UnstructuredMesh, side_set: typing.Optional[str] = None
) -> np.ndarray:

    # XXX: Need to find a way for side sets and ABC.
    if side_set:
        return mesh.points[mesh.get_side_set_nodes(side_set)]

    # Do not generate points in the sponge layers.
    if "ABC_elements" in mesh.elemental_fields:
        mesh = mesh.apply_element_mask(
            mask=mesh.elemental_fields["ABC_elements"] == 0.0
        )

        # XXX: This needs to be better!
        # A bit nasty but remove one more layer.
        mesh.find_surface()
        new_mask = np.ones(mesh.nelem, dtype=np.bool)
        new_mask[mesh.side_sets["surface"][0]] = False
        mesh = mesh.apply_element_mask(new_mask)

    return (
        mesh.points
        if side_set is None
        else mesh.points[mesh.get_side_set_nodes(side_set)]
    )


@_vertices.register  # type: ignore
def _(mesh: pathlib.Path, side_set: typing.Optional[str] = None) -> np.ndarray:
    return _vertices(UnstructuredMesh.from_h5(mesh), side_set)


def vertices_to_coordinates(
    mesh: typing.Union[UnstructuredMesh, pathlib.Path],
    side_set: typing.Optional[str] = None,
) -> typing.List[crd.CoordinateType]:

    p = _vertices(mesh, side_set)
    dim = _dimension(mesh)
    ref = _reference_frame(mesh)

    if dim == Dimension.DIM2:
        if ref == ReferenceFrame.CARTESIAN:
            return [crd.CartesianCoordinatePair(x=_p[0], y=_p[1]) for _p in p]
        else:
            raise NotImplementedError

    else:
        if ref == ReferenceFrame.CARTESIAN:
            return [
                crd.CartesianCoordinateTriple(x=_p[0], y=_p[1], z=_p[2])
                for _p in p
            ]
        else:
            raise NotImplementedError
