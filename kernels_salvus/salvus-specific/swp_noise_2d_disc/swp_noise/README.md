# SWP Noise

A toolbox for full-waveform modeling and inversion of ambient noise correlation
wavefields.

[![pipeline status](https://gitlab.com/Mondaic/Projects/ifg-contract/swp_noise/badges/main/pipeline.svg)](https://gitlab.com/Mondaic/Projects/ifg-contract/swp_noise/-/commits/main)
[![coverage report](https://gitlab.com/Mondaic/Projects/ifg-contract/swp_noise/badges/main/coverage.svg)](https://gitlab.com/Mondaic/Projects/ifg-contract/swp_noise/-/commits/main)