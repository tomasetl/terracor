# ---
# jupyter:
#   jupytext:
#     formats: ipynb,py:light
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.5'
#       jupytext_version: 1.13.8
#   kernelspec:
#     display_name: Python 3 (ipykernel)
#     language: python
#     name: python3
# ---

# <div style='background-image: url("../header.png") ; padding: 0px ; background-size: cover ; border-radius: 5px ; height: 350px'>
# </div>

# # 2-D Disc

# This notebook demonstrates how to run noise simulations + gradients in a 2-D circular domain that mimics Earth.

# +
import numpy as np

import salvus.namespace as sn
import swp_noise
# -

# Instead of basing things on frequency we'll now work in terms of periods. To keep computational costs in check we'll operate on fairly long periods.

period = 300.0

# One difference, in particular to the Cartesian domains we have seen so far as that we are now working with the full domain. This means the simulations have to reach back much further in time to compute the appropriate correlations. `swp_noise` comes with a simple utility to compute the seconds until energy loss given a certain period and Q factor. This is only a rough proxy but should give a good first hint.

seconds_until_energy_loss = swp_noise.utils.utils.seconds_until_energy_loss(
    q=100, p=period, loss=0.0001
)
print(seconds_until_energy_loss)

# We will first generate the domain, as we'll utilize this for a few more things later on.

domain = sn.domain.dim2.CircularDomain(radius_in_meter=6371e3)

# +
# We just use this to generate a simulation template - no need to
# persist the project.
# !rm -rf temp_salvus_project
p = sn.Project.from_domain(
    path="temp_salvus_project", domain=domain, load_if_exists=True
)

wc = sn.WaveformSimulationConfiguration(attenuation=True)

p += sn.SimulationConfiguration(
    name="my_first_correlation_on_disc",
    model_configuration=sn.ModelConfiguration(
        background_model="prem_iso_no_crust",
    ),
    elements_per_wavelength=1.5,
    tensor_order=4,
    # We'll use a Ricker wavelet let - so we'll mesh for a smaller period.
    min_period_in_seconds=period / 1.5,
    event_configuration=sn.EventConfiguration(
        # The stf is overwritten by swp_noise later on
        # so it is inconsequential.
        sn.simple_config.stf.Delta(),
        wc,
    ),
)

# Now we break out of SalvusProject and just use the simulation template.
simulation_template = p.simulations.get_simulation_template(
    "my_first_correlation_on_disc"
)
# -

# Let's have a look at the mesh.

simulation_template

# ## Define the correlation points

# In a spherical system the coordinates have to be defined as a spherical coordinate pair.

# Define a few correlation points.
c = swp_noise.coordinate.SphericalCoordinatePair
p0 = swp_noise.CorrelationPoint(
    point=c(longitude=20.0, z=swp_noise.coordinate.Depth(0.0)), name="A"
)
p1 = swp_noise.CorrelationPoint(
    point=c(longitude=70.0, z=swp_noise.coordinate.Depth(0.0)), name="B"
)

swp_noise.visualization.plot_domain(domain=domain, entity=[p0, p1])

correlation = swp_noise.Correlation(
    virtual_source=p0,
    virtual_source_mechanism=swp_noise.source_mechanism.seismology.Vector2D(
        fr=1e20, fp=0.0
    ),
    virtual_receivers=[p1],
    virtual_receiver_fields=["displacement"],
)

swp_noise.visualization.plot_domain(domain=domain, entity=correlation)

# ## Circular noise source model

# +
# The noise source model is defined as a list of longitudes and values.
source_amplitude_longitudes=np.linspace(0.0, 360.0, 250)
source_amplitude_values = np.ones_like(source_amplitude_longitudes)

# Example how to define something more complicated.
# source_amplitude_values = (np.sin(np.deg2rad(source_amplitude_longitudes) * 8) + 1.0) / 2.0

# The other parameters are the same as always.
noise_source_model = (
    swp_noise.noise_source_models.circular.HeterogeneousCircular2D(
        recording_field="displacement",
        emanating_components=["Z"],
        side_set="r1",
        source_amplitude_longitudes=source_amplitude_longitudes,
        source_amplitude_values=source_amplitude_values
    )
)

# It can also be plotted.
noise_source_model.plot()
# -

# ## Launching simulations
#
# This is not any different to what you have previously seen.

# Some meta-parameters are required to run simulations - in SWPNoise
# these are collected in a simulation launch parameters object.
# The meaning of all parameters is the same as in Salvus - so
# please refer to it for a full documentation.
simulation_launch_parameters = swp_noise.SimulationLaunchParameters(
    site_name="local_12_8", ranks_per_job=4, memory_per_rank_in_MB=1000.0
)

corr = swp_noise.simulations.run_correlation_simulation(
    simulation_launch_parameters=simulation_launch_parameters,
    ###
    simulation_template=simulation_template,
    correlation=correlation,
    ##
    stf=sn.simple_config.stf.Ricker(center_frequency=1.0 / period),
    # THIS IS QUITE EXPENSIVE SO BE CAREFUL.
    simulation_history_in_s=seconds_until_energy_loss,
    lag_time_in_s=7200.0,
    #simulation_history_in_s=1000.0,
    #lag_time_in_s=500.0,
    ###
    noise_source_model=noise_source_model,
    ###
    wavefield_compression=sn.WavefieldCompression(
        forward_wavefield_sampling_interval=10
    ),
    ###
    output_folder="correlation",
    overwrite=True
)

corr.plot_correlation(index=0, receiver_field="displacement")

# ## Misfit definition
#
# We want to limit the misfit to a one hour window on both the causal and acausal branches.

# +
from salvus.project.tools.data_selection import compute_window


# Temporal weights in Salvus work by defining a function that will be executed for
# each receiver/virtual receiver.
# Weights can then be given per station, channel, and time sample.
# The syntax is, at first, slightly involved but it can express common scenarios
# like data windowing and receiver weighting.
def temporal_weights(st, receiver, sources):
    weights = {}
    weights["Z"] = [
        {
            "values": compute_window(
                t=st[0].times() + st[0].stats.starttime.timestamp,
                window_center=0.0,
                window_width=6000.0,
                ramp_width=1200.0,
            )
        }
    ]
    return weights


# -

corr.plot_correlation(
    index=0,
    receiver_field="displacement",
    temporal_weights_function=temporal_weights,
)

correlation_misfit = swp_noise.CorrelationMisfit(
    # The synthetic data.
    synthetic_correlation=corr,
    # What to compare against, e.g. nothing here.
    observed_correlation=None,
    # The misfit functional - can either be a name mapping
    # to one of Salvus' built-in misfit functional or,
    # as we'll see in another tutorial, a custom Python function.
    misfit_function="L2_energy_no_observed_data",
    # Potential extra arguments to the misfit function.
    extra_kwargs_misfit_function=None,
    # The receiver field to use.
    receiver_field="displacement",
    # The temporal weights function.
    temporal_weights_function=temporal_weights
)

correlation_misfit.misfit_value

# ## Source gradient

src_gradient = swp_noise.simulations.run_source_gradient_simulation(
    # The same parameters as for the forward correlation simulation.
    simulation_launch_parameters=simulation_launch_parameters,
    # The correlation misfit - which also knows where the other
    # simulations ran.
    correlation_misfit=correlation_misfit,
    # Which components to compute the source gradient for.
    # Not relevant for scalar wave equation.
    source_gradient_components="Z",
    # Where to store the output and overwrite or not.
    output_folder="source_gradient",
    overwrite=True
)

# Let's visualize the source gradient.
src_gradient.plot_source_gradient(component="Z")

# This representation is not easy to understand - let's try a different one.
#
# All the values are negative and so there is less absolute sensitivity between the two stations which makes sense.

# +
import matplotlib.pyplot as plt

plt.figure(figsize=(8, 8))
plt.scatter(
    x=src_gradient.coordinates[:, 0],
    y=src_gradient.coordinates[:, 1],
    c=src_gradient.data[:, 0],
)
plt.show()
# -

str_gradient = swp_noise.simulations.run_structure_gradient_simulation(
    # The same parameters as for the source gradient.
    simulation_launch_parameters=simulation_launch_parameters,
    correlation_misfit=correlation_misfit,
    # Also pass the source gradient.
    source_gradient=src_gradient,
    # The structural parameters you want the gradient of.
    gradient_parameterization="rho-vp",
    # This again needs access to the noise source model.
    # Should always be the same as for the forward correlation
    # simulation.
    noise_source_model=noise_source_model,
    # Where to store the output and overwrite or not.
    output_folder="structure_gradient",
    overwrite=True
)

str_gradient.get()

# ## Cleaning Up
#
# `swp_noise` runs quite a number of Salvus simulations for these results and they produce a number of files that are no longer needed once everything has been computed. So remember to clean up after yourself.

str_gradient.delete_remote_files()
src_gradient.delete_remote_files()
corr.delete_remote_files()
