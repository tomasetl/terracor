#!/bin/bash

#OAR -n 2_correlate
#OAR -l /core=1, walltime=48:00:00
#OAR --stdout corr.out
#OAR --stderr corr.err
#OAR --project global

cd /summer/nocoda/tomasetl/python/

source ~/.bashrc

conda activate mypy3

python3 create_file_summary.py ./data_4.0hz/events/ pairs_P.txt P
python3 create_file_summary.py ./data_4.0hz/events/ pairs_PKP.txt PKP
python3 create_file_summary.py ./data_4.0hz/events/ pairs_PcP.txt PcP
    
python3 04_xcorr_P.py
python3 04_xcorr_PKP.py
python3 04_xcorr_PcP.py
