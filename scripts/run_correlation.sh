#!/bin/bash

path=/summer/nocoda/tomasetl/python/test/

oarsub -S ${path}1_config.sh

sleep 1800

download_data
for i in {1..2}; do oarsub -S /summer/nocoda/tomasetl/python/2020/feb/test/dwd_data.sh; done

sleep 1800

while find ${path}data_4.0hz/events/ -name "*.lock";do sleep 3600; done

oarsub -S ${path}2_correlate.sh

sleep 3600

while find ${path}C1_01_xcorr_P__coher/xcorr__000/ -name "*.h5";do sleep 1800; done

oarsub -S ${path}3_plot.sh
