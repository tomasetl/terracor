#!/bin/bash

#OAR -n config
#OAR -l /core=1, walltime=04:00:00
#OAR --stdout config.out
#OAR --stderr config.err
#OAR --project global

cd /summer/nocoda/tomasetl/python/
source ~/.bashrc

conda activate mypy3

python3 01_get_stations.py

python3 extract_pairs-P.py
python3 extract_pairs-PKP.py
python3 extract_pairs-PcP.py
python3 unique_station_list.py

python3 02_config_download.py
