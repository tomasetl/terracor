#!/bin/bash

#OAR -n dwd_
#OAR -l /core=1, walltime=08:00:00
#OAR --stdout dwd.out
#OAR --stderr dwd.err
#OAR --project global


source /applis/site/nix.sh
source /applis/environments/conda.sh

cd /summer/nocoda/tomasetl/python/

conda activate mypy3

python3 03_download_data.py
