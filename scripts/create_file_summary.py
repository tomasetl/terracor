#!/usr/bin/python

__author__ = "Lisa Tomasetto"
__copyright__ = "Copyright 2022, UGA"
__credits__ = ["Lisa Tomasetto"]
__version__ = "1.0"
__maintainer__ = "Lisa Tomasetto"
__email__ = "lisa.tomasetto@univ-grenoble-alpes.fr"
__status__ = "Production"

""" This script reads the .h5 datafiles in a given directory and writes a .txt file with station ID, number of the file it is in and path of the file """

#######################################################################
##Libraries
import pandas as pd
import numpy as np
import h5py as h5
from sys import exit, argv 
import glob

#######################################################################
try:
    path = argv[1]  #path of .h5 files containing data
except:
    print('Give me a path please, such as ./data_4.0hz/events/')
    exit()
try:
    corr_path = argv[2]
except:
    print('Give me a path to the list of correlations file please')
    exit()
try:
    phase = argv[3]
except:
    print('Give me a phase, P, PKP, PcP')
    exit()
## Write file with effective downloaded stations
list_of_h5 = glob.glob(path+'*.h5')
print("Found %d h5 files in directory !"%len(list_of_h5))
print("-----------------  ------------------------  -----------------------")

# Open json file with station pairs
df_pairs = pd.read_csv(corr_path, sep='\t', names = ["station_A", "station_B", "lat_A", "lat_B", "lon_A", "lon_B", "elev_A", "elev_B", "depth_A", "depth_B", "distance"])
print(df_pairs["station_B"])
N = len(df_pairs.station_A)
filenameA = pd.Series(data = np.empty(N, dtype=str))
filenameB = pd.Series(data = np.empty(N, dtype=str))
print(N)

df_pairs["filename_A"] = filenameA
df_pairs["filename_B"] = filenameB
df = df_pairs.copy()

for filename in list_of_h5:
    print(filename)
    f = h5.File(filename,'r')
    try:
        for net in f['_metadata']['sta'].keys():
            for sta in f['_metadata']['sta'][net].keys():
                id_sta = net + '.' + sta
                print(id_sta)
                try:
                    mask = df["station_A"] == id_sta
                    df.loc[mask, "filename_A"] = filename
                except:
                    print("not in station A")
                try:
                    mask = df["station_B"] == id_sta
                    df.loc[mask, "filename_B"] = filename
                except:
                    print("not in station B")
    except KeyError:
        continue
    except:
        raise
    
nan_value = np.nan
df.replace("", nan_value, inplace=True)

df = df.dropna(axis=0, how = 'any')
df_pairs = df[["station_A", "station_B"]]

# Save to json or txt file
df.to_csv("pairs_%s.txt"%phase, header = False, sep = "\t", index = False)
df_pairs.to_csv("list_pairs_%s.txt"%phase, header=False, sep="_", index = False)
