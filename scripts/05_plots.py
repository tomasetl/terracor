#!/usr/bin/env python3
""" Created on Thu Apr 1 2021 
@author: Tomasetto Lisa
"""
import sys
sys.path.append('/bettik/tomasetl/pycorr/v1.0/')
##Libraries
import m_pycorr.live.c1_md as c1_md
import numpy as np
import matplotlib
import matplotlib.pyplot as plt
import matplotlib.pylab as pl
import h5py
from scipy import signal
from scipy.interpolate import interp1d
import pandas as pd
from obspy import Trace
from obspy.taup import plot_travel_times, TauPyModel
from obspy.taup.taup_geo import calc_dist
import datetime, time
from datetime import datetime
from sys import exit, argv
import cartopy.crs as ccrs
from cartopy.mpl.gridliner import LONGITUDE_FORMATTER, LATITUDE_FORMATTER
from pyproj import Geod



##Fontsize
SMALL_SIZE = 14
MEDIUM_SIZE = 16
BIGGER_SIZE = 22

plt.rc('font', size=SMALL_SIZE)          # controls default text sizes
plt.rc('axes', titlesize=SMALL_SIZE)     # fontsize of the axes title
plt.rc('axes', labelsize=MEDIUM_SIZE)    # fontsize of the x and y labels
plt.rc('xtick', labelsize=SMALL_SIZE)    # fontsize of the tick labels
plt.rc('ytick', labelsize=SMALL_SIZE)    # fontsize of the tick labels
plt.rc('legend', fontsize=SMALL_SIZE)    # legend fontsize
plt.rc('figure', titlesize=BIGGER_SIZE)  # fontsize of the figure title
np.set_printoptions(threshold=sys.maxsize)
matplotlib.rcParams['pdf.fonttype'] = 42
#########################################################################################
##Entry


try:
    list_phases = [argv[1]]
    print(list_phases)
except:
    print('Enter phase : P PcP or PKP')
    exit()
try:
    path_corr = argv[2]
    print(path_corr)
except:
    print('Enter path to correlations directory such as C1_04_xcorr_lisa__coher/')
    exit()

geoid = Geod(ellps="WGS84")

f = open("./events.txt", "r")
line = f.readline()
date_ev, lat_ev, lon_ev, f, A, unit = line.split("   ")
lat_ev = float(lat_ev)
lon_ev = float(lon_ev)
f = float(f)
A = float(A)
date_ev = date_ev[0:10]
year, month, day = date_ev.split("-")
##Get Correlations
h1 = c1_md.c1_md(path_corr)
date = datetime.fromtimestamp(int(h1.md_c['date1'][0]))

dset = h1.get_selection(idvs=['*'], idvr=['*'],icmp=0,dmin=None, dmax=None,filter_p=True,p1=3.,p2=10.,ctype='P',norm_tr=True,bin_distance=True,bin_width=0.1,dist_unit='deg',time_unit='s', save=False)
print(dset['id'], len(dset['id']))

N = len(dset['lat'])
distance = np.empty(N)
latA = dset['lat'].T[0]
latB = dset['lat'].T[1]
lonA = dset['lon'].T[0]
lonB = dset['lon'].T[1]
for i in range(N):
    distance[i] = calc_dist(latA[i], lonA[i], latB[i], lonB[i], 6371, 0)

# IF MATRIX SIZE PROBLEM
"""for k in dset.keys():
    try:
        print(len(dset['%s'%k]))
    except:
        print(dset['%s'%k])"""
    
#Plot stations
#h1.plot_station_map()

#Initialize variables
data =dset['data'].T # Transpose data matrix
#print(data.shape)
dt = dset['time'][1]  # time step
#print(dset['time'])


##Plot

#P-waves theoretical arrival
model = TauPyModel(model="prem")
p_arrival = []
absc=[]
dist=np.linspace(0, 210, 1000)
for d in dist:
    arrivals = model.get_travel_times(source_depth_in_km=0, distance_in_degree=d, phase_list=list_phases)
    try:
        a = arrivals[0]
    except:
        continue
    absc.append(d)
    p_arrival.append(a.time)
p_arrival = np.array(p_arrival)  # P-wave arrival time 
absc = np.array(absc) # distance of arrivals

f = interp1d(absc, p_arrival, kind='cubic')

#Plot Raw Traces
if int(np.sum(dset['bn'])) < 50:
    fig, ax = plt.subplots(1, 1, figsize=(10, 10))
    ax.plot(p_arrival, absc, 'r--', lw=1, label='Theoretical %s-waves '%list_phases[0])
    ax.plot(dset['time'],dset['data']+dset['dist'], color='k')
    ax.set(xlabel='Time (s)s')
    ax.set(ylabel='Distance (°)')
    ax.set_xlim(700, 780)
    ax.set_ylim(80, 88)
    fig.suptitle('Raw Traces for event')
    plt.legend()
    plt.show()

#Plot effective paths
col = pl.cm.inferno(np.linspace(0,1,180))
if list_phases == ['PcP']:
    (lon_ev_antipod, lat_ev_antipod, baz_ev_antipod) = geoid.fwd(lon_ev, lat_ev, 0, 180*1000*111.91)
    ax = plt.subplot(111, projection=ccrs.LambertAzimuthalEqualArea(central_longitude=lon_ev_antipod, central_latitude=lat_ev_antipod))
else:
    ax = plt.subplot(111, projection=ccrs.LambertAzimuthalEqualArea(central_longitude=lon_ev, central_latitude=lat_ev))
ax.stock_img()
ax.coastlines()
ax.scatter(lonA, latA, marker='o', c='red', s=20, transform=ccrs.PlateCarree(), cmap='plasma_r', alpha=0.5, label='station A')
ax.scatter(lonB, latB, marker='o', c='darkviolet', s=20, transform=ccrs.PlateCarree(), cmap='plasma_r', alpha=0.5, label='station B')
for i in range(N):
    plt.plot([lonA[i], lonB[i]], [latA[i], latB[i]], linewidth=0.5, color=col[int(distance[i])], alpha=0.25, transform=ccrs.Geodetic())
plt.scatter(lon_ev, lat_ev, marker='*', c='g', s=500, transform=ccrs.PlateCarree(), cmap='plasma_r', alpha=0.5, label='source centroid')
#plt.legend()
plt.title('Effective paths')
#plt.show()
plt.savefig('effective_path%s.png'%list_phases[0])
plt.close()

## Relative time
index = np.empty(len(dset['dist']))
M = np.empty((4*60*4, len(dset['dist'])))
for i, d in enumerate(dset['dist']):
    ind_dist = np.argmin(abs(absc-d))
    tt = p_arrival[ind_dist]
    ind_time = np.argmin(abs(dset['time']-tt))
    if ind_time < 2*4*60:
        #print(ind_time)
        #print(len(dset['data'][0:(2*4*60+ind_time), i]))
        Z =np.zeros(2*4*60-ind_time)
        D = np.array(dset['data'][0:(2*4*60+ind_time), i])
        M[:, i] = np.concatenate((Z, D))
    elif ind_time > (len(dset['data'])-2*4*60):
        D = np.array(dset['data'][(ind_time-2*4*60):len(dset['data']), i])
        Z = np.zeros(len(dset['data'])-ind_time)
        #print(ind_time)
        M[:, i] = np.concatenate((D, Z))
    else:
        M[:, i] = dset['data'][(ind_time-2*4*60):(ind_time+2*4*60), i]
#print(M.shape)
t = np.arange(-2*60, 2*60, 0.25)

#--------------------------Plot Pcolor--------------------------------------------
fig, ax = plt.subplots(2,2, figsize=(15, 10), sharex= True, gridspec_kw={'height_ratios':[4, 1]})
ax1 = ax[0, 0]
ax2 = ax[1,0]
ax3 = ax[0, 1]
ax4 = ax[1,1]

# ___ Section 1
x, y = np.meshgrid(dset['dist'], dset['time'])
im = ax1.pcolormesh(x, y, dset['data'], cmap='bone', shading='auto', vmin=-1, vmax = 1)
ax1.scatter(absc, p_arrival, s=0.5, c='red', alpha=0.3, label='Theoretical %s-wave'%list_phases[0])
if list_phases == ['P']:
    ax1.set_xlim(0, 90)
    ax1.set_ylim(0, 14*60)
#if list_phases == ['PcP']:
#    ax1.set_xlim(0, 80)
#    ax1.set_ylim((7*60, 11*60))
#if list_phases == ['PKP']:
#    ax1.set_xlim(100, 150)
#    ax1.set_ylim(1000, 1300)
ax1.set(xlabel='Distance (°)')
ax1.set(ylabel='Time (s)')

ax2.plot(dset['dist'],dset['bn'])
ax2.set(xlabel='Distance (°)')
ax2.set(ylabel='Number of traces\n Total: %d'%(np.sum(dset['bn'])))
ax1.legend()

# ___ Section 2
x, y = np.meshgrid(dset['dist'], t)
ax3.pcolormesh(x, y, M, cmap='bone', shading='auto', vmin = -1, vmax = 1)
if list_phases == ['P']:
    ax3.set_xlim(0, 90)
if list_phases == ['PcP']:
    ax3.set_xlim(40, 70)
#if list_phases == ['PKP']:
#ax3.set_xlim(0, 150)

ax3.set_ylim(-2*60, 2*60)
ax3.set(xlabel='Distance (°)')
ax3.set(ylabel='Relative Time (s)')

ax4.plot(dset['dist'],dset['bn'])
ax4.set(xlabel='Distance (°)')
ax4.set(ylabel='Number of traces\n Total: %d'%(np.sum(dset['bn'])))
plt.tight_layout()
plt.subplots_adjust(bottom=0.1, right=0.8, top=0.9)
cax = plt.axes([0.85, 0.1, 0.025, 0.8])
plt.colorbar(im, cax=cax, shrink=0.6)
plt.suptitle('%s'%date)
#plt.show()
plt.savefig('subsection%s.png'%list_phases[0], dpi=200)


## Vespagram

tau = 0.25
perturbation = np.arange(-1, 1, 0.01)
if list_phases == ["PKIKP"]:
    dist_ref = 145
else:
    dist_ref = 60
t_ref = f(dist_ref)

for index, u in enumerate(perturbation):
    #Slant Stack
    dist = dset["dist"]
    bdata = np.zeros(data.T.shape)
    range_dist = max(distance) - min(distance)
    t_arr = f(dist) + u*dist
    #print(t_arr)
    delay = t_arr - t_ref
    #print(delay)
    max_delay = max(abs(delay))
    delta = np.zeros((int(max_delay/tau*2) +1, data.T.shape[1]))
    for idx, tr in enumerate(data):
        try:
            #print(int(np.round(delta.shape[0]/2)))
            #print(int(np.round(delay[idx]/tau)))
            #print(int(np.round(delta.shape[0]/2))  - int(np.round(delay[idx]/tau)))
            delta[int(np.round(delta.shape[0]/2))  - int((np.round(delay[idx]/tau))) ,idx] = 1.
        except:
            if (int(np.round(delta.shape[0]/2))  - int(np.round(delay[idx]/tau)))  == delta.shape[0]:
                delta[-1, idx] = 1
            else:
                raise
        bdata[:, idx]= signal.fftconvolve(tr, delta[:, idx], mode='same')
    norm_value = float(len(np.where(np.max(bdata, axis=0))[0]))
    if norm_value == 0: norm_value = 1.
    slant_stack = np.sum(bdata, axis=1)/norm_value
    vespa[:, index] = slant_stack

plt.figure()
plt.contour(dset["time"], perturbation, vespa.T, cmap='plasma')
plt.xlabel('Time (s)')
plt.ylabel('Perturbation speed (s/deg)')
plt.title('Vespagram %s'%date)
#plt.show()
plt.savefig('vespagram_%s.pdf'%list_phases[0])
