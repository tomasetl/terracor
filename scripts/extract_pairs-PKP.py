#!/usr/bin/env python
# coding: utf-8

# # Check dataset

###############################################################################

### Libraries
# Anaconda Packages

import sys,os,glob,time,copy,warnings,cProfile,shutil,matplotlib
from IPython import display
import h5py as h5
import numpy as np
import scipy.fftpack
import scipy.signal
import scipy.io as io
from scipy import interpolate
import skimage.restoration as deconv
from math import cos, sin, radians
import matplotlib
import matplotlib.pyplot as plt
import matplotlib.pylab as pl
from matplotlib.colors import LinearSegmentedColormap
from matplotlib.transforms import offset_copy
import cartopy.crs as ccrs
from cartopy.io.img_tiles import Stamen, OSM
import cartopy.feature as cfeature
from obspy.geodetics.base import gps2dist_azimuth
from obspy.geodetics import locations2degrees
from obspy.taup import TauPyModel
from mpl_toolkits.axes_grid1 import make_axes_locatable, axes_size
matplotlib.rcParams['pdf.fonttype'] = 42
from pyproj import Geod
import ipdb
import json
import pandas as pd

sys.path.append("/bettik/tomasetl/pycorr/v1.0/")
# Own Libraries

import m_pycorr.mods.dd as dd
import m_pycorr.mods.lang as lang
import m_manage_data as m_mod


## New Version
stations = "stations.txt"
events = "events.txt"

datacenter = []
net = []
sta = []
locid = []
lat = []
lon = []
alt = []
depth = []
id_sta = []

f = open(stations, 'r')
lines = f.readlines()

for l in lines:
    liste = l.split('    ')
    datacenter.append(liste[0])
    net.append(liste[1])
    sta.append(liste[2])
    locid.append(liste[3])
    lat.append(float(liste[4]))
    lon.append(float(liste[5]))
    alt.append(float(liste[6]))
    depth.append(float(liste[7]))
    if (liste[3] == '--'):
        id_sta.append('%s/%s/00'%(liste[1], liste[2]))
    else:
         id_sta.append('%s/%s/%s'%(liste[1], liste[2], liste[3]))
f.close()

ev = open(events)
for lev in ev.readlines():
    lat_ev = float(lev.split('   ')[1])
    lon_ev = float(lev.split('   ')[2])

###Plot all stations and event coordinates

m_mod.mmap(111,lon,lat,ch=None,xylabel=True,ev=[lon_ev, lat_ev],res=2)
plt.scatter(lon_ev, lat_ev, marker='*', c='g', s=500,transform=ccrs.PlateCarree(),cmap='plasma_r',alpha=0.5, label='event')
plt.title('all stations map')
plt.legend()
#plt.show()
plt.savefig('stations_all_PKP.pdf')
plt.close('all')

### Plot equidistant point source-station aligned on the same azimuth
vec_dist  = np.zeros([len(id_sta),1])
vec_az    = np.zeros([len(id_sta),1])
vec_baz   = np.zeros([len(id_sta),1])

for counterA, staA in enumerate(id_sta):
    (vec_dist[counterA],vec_az[counterA],vec_baz[counterA]) = gps2dist_azimuth(lat_ev, lon_ev, lat[counterA], lon[counterA])
    # distance, azimuth and back-azimuth of each station to the event coordinates
geoid = Geod(ellps="WGS84")    
term_lon,term_lat,term_baz = geoid.fwd(lon, lat, vec_baz+180.,vec_dist)  # coordinates of point with same source station azimuth and distance from station coordinates

m_mod.mmap(111,lon,lat,ch=None,xylabel=True,res=2,proj='azi',ev=[lon_ev, lat_ev]) # plot stations
plt.scatter(term_lon,term_lat, marker='o', c='gold', s=20,transform=ccrs.PlateCarree(),cmap='plasma_r',alpha=0.5,label='projected equidistant point') # plot projected points
plt.scatter(lon_ev, lat_ev, marker='*', c='g', s=500,transform=ccrs.PlateCarree(),cmap='plasma_r',alpha=0.5, label='event')  # event coordinates
plt.legend()
plt.title('Stations and equidistant aligned with source points')
#plt.show()
plt.savefig('stations_proj_PKP.pdf')
plt.close('all')

### Plot effective station pairs selection

mat_dist   = np.zeros([len(id_sta),len(id_sta)])
bool_dist  = np.zeros([len(id_sta),len(id_sta)])

for counterA, staA in enumerate(id_sta):
    az,baz,mat_dist[:,counterA] = geoid.inv(np.tile(term_lon[counterA],np.array(lon).size), np.tile(term_lat[counterA],np.array(lon).size), lon , lat)
    # azimuth, back azimuth and distance beetween actual stations and projected point
    D_in_deg = vec_dist[counterA]*180/(np.pi*6.371*1e6)
    if D_in_deg < 120:
            continue
    rayon = 5
    bool_dist[:, counterA] = mat_dist[:, counterA] < rayon*111.91*1000
    # if distance < 5% distance from source, keep station pair
valid_idxA = np.where(np.sum(bool_dist,0)>0)[0]
valid_idxB = np.where(np.sum(bool_dist,1)>0)[0]

# List stations
valid_idx = np.unique(np.concatenate([valid_idxA, valid_idxB]))
stations = []
for idx in valid_idx :
    stations.append([datacenter[idx], net[idx], sta[idx], locid[idx], lat[idx], lon[idx], alt[idx] ,depth[idx]])

df_sta = pd.DataFrame(stations)
df_sta.to_csv("stations_list_PKP.txt", header = False, sep = "\t", index = False)

# List pairs
rows = []
for counterA in valid_idxA:
    id_A = id_sta[counterA].replace('/', '.')
    for counterB in np.where(bool_dist[:,counterA])[0]:
        id_B = id_sta[counterB].replace('/','.')
        dist = geoid.line_length([lon[counterA],lon[counterB]], [lat[counterA], lat[counterB]])/1000/111.91  # distance inter stations
        rows.append([id_A, id_B,lat[counterA], lat[counterB], lon[counterA], lon[counterB], alt[counterA], alt[counterB], depth[counterA], depth[counterB], dist]) 

df = pd.DataFrame(rows, columns=["station_A", "station_B", "lat_A", "lat_B", "lon_A", "lon_B", "elev_A", "elev_B", "depth_A", "depth_B", "distance"])
df_pairs = df[["station_A", "station_B"]]

df.to_csv("pairs_PKP.txt", header = False, sep = '\t', index = False)
df_pairs.to_csv("list_pairs_PKP.txt", header=False, sep="_", index=False)

## Figures
n = 180
col = pl.cm.inferno(np.linspace(0,1,n))

# Final Selection
m_mod.mmap(111,np.array(lon)[valid_idxA],np.array(lat)[valid_idxA],ch=None,ev=[lon_ev, lat_ev],proj='azi',xylabel=True,res=2)  # plot only stations involved in a pair
for i in range(len(df.station_A)):
    plt.plot([df["lon_A"][i],df["lon_B"][i]], [df["lat_A"][i], df["lat_B"][i]], color=col[int(df.distance[i])], linewidth=0.5,alpha=0.25, transform=ccrs.Geodetic()) # plot path along great circle, color depending on distance
plt.scatter(np.array(lon)[valid_idxB],np.array(lat)[valid_idxB], marker='o', c='darkviolet', s=20,transform=ccrs.PlateCarree(),cmap='plasma_r',alpha=0.5,)
plt.scatter(lon_ev, lat_ev, marker='*', c='g', s=500,transform=ccrs.PlateCarree(),cmap='plasma_r',alpha=0.5, label='event')
#plt.show()
plt.savefig('station_pairs_PKP.pdf')
plt.close('all')

# Histogram distances
plt.hist(df["distance"],bins=180)
plt.title('Histogram of station pairs distance \n Total : %d'%len(df["station_A"]))
#plt.show()
plt.savefig('station_histo_PKP.pdf')
plt.close('all')






