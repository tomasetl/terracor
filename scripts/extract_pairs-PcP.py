#!/usr/bin/env python
# coding: utf-8

# # Check dataset
# Libraries
# Anaconda Libraries
import sys,os,glob,time,copy,warnings,cProfile,shutil,matplotlib
from IPython import display
import h5py as h5
import numpy as np
import scipy.fftpack
import scipy.signal
import scipy.io as io
import skimage.restoration as deconv
from math import cos, sin, radians
import matplotlib
import matplotlib.pyplot as plt
import matplotlib.pylab as pl
from matplotlib.colors import LinearSegmentedColormap
from matplotlib.transforms import offset_copy
import cartopy.crs as ccrs
from cartopy.io.img_tiles import Stamen, OSM
import cartopy.feature as cfeature
from obspy.geodetics.base import gps2dist_azimuth
from obspy.geodetics import locations2degrees
from obspy.taup import TauPyModel
from mpl_toolkits.axes_grid1 import make_axes_locatable, axes_size

from pyproj import Geod
import ipdb
import json
sys.path.append("/bettik/tomasetl/pycorr/v1.0")
import pandas as pd

# Own Libraries

import mypycorr.mods.dd as dd
import mypycorr.mods.lang as lang
import m_manage_data as m_mod

######################################################################
matplotlib.rcParams['pdf.fonttype'] = 42

## New Version
stations = "stations.txt"
events = "events.txt"

datacenter = []
net = []
sta = []
locid = []
lat = []
lon = []
alt = []
depth = []
id_sta = []

f = open(stations, 'r')
lines = f.readlines()

for l in lines:
    liste = l.split('    ')
    datacenter.append(liste[0])
    net.append(liste[1])
    sta.append(liste[2])
    if (liste[3] == '--'):
        id_sta.append('%s/%s/00'%(liste[1], liste[2]))
        locid.append('00')
    else:
         id_sta.append('%s/%s/%s'%(liste[1], liste[2], liste[3]))
         locid.append(liste[3])
    lat.append(float(liste[4]))
    lon.append(float(liste[5]))
    alt.append(float(liste[6]))
    depth.append(float(liste[7]))
    #id_sta.append('%s/%s/%s'%(liste[1], liste[2],liste[3]))
    
f.close()

ev = open(events)
for lev in ev.readlines():
    lat_ev = float(lev.split('   ')[1])
    lon_ev = float(lev.split('   ')[2])

## Plot available stations coordinates
plt.close('all')
m_mod.mmap(111,lon,lat,ch=None,xylabel=True,res=2)
plt.scatter(lon_ev, lat_ev, marker='*', c='g', s=500,transform=ccrs.PlateCarree(),cmap='plasma_r',alpha=0.5)
plt.savefig('station_all_PcP.pdf')
#plt.show()
plt.close('all')

## Get Ray parameter for PcP phases
model = TauPyModel(model="PREM")  # PREM model
ray_p_pcp=[]
vect_epi_dist =  np.linspace(0,90,181)  # distance vector each 0.5°
for dist in vect_epi_dist:
    #print(dist)
    arrivals = model.get_travel_times(source_depth_in_km=0,
                                  distance_in_degree=dist,
                                  phase_list=["PcP"])
    arr = arrivals[0]
    ray_p_pcp.append(arr.ray_param)  
    
    
ray_p_pcp = np.array(ray_p_pcp)  # PcP ray parameter matrix

## Get azimuth backazimuth and distance from each station to source
vec_dist  = np.zeros([len(id_sta),1])
vec_az    = np.zeros([len(id_sta),1])
vec_baz   = np.zeros([len(id_sta),1])

for counterA, staA in enumerate(id_sta):
    (vec_dist[counterA],vec_az[counterA],vec_baz[counterA]) = gps2dist_azimuth(lat_ev, lon_ev, lat[counterA], lon[counterA])
plt.close('all')

geoid = Geod(ellps="WGS84")  

## Check if station in PKP zone from source

ivec1 = vec_dist/1000/111.91 > 130  # in degrees
ivec2 = vec_dist/1000/111.91 < 180  # in degrees
valid_idxA = np.where(ivec1 * ivec2)[0]  # indexes for stations in PcP zone

## Distances to find PcP from station in PKP range (same ray parameter)
vect_dist_pcp = []
for idist in vec_dist[valid_idxA]:  # for each station in PKP range
    ray_to_find = model.get_travel_times(source_depth_in_km=0,
                                  distance_in_degree=idist/1000/111.91,
                                  phase_list=["PKIKP","PKP"])[0].ray_param  # find ray parameter 
    vect_dist_pcp.append(vect_epi_dist[np.argmin(np.abs(ray_p_pcp-ray_to_find))])  # select distance of closest ray parameter

term_lon,term_lat,term_baz = geoid.fwd(np.array(lon)[valid_idxA],np.array(lat)[valid_idxA], vec_baz[valid_idxA]+180.,np.array(vect_dist_pcp)*1000*111.91)  # get coordinates of expected PcP (distance calculated before, same azimuth as station n°1 

#plt.plot(vect_dist_pcp)
#plt.show()

## Plot stations in PKP range and expected PcP arrival points 

m_mod.mmap(111,np.array(lon)[valid_idxA],np.array(lat)[valid_idxA],ch=None,xylabel=True,res=2)
plt.scatter(term_lon,term_lat, marker='o', c='g', s=20,transform=ccrs.PlateCarree(),cmap='plasma_r',alpha=0.5, label='expected PcP points')
plt.title('Stations in PKP range and expected PcP points')
plt.legend()
#plt.show()
plt.savefig('station_proj_PcP.pdf')
plt.close('all')


mat_dist   = np.zeros([len(id_sta),len(valid_idxA)])  # distance between stations matrix
bool_dist  = np.zeros([len(id_sta),len(valid_idxA)])  # is the station pair selected ? matrix

for counterA, staA in enumerate(np.array(id_sta)[valid_idxA].tolist()):  # for stations in PKP range
    #ipdb.set_trace()
    az,baz,mat_dist[:,counterA] = geoid.inv(np.tile(term_lon[counterA],np.array(lon).size), np.tile(term_lat[counterA],np.array(lon).size), lon , lat)  # get distance from expected PcP points to other stations
    #print(staA)
    #print(staA[0:2])
    #if staA[0:2] in ["1K", "AU", "4J"]:
    bool_dist[:,counterA] = (mat_dist[:,counterA]/1000/111.91) < 5  # keep pair if distance < 25% source station distance
    #else:
    #    continue

## Select station pairs
valid_idxB = np.where(np.sum(bool_dist,1)>0)[0]  # select stations involved in pair, second station PKPPcP
valid_idxA = valid_idxA[np.where(np.sum(bool_dist,0)>0)[0]]  # select stations involved in pair, first station PKP


# List stations
valid_idx = np.unique(np.concatenate([valid_idxA, valid_idxB]))
stations = []
for idx in valid_idx :
    stations.append([datacenter[idx], net[idx], sta[idx], locid[idx], lat[idx], lon[idx], alt[idx] ,depth[idx]])

df_sta = pd.DataFrame(stations)
df_sta.to_csv("stations_list_PcP.txt", header = False, sep = "\t", index = False)

#List pairs
rows = []
for counterA in valid_idxA:
    id_A = id_sta[counterA].replace('/', '.')
    for counterB in valid_idxB:
        id_B = id_sta[counterB].replace('/','.')
        dist = geoid.line_length([lon[counterA],lon[counterB]], [lat[counterA], lat[counterB]])/1000/111.91
        rows.append([id_A, id_B,lat[counterA], lat[counterB], lon[counterA], lon[counterB], alt[counterA], alt[counterB], depth[counterA], depth[counterB], dist]) 

df = pd.DataFrame(rows, columns=["station_A", "station_B", "lat_A", "lat_B", "lon_A", "lon_B", "elev_A", "elev_B", "depth_A", "depth_B", "distance"])
df_pairs = df[["station_A", "station_B"]]
#rows.append([id_A, id_B,lat[counterA], lat[counterB], lon[counterA], lon[counterB], dist])
#df = pd.DataFrame(rows, columns=["station_A", "station_B", "lat_A", "lat_B", "lon_A", "lon_B", "distance"])
#df_pairs = df[["station_A", "station_B"]]

df.to_csv("pairs_PcP.txt", header=False, sep="\t", index=False)
df_pairs.to_csv("list_pairs_PcP.txt", header=False, sep="_", index=False)


#Figures
n = 180
col = pl.cm.inferno(np.linspace(0,1,n))

(lon_ev_antipod, lat_ev_antipod, baz_ev_antipod) = geoid.fwd(lon_ev, lat_ev, 0, 180*1000*111.91)
m_mod.mmap(111,np.array(lon)[valid_idxA],np.array(lat)[valid_idxA],ch=None,xylabel=True,res=2,proj='azi',ev=[lon_ev_antipod,lat_ev_antipod])
for i in range(len(df.station_A)):
            plt.plot([df["lon_A"][i],df["lon_B"][i]], [df["lat_A"][i], df["lat_B"][i]], color=col[int(df.distance[i])], linewidth=0.5,alpha=0.25, transform=ccrs.Geodetic()) # plot path along great circle, color depending on distance
plt.scatter(lon_ev, lat_ev, marker='*', c='g', s=500,transform=ccrs.PlateCarree(),cmap='plasma_r',alpha=0.5, label='event')
plt.scatter(np.array(lon)[valid_idxB],np.array(lat)[valid_idxB], marker='o', c='darkviolet', s=20,transform=ccrs.PlateCarree(),alpha=0.5)
plt.scatter(np.array(lon)[valid_idxA], np.array(lat)[valid_idxA], marker='o', c='red', s=20, transform=ccrs.PlateCarree(), alpha=0.5)
plt.title('selected station pairs')
#plt.show()
plt.savefig('station_pairs_PcP.pdf')
plt.close('all')

# Histogram distances
plt.hist(df.distance,bins=180)
plt.title("Histogram of station pairs distance \n Total : %d"%len(df["station_A"]))
#plt.show()
plt.savefig('station_histo_PcP.pdf')



