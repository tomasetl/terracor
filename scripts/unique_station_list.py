#/usr/bin/env python3

__author__ = "Lisa Tomasetto"
__copyright__ = "Copyright 2022, UGA"
__credits__ = ["Lisa Tomasetto"]
__version__ = "1.0"
__maintainer__ = "Lisa Tomasetto"
__email__ = "lisa.tomasetto@univ-grenoble-alpes.fr"
__status__ = "Production"

""" This scripts reads the stations list returned by the extract_pairs scripts and returns a station list in the same format where station occurence is unique"""
#######################################################################
##Librairies
import numpy as np
import sys, os,glob, copy
import pandas as pd


#######################################################################
# Station lists paths
df_P  = pd.read_csv("stations_list_P.txt", sep="\t", header= None, names = ["datacenter", "network", "station", "loc", "lat", "lon", "elev", "depth"], keep_default_na = False)
df_PKP  = pd.read_csv("stations_list_PKP.txt", sep="\t", header= None, names = ["datacenter", "network", "station", "loc", "lat", "lon", "elev", "depth"], keep_default_na = False)
df_PcP = pd.read_csv("stations_list_PcP.txt", sep="\t", header= None, names = ["datacenter", "network", "station", "loc", "lat", "lon", "elev", "depth"], keep_default_na = False)

df = pd.concat([df_P, df_PKP, df_PcP])
df = df.drop_duplicates(subset = "station")

print(df)
df.to_csv("stations_list.txt", sep= "\t", header = False, index = None)
