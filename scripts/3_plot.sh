#!/bin/bash

#OAR -n 3_plot
#OAR -l /core=1, walltime=08:00:00
#OAR --stdout plot.out
#OAR --stderr plot.err
#OAR --project global

cd /summer/nocoda/tomasetl/python/

source ~/.bashrc

conda activate mypy3

python3 05_plots.py P ./C1_04_xcorr_P___coher
python3 05_plots.py PKP ./C1_04_xcorr_PKP___coher
python3 05_plots.py PcP ./C1_04_xcorr_PcP___coher
