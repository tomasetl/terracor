# terracor

#### What is the motivation of this project ?
Project to find body waves from microseismic event to:
- reduce P-waves ambiguities in retrieval
- improve the illumination problem
- use the secondary microseismic peak since it shows high energy
- use secondary microseismic P-wave noise source model to retrieve P-waves using interferometry.

## Recommanded install
**for MacOSX**

`wget https://repo.anaconda.com/miniconda/Miniconda3-latest-MacOSX-x86_64.sh -O ~/miniconda.sh`

**for Linux**

`wget https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh -O ~/miniconda.sh`

`bash ~/miniconda.sh`  
`source .bashrc`  

### Create the conda environnement with needed packages
`conda create -n mypy3 python=3.8`  
`conda update -n base -c defaults conda`  
`conda activate mypy3`  
`conda install numpy scipy h5py`  
`conda install matplotlib cartopy pillow`  
`conda install conda install -c conda-forge obspy`  
`conda install pandas pyproj`  
