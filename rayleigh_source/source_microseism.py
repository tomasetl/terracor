#!/usr/bin/env python3

__author__ = "Lisa Tomasetto"
__copyright__ = "Copyright 2022, UGA"
__credits__ = ["Lisa Tomasetto"]
__version__ = "1.0"
__maintainer__ = "Lisa Tomasetto"
__email__ = "lisa.tomasetto@univ-grenoble-alpes.fr"
__status__ = "Production"

"""This program aims at modelizing the ambient noise source in the secondary microseismic range for Rayleigh waves. Using Longuet-Higgins site effect and F.Ardhuin WW3 model."""
##################################################################################
##Libraries
from calendar import monthrange
import matplotlib.pyplot as plt
import matplotlib as mpl
import numpy as np
import cartopy.crs as ccrs
from cartopy.mpl.gridliner import LONGITUDE_FORMATTER, LATITUDE_FORMATTER
import matplotlib.ticker as mticker
from obspy.geodetics.base import gps2dist_azimuth
from datetime import datetime
import pandas as pd
import wget
import os.path

##Own librairies
from read_hs_p2l import read_WWNC, read_WWNCf
from parameters import *
from readWW31 import read_dpt


###

def site_effect(z, f, vs_crust, path):
    """ Bathymetry secondary microseismic excitation coefficients (Rayleigh waves).
    Input:
    z : thickness of water layer
    f: seismic frequency in hertz
    vs_crust : shear waves velocity in the crust (sea bed)
      default value is 2.8 km/s
    path : the path to the Longuet Higgins file containing tabulated values of site effect coefficient for the 4th first modes.

    Output :
    Numerical value of the site effect in the shape (f, z)."""

    df = pd.read_csv('%s'%path, sep='\t', header =0, usecols=[0, 1, 2, 3, 4, 5, 6, 7], names = ['fh1', 'c1', 'fh2', 'c2', 'fh3', 'c3', 'fh4', 'c4'])
    x1 = np.linspace(np.min(df.fh1), np.max(df.fh1), 1000) 
    x2 = np.linspace(np.min(df.fh2), np.max(df.fh2), 1000) 
    x3 = np.linspace(np.min(df.fh3), np.max(df.fh3), 1000) 
    x4 = np.linspace(np.min(df.fh4), np.max(df.fh4), 1000) 
    c1 = np.interp(x1, df.fh1, df.c1)
    c2 = np.interp(x2, df.fh2, df.c2)
    c3 = np.interp(x3, df.fh3, df.c3)
    c4 = np.interp(x4, df.fh4, df.c4)
    try :
        n = len(f)
        x = z.shape[1]
        y = z.shape[0]
        #print(n, x, y)
        #print(z.shape)
        C = np.empty((x, y, n))
        #print(C.shape)
        for k in range(x):  # 0--> 719
            for l in range(y):  # 0 --> 317
                h = z[l, k]
                if h != h:
                    C[k, l, :] = np.nan
                else:
                    c = 0
                    for i, fq in enumerate(f):
                        fh_v = 2*np.pi*fq*h/(vs_crust*1e3)
                        ind1 = np.argmin(abs(fh_v - x1))
                        ind2 = np.argmin(abs(fh_v - x2))
                        ind3 = np.argmin(abs(fh_v - x3))
                        ind4 = np.argmin(abs(fh_v - x4))
                        #print(ind1, ind2, ind3, ind4)
                        c = c1[ind1]**2 + c2[ind2]**2 + c3[ind3]**2 + c4[ind4]**2 
                        C[k, l, i] = c
    except:
        raise
    return np.squeeze(C)

def main(file_bathy, fpath, path, lat_min, lat_max, lon_min, lon_max, U, key, YEAR, MONTH, DAY, HOUR):
    """This function calculate all the info about rayleigh source of microseisms"""
    ## Constants
    # seismic waves
    vs_crust = 2.8  # km/s
    rho_s = 2830  # kg/m3
    res_mod = radians(0.5)  # angular resolution of the model
    radius = 6.371*1e6
    lg10 = log(10)

    Q = {'KIP': 580, 'BORG': 180, 'BKS': 88, 'SSB': 260}
    """attenuation factor used in Ardhuin et al. (2011) for 
    KIP (Hawaii) few reflections old crust,
    BORG (Iceland) few reflection young crust,
    BKS (California USA),
    SSB (France) respectively"""

    ## Geometrical properties of the bathymetry grid
    [zlat, zlon, dpt1, var] = read_dpt(file_bathy)
    ind_lat = np.squeeze(np.argwhere((zlat > lat_min)&(zlat < lat_max)))
    ind_lon = np.squeeze(np.argwhere((zlon > lon_min)&(zlon < lon_max)))
    ny, nx = len(ind_lat), len(ind_lon)
    X, Y = np.meshgrid(zlon, zlat, indexing='xy')
    
    # calculate spherical surface elementary elements
    msin = np.array([np.sin(np.pi/2 - np.radians(zlat))]).T
    ones = np.ones((1, len(zlon)))
    dA = radius**2*res_mod**2*np.dot(msin,ones)
    
    # Useful matrices
    distance_in_meters = np.empty((len(zlat), len(zlon)))
    distance = np.empty((len(zlat), len(zlon)))
    az = np.empty((len(zlat), len(zlon)))
    baz = np.empty((len(zlat), len(zlon)))

    # Azimuthal distribution
    azimuth = np.arange(0, 360, azimuth_step)
    index_az = np.empty((len(zlat), len(zlon)))
    for i in range(len(zlat)):
        for j in range(len(zlon)):
            distance_in_meters[i,j], az[i,j], baz[i,j] = gps2dist_azimuth(lat_sta, lon_sta, Y[i,j], X[i, j])
            distance[i, j] = distance_in_meters[i,j]*180/(np.pi*radius)
            index_az[i, j] = np.argmin(abs(azimuth-az[ i, j]))
            
    # Attenuation Factor
    Qsta = Q[key]
    DF = [] ## future dataframe will be filled iteratively

    # Values not frequency or time dependent
    
    index = np.squeeze(np.argwhere(np.radians(distance)==0))
    denominateur = 1/(radius*np.sin(np.radians(distance)))
    try:
        denominateur[index[0], index[1]] = 0.0
    except:
        print('no exception')
    
    for iyear in np.array([YEAR]):
        if isinstance(MONTH, int):
            MONTH = np.array([MONTH])
        elif not len(MONTH):
            MONTH = np.arange(1, 13)
        else:
            MONTH = np.array(MONTH)
            for imonth in MONTH:
                daymax = monthrange(iyear,imonth)[1]
                if imonth < 10:
                    filepath = '%sWW3-GLOB-30M_%d0%d'%(fpath, iyear, imonth)
                else:
                    filepath = '%sWW3-GLOB-30M_%d%d'%(fpath, iyear, imonth)
                filename_p2l = '%s_p2l.nc'%(filepath)
                print(filename_p2l)
                try:
                    day = np.array(DAY)
                    if day[0] > day[-1]:
                        index = np.squeeze(np.argwhere(day==daymax))
                        if imonth == MONTH[0]:
                            day = day[:index+1]
                        elif imonth == MONTH[-1]:
                            index = np.squeeze(np.argwhere(day==monthrange(iyear, imonth-1)[1]))
                            day = day[index+1:]
                        else:
                            day = np.arange(1,daymax+1)
                except:
                    try:
                        day = int(np.squeeze(day))
                    except:
                        day= np.arange(1,(monthrange(iyear,imonth)[1])+1)
                for iday in day:
                    if isinstance(HOUR, int):
                        HOUR = np.array([HOUR])
                    elif not len(HOUR):
                        HOUR = np.arange(0,24,3)
                    else:
                        HOUR = np.array(HOUR)
                    for ih in HOUR:
                        # Load Fp
                        (lat, lon, freq, p2l, unit1, scale) = read_WWNCf(filename_p2l, [iyear, imonth, iday, ih], [], [])

                        # Frequency 
                        n_freq = len(freq)  # number of frequencies
                        xfr = np.exp(np.log(freq[-1]/freq[0])/(n_freq-1))  # determines the xfr geometric progression factor
                        df = freq*0.5*(xfr-1/xfr)  # frequency interval in wave model times 2
                        freq = 2*freq  #ocean to seismic waves frequency
                        
                        if unit1 == 'log10(Pa2 m2 s+1E-12':
                            p2l = np.exp(lg10*p2l)  - (1e-12-1e-16)
                        elif unit1 == 'log10(m4s+0.01':
                            p2l = np.exp(lg10*p2l) - 0.009999
                        elif unit1 == 'log10(Pa2 m2 s+1E-12)':
                            p2l = np.exp(lg10*p2l)  - (1e-12-1e-16)
                        else:
                            p2l = np.exp(lg10*p2l)
                        
                        SDF_freq = np.empty((len(dpt1), len(dpt1[0]), n_freq))
                        F_delta = np.empty((len(dpt1), len(dpt1[0]), n_freq))
                        source_spectra = np.empty((len(azimuth), n_freq)) 
                        Fp = p2l
                        if YEAR > 2017:
                                Fp = Fp[:, :-6, :]
                        C = site_effect(dpt1, freq, vs_crust, path)
                        MAT = Fp*C
                        for i, f in enumerate(freq):
                            SDF_freq[:, :, i] = 2*np.pi/(rho_s**2*(vs_crust*1e3)**5)*f*MAT[:, :, i].T
                            EXP = np.exp(-2*np.pi*f*np.radians(distance)*radius/(U*Qsta))
                            F_delta[ :, :, i] = denominateur*SDF_freq[:, :, i]*EXP*dA
                            for j in range(len(azimuth)):
                                index = np.where(index_az==j)
                                #print(index)
                                SELECT = F_delta[index[0], index[1], i]
                                source_spectra[j, i] = np.sum(SELECT)
                        SDF_freq[SDF_freq == None] = np.nan
                        F_delta = np.nan_to_num(F_delta)
                        SQRT = np.sqrt(np.nan_to_num(F_delta))
                        disp_RMS = 10*np.log(np.sum(SQRT, axis = (0,1)))
                        date = datetime(iyear, imonth, iday, ih, 0)
                        dict1 = {}
                        new_item = {'date': date, 'fq': freq, 'source_global': SDF_freq, 'source_local': F_delta, 'disp': disp_RMS, 'source_spectra': source_spectra}
                        dict1.update(new_item)
                        DF.append(dict1)
    df = pd.DataFrame(DF)
    df.to_json(path_or_buf = "./data%s_%s_%s.json"%(iyear, imonth, iday), orient = "columns")
    print("DONE ! Filename: data%s_%s_%s.json"%(iyear, imonth, iday))
    return df

def plot_spectrogram(date, freq, disp):
    """ this function plots the modelled spectrogram at the given station. It reads the information from a dataframe calculated by the main function"""
    plt.close("all")
    plt.figure(figsize=(20,10))
    plt.gcf().autofmt_xdate()
    plt.xlabel('Date ')
    plt.ylabel('Frequency (Hz)')
    im = plt.pcolor(date, freq, disp.T, cmap='jet', shading='auto')
    c = plt.colorbar(im)
    c.ax.set_title('F_delta (dB)')
    plt.show()

def plot_global_source(freq, date, zlon, zlat, df, lon_min = -180, lon_max = 180, lat_min = -90, lat_max = 90):
    freqrange = np.array((df["fq"].to_numpy())[0])
    print(np.argmin(freqrange-freq))
    exit()
    index = np.argmin(freqrange-freq)
    SDF = np.array(df[df["date"]==date]["source_global"][0])[:, :, index]
    SDF = SDF.astype('float64')
    plt.figure(figsize = (15, 8))
    ax = plt.axes(projection = ccrs.PlateCarree())
    ax.coastlines()
    im = ax.pcolor(zlon, zlat, SDF, cmap='jet')
    ax.set_title('Source of the power spectrum for the vertical displacement.\n Rayleigh waves. Frequency %.3f Hz\n %s'%(freq, date))
    plt.colorbar(im, label = 'SDF (m.s)', orientation = 'horizontal', ax=ax, shading='auto')
    ax.set_extent((lon_min, lon_max, lat_min, lat_max))
    gl = ax.gridlines(crs=ccrs.PlateCarree(), draw_labels=True, linewidth=1, color='gray', alpha=0.5, linestyle='--')
    plt.show()
    #plt.savefig('SDF_rayleigh_%d_%d_%d_%d.png'%(iyear, imonth, iday, ih))

def plot_local_source(freq, date, lat_sta, lon_sta, zlon, zlat, df, lon_min = -180, lon_max = 180, lat_min = -90, lat_max = 90):
    freqrange = np.array((df["fq"].to_numpy())[0])
    index = np.argmin(freqrange-freq)
    dad = np.array(df[df["date"]==date]["source_local"][0])[:, :, index]
    dad = dad.astype('float64')
    # F_delta in meters square
    plt.figure(figsize = (15, 8))
    ax = plt.axes(projection = ccrs.NearsidePerspective(central_longitude=lon_sta, central_latitude=lat_sta))
    ax.coastlines()
    im = ax.pcolor(zlon, zlat, dad, cmap='jet', transform = ccrs.PlateCarree())
    ax.set_title('Contribution of sources to power spectrum of vertical displacement at station.\n Rayleigh waves. Frequency %.3f Hz \n %s'%(freq, date))
    plt.colorbar(im, label = '', orientation = 'horizontal', ax=ax, shading='auto')
    ax.plot(lon_sta, lat_sta, 'r+', transform = ccrs.PlateCarree(), label='Station')
    ax.set_extent((lon_min, lon_max, lat_min, lat_max))
    gl = ax.gridlines(crs=ccrs.PlateCarree(), draw_labels=True, linewidth=1, color='gray', alpha=0.5, linestyle='--')
    plt.show()
    #plt.savefig('rayleigh_%d-%d-%d-%d'%(iyear, imonth, iday, ih))
    

if __name__ == "__main__":
    from parameters import *
    jsonpath="data2020_1_1.json"
    if os.path.isdir(jsonpath):  
        print("\nIt is a directory")  
    elif os.path.isfile(jsonpath):  
        print("File already there !")
        df = pd.read_json(path_or_buf = jsonpath, orient = "columns")
    else:
        print("json file not calculated ... Let's do it")
        print("It takes some time")
        df = main(file_bathy, fpath, path, lat_min, lat_max, lon_min, lon_max, U, key, YEAR, MONTH, DAY, HOUR)
    print(df.keys())
    zlon = np.arange(-180, 180, 0.5)
    zlat = np.arange(-78, 80.5, 0.5)
    date = df["date"].to_numpy()
    freq = (df["fq"].to_numpy())[0] #np.squeeze(df["fq"].to_numpy())[0]
    disp = np.stack(np.squeeze(df["disp"].to_numpy()))
    time = date[0]
    print(time)
    
    f = 0.15
    
    plot_spectrogram(date, freq, disp)
    plot_global_source(f, time, zlon, zlat, df, lon_min = -180, lon_max = 180, lat_min = -90, lat_max = 90)
    plot_local_source(f, time, lat_sta, lon_sta, zlon, zlat, df, lon_min = -180, lon_max = 180, lat_min = -90, lat_max = 90)
