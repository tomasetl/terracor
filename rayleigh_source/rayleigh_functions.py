#!/usr/bin/env python3

__author__ = "Lisa Tomasetto"
__copyright__ = "Copyright 2022, UGA"
__credits__ = ["Lisa Tomasetto"]
__version__ = "1.0"
__maintainer__ = "Lisa Tomasetto"
__email__ = "lisa.tomasetto@univ-grenoble-alpes.fr"
__status__ = "Production"

##Libraries
from calendar import monthrange
import matplotlib.pyplot as plt
import matplotlib as mpl
import numpy as np
import cartopy.crs as ccrs
from cartopy.mpl.gridliner import LONGITUDE_FORMATTER, LATITUDE_FORMATTER
import matplotlib.ticker as mticker
from obspy.geodetics.base import gps2dist_azimuth
from datetime import datetime
import pandas as pd
from math import radians, log
#import wget
#import os.path

##Own librairies
from read_hs_p2l import read_WWNC, read_WWNCf
from readWW31 import read_dpt



def site_effect(z, f, vs_crust, path):
    """ Bathymetry secondary microseismic excitation coefficients (Rayleigh waves).
    Input:
    z : thickness of water layer
    f: seismic frequency in hertz
    vs_crust : shear waves velocity in the crust (sea bed)
      default value is 2.8 km/s
    path : the path to the Longuet Higgins file containing tabulated values of site effect coefficient for the 4th first modes.

    Output :
    Numerical value of the site effect in the shape (f, z)."""

    df = pd.read_csv('%s'%path, sep='\t', header =0, usecols=[0, 1, 2, 3, 4, 5, 6, 7], names = ['fh1', 'c1', 'fh2', 'c2', 'fh3', 'c3', 'fh4', 'c4'])
    x1 = np.linspace(np.min(df.fh1), np.max(df.fh1), 1000) 
    x2 = np.linspace(np.min(df.fh2), np.max(df.fh2), 1000) 
    x3 = np.linspace(np.min(df.fh3), np.max(df.fh3), 1000) 
    x4 = np.linspace(np.min(df.fh4), np.max(df.fh4), 1000) 
    c1 = np.interp(x1, df.fh1, df.c1)
    c2 = np.interp(x2, df.fh2, df.c2)
    c3 = np.interp(x3, df.fh3, df.c3)
    c4 = np.interp(x4, df.fh4, df.c4)
    try :
        n = len(f)
        x = z.shape[1]
        y = z.shape[0]
        #print(n, x, y)
        #print(z.shape)
        C = np.empty((x, y, n))
        #print(C.shape)
        for k in range(x):  # 0--> 719
            for l in range(y):  # 0 --> 317
                h = z[l, k]
                if h != h:
                    C[k, l, :] = np.nan
                else:
                    c = 0
                    for i, fq in enumerate(f):
                        fh_v = 2*np.pi*fq*h/(vs_crust*1e3)
                        ind1 = np.argmin(abs(fh_v - x1))
                        ind2 = np.argmin(abs(fh_v - x2))
                        ind3 = np.argmin(abs(fh_v - x3))
                        ind4 = np.argmin(abs(fh_v - x4))
                        #print(ind1, ind2, ind3, ind4)
                        c = c1[ind1]**2 + c2[ind2]**2 + c3[ind3]**2 + c4[ind4]**2 
                        C[k, l, i] = c
    except:
        raise
    return np.squeeze(C)

def loop_azimuthal(YEAR, MONTH, DAY, HOUR, fpath, path, f1, f2, vs_crust, rho_s, U, Qsta, res_mod, radius, lg10, dist, distance, azimuth, dpt1, X, ind_lat, ind_lon, index_az, index_dist, plot):
    #url = "ftp://ftp.ifremer.fr/ifremer/ww3/HINDCAST/SISMO/GLOBAL05_%d_REF102040/"
    ## Initialize variables
    L = []  ## Result of azimuthal contribution (dictionnary)
    M = np.empty((len(azimuth), len(dist)))  ## distribution of azimuthal contribution as a function of distance
    ## Loop over dates
    for iyear in np.array([YEAR]):
        if isinstance(MONTH, int):
            MONTH = np.array([MONTH])
        elif not len(MONTH):
            MONTH = np.arange(1, 13)
        else:
            MONTH = np.array(MONTH)
        for imonth in MONTH:
            daymax = monthrange(iyear,imonth)[1]
            if imonth < 10:
                filepath = '%sWW3-GLOB-30M_%d0%d'%(fpath, iyear, imonth)  # path to wave model files
            else:
                filepath = '%sWW3-GLOB-30M_%d%d'%(fpath, iyear, imonth)  # path to wave model files
            filename_p2l = '%s_p2l.nc'%(filepath)
            print(filename_p2l)
            try:
                day = np.array(DAY)
                if day[0] > day[-1]:
                    index = np.squeeze(np.argwhere(day==daymax))
                    if imonth == MONTH[0]:
                        day = day[:index+1]
                    elif imonth == MONTH[-1]:
                        index = np.squeeze(np.argwhere(day==monthrange(iyear, imonth-1)[1]))
                        day = day[index+1:]
                    else:
                        day = np.arange(1,daymax+1)
            except:
                try:
                    day = int(np.squeeze(day))
                except:
                    day= np.arange(1,(monthrange(iyear,imonth)[1])+1)
            for iday in day:
                if isinstance(HOUR, int):
                    HOUR = np.array([HOUR])
                elif not len(HOUR):
                    HOUR = np.arange(0,24,3)
                else:
                    HOUR = np.array(HOUR)
                for ih in HOUR:
                    #if not os.path(filename_p2l):
                    #    url = url+"WW3-GLOB-30M_%d%d"%(iyear, imonth)
                    #    print(url)
                    #    wget.download(url, fpath)
                    ## Open F_p3D
                    (lat, lon, freq, p2l, unit1, scale) = read_WWNCf(filename_p2l, [iyear, imonth, iday, ih], [], [])
                    nf = len(freq)
                    xfr = np.exp(np.log(freq[-1]/freq[0])/(nf-1))  # determines the xfr geometric progression factor
                    df = freq*0.5*(xfr-1/xfr)  # frequency interval in wave model times 2
                    freq = 2*freq  #ocean to seismic waves freq
                    
                    ## Check units of the model, depends on version 
                    if unit1 == 'log10(Pa2 m2 s+1E-12':
                        p2l = np.exp(lg10*p2l)  - (1e-12-1e-16)
                    elif unit1 == 'log10(m4s+0.01':
                        p2l = np.exp(lg10*p2l) - 0.009999
                    elif unit1 == 'log10(Pa2 m2 s+1E-12)':
                        p2l = np.exp(lg10*p2l)  - (1e-12-1e-16)
                    print(unit1)

                    ## Frequency band
                    if f1 < f2:
                        summ = np.zeros(dpt1.shape)
                        index_freq = np.squeeze(np.argwhere((f1<=freq)&(freq<=f2)))
                        n_freq = len(index_freq)
                        df = df[index_freq]
                        freq = freq[index_freq]
                        
                        SDF_freq = np.zeros((len(dpt1), len(dpt1[0]), n_freq))
                        SDF_int = np.zeros((len(dpt1), len(dpt1[0]), n_freq))
                        
                        daily_azi_distrib = np.empty((len(dpt1), len(dpt1[0]), n_freq))
                        integrate= np.empty((len(dpt1), len(dpt1[0]), n_freq))

                        Fp = p2l[:, :, index_freq]  # F_p in Pa2.m2.s
                        if YEAR > 2017:
                                Fp = Fp[:, :-6, :]
                        C = site_effect(dpt1, freq, vs_crust, path)  # Compute Longuet-Higgins site effect
                        MAT = Fp*C
                        print("MAT")
                        ## Calculus of SDF and F_delta, integration over frequency band
                        for i, f in enumerate(freq):

                            SDF_freq[:, :, i] = 2*np.pi/(rho_s**2*(vs_crust*1e3)**5)*f*MAT[:,:,i].T # SDF in m.s
                            SDF_int[:, :, i] = SDF_freq[:, :, i]*df[i]  # SDF times df in m
                            EXP = np.exp(-2*np.pi*f*np.radians(distance)*radius/(U*Qsta))
                            area = radius**2*np.sin(np.radians(X))*res_mod**2
                            index = np.squeeze(np.argwhere(np.radians(distance)==0))
                            denominateur = 1/(radius*np.sin(np.radians(distance)))
                            denominateur[index[0], index[1]] = 0.0
                            
                            daily_azi_distrib[ :, :, i] = denominateur*SDF_freq[:, :, i]*EXP*area # F_delta in m2.S
                            integrate[:, :, i] = daily_azi_distrib[:, : , i]*df[i]  # F_delta times df in m2
                        ## Integrate over frequency
                        SDF = np.sum(SDF_int, axis=2)
                        SDF = SDF[ind_lat, :]
                        SDF= SDF[:, ind_lon]
                        dad = abs(np.sum(integrate, axis=2))
                        
                    ## Single Frequency
                    elif f1 == f2:
                        index_freq = np.squeeze(np.argmin(abs(freq-f1)))
                        print('unique frequency ', f1)
                        Fp = p2l[:, :, index_freq].T
                        if YEAR > 2017:
                            Fp = Fp[:-6, :]
                        C = site_effect(dpt1, f1, vs_crust, path)
                        C = C.reshape(len(dpt1), len(dpt1[0]))
                        SDF = 2*np.pi*f1/(rho_s**2*(vs_crust*1e3)**5)*Fp.T*C
                        EXP = np.exp(-2*np.pi*f1*np.radians(distance)*radius/(U*Qsta))
                        area = radius**2*np.sin(np.radians(X))*res_mod**2
                        index = np.squeeze(np.argwhere(np.radians(distance)==0))
                        denominateur = 1/(radius*np.sin(np.radians(distance)))
                        denominateur[index[0], index[1]] = 0.0
                        dad = abs(denominateur*SDF*EXP*area)
                        SDF = SDF[ind_lat, :]
                        SDF = SDF[:, ind_lon]
                        
                    ## If frequency mistake    
                    else:
                        print('two frequencies with f2 < f1 were given')
                        exit()
                    
                    ## Plot for SDF and F_delta at each step
                    if plot:
                        # SDF in meters
                        plt.figure(figsize = (15, 8))
                        ax = plt.axes(projection = ccrs.PlateCarree())
                        ax.coastlines()
                        im = ax.pcolor(zlon[ind_lon], zlat[ind_lat], SDF, shading='auto', cmap='jet')
                        ax.set_title('Source of the power spectrum for the vertical displacement.\n Rayleigh waves. Frequency %.3f-%.3f Hz\n %d-%d-%dT%.2f'%(f1, f2, iyear, imonth, iday, ih))
                        plt.colorbar(im, label = 'SDF (m)', orientation = 'horizontal', ax=ax)
                        ax.set_extent((lon_min, lon_max, lat_min, lat_max))
                        gl = ax.gridlines(crs=ccrs.PlateCarree(), draw_labels=True, linewidth=1, color='gray', alpha=0.5, linestyle='--')
                        #plt.show()
                        plt.savefig('SDF_rayleigh_%d_%d_%d_%d.png'%(iyear, imonth, iday, ih))
                        
                        # F_delta in meters square
                        plt.figure(figsize = (15, 8))
                        ax = plt.axes(projection = ccrs.AzimuthalEquidistant(central_longitude=lon_sta, central_latitude=lat_sta))
                        ax.coastlines()
                        im = ax.pcolor(zlon, zlat, dad, shading='auto', cmap='jet', transform = ccrs.PlateCarree())
                        ax.set_title('Contribution of sources to power spectrum of vertical displacement at station.\n Rayleigh waves. Frequency %.3f-%.3f Hz\n %d-%d-%dT%.2f'%(f1, f2, iyear, imonth, iday, ih))
                        plt.colorbar(im, label = '', orientation = 'horizontal', ax=ax)
                        ax.plot(lon_sta, lat_sta, 'r+', transform = ccrs.PlateCarree(), label='Station')
                        #ax.set_extent((lon_min, lon_max, lat_min, lat_max))
                        gl = ax.gridlines(crs=ccrs.PlateCarree(), draw_labels=True, linewidth=1, color='gray', alpha=0.5, linestyle='--')
                        #plt.show()
                        plt.savefig('rayleigh_%d-%d-%d-%d'%(iyear, imonth, iday, ih))
                        
                    ## Store info in dictionnary per azimuth and distance range
                    for i in range(len(dist)):
                        for j in range(len(azimuth)):
                            index = np.logical_and(index_az==j,  index_dist==i)
                            dad = np.nan_to_num(dad)
                            SELECT = dad[index]
                            M[j, i]  = np.sum(SELECT)
                    AZ = np.sum(M, axis=1)
                    date = datetime(iyear, imonth, iday, ih, 0)
                    dict1 = {}
                    new_item = {'date': date, 'M': M, 'AZ': AZ}
                    dict1.update(new_item)
                    L.append(dict1)
    return L