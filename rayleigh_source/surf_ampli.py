#!/usr/bin/env python3

__author__ = "Lisa Tomasetto"
__copyright__ = "Copyright 2022, UGA"
__credits__ = ["Lisa Tomasetto"]
__version__ = "1.0"
__maintainer__ = "Lisa Tomasetto"
__email__ = "lisa.tomasetto@univ-grenoble-alpes.fr"
__status__ = "Production"

""" This script calculates the site effect of microseisms for Rayleigh waves depending on the frequency and the water depth. Based on Longuet-Higgins (1950) values. (cf Table 1 p.32)"""

import math
import numpy as np
from numpy.lib.scimath import sqrt as csqrt
import matplotlib.pyplot as plt
import pandas as pd
from readWW31 import read_dpt
import cartopy.crs as ccrs

def site_effect(z, f, vs_crust, path):
    """ Bathymetry secondary microseismic excitation coefficients (Rayleigh waves).
    Input:
    z : thickness of water layer
    f: seismic frequency in hertz
    vs_crust : shear waves velocity in the crust (sea bed)
      default value is 2.8 km/s
    path : the path to the Longuet Higgins file containing tabulated values of site effect coefficient for the 4th first modes.

    Output :
    Numerical value of the site effect in the shape (f, z)."""

    df = pd.read_csv('%s'%path, sep='\t', header =0, usecols=[0, 1, 2, 3, 4, 5, 6, 7], names = ['fh1', 'c1', 'fh2', 'c2', 'fh3', 'c3', 'fh4', 'c4'])
    x1 = np.linspace(np.min(df.fh1), np.max(df.fh1), 1000) 
    x2 = np.linspace(np.min(df.fh2), np.max(df.fh2), 1000) 
    x3 = np.linspace(np.min(df.fh3), np.max(df.fh3), 1000) 
    x4 = np.linspace(np.min(df.fh4), np.max(df.fh4), 1000) 
    c1 = np.interp(x1, df.fh1, df.c1)
    c2 = np.interp(x2, df.fh2, df.c2)
    c3 = np.interp(x3, df.fh3, df.c3)
    c4 = np.interp(x4, df.fh4, df.c4)
    try :
        n = len(f)
        x = z.shape[1]
        y = z.shape[0]
        #print(n, x, y)
        #print(z.shape)
        C = np.empty((x, y, n))
        #print(C.shape)
        for k in range(x):  # 0--> 719
            for l in range(y):  # 0 --> 317
                h = z[l, k]
                if h != h:
                    C[k, l, :] = np.nan
                else:
                    c = 0
                    for i, fq in enumerate(f):
                        fh_v = 2*np.pi*fq*h/(vs_crust*1e3)
                        ind1 = np.argmin(abs(fh_v - x1))
                        ind2 = np.argmin(abs(fh_v - x2))
                        ind3 = np.argmin(abs(fh_v - x3))
                        ind4 = np.argmin(abs(fh_v - x4))
                        #print(ind1, ind2, ind3, ind4)
                        c = c1[ind1]**2 + c2[ind2]**2 + c3[ind3]**2 + c4[ind4]**2 
                        C[k, l, i] = c
    except:
        raise
    return np.squeeze(C)


if __name__ == "__main__":
    f = [1/12, 1/11, 1/10, 1/9, 1/8, 1/7, 1/6, 1/5, 1/4, 1/3]
    file_bathy = 'ww3.07121700.dpt'
    [zlat, zlon, dpt1, var] = read_dpt(file_bathy)
    vs_crust = 2.8
    path = './longuet_higgins.txt'
    C = site_effect(dpt1, f, vs_crust, path)
    for i in range(len(f)):
        plt.figure(figsize=(15,8))
        ax = plt.axes(projection=ccrs.PlateCarree())
        ax.coastlines()
        im = ax.pcolor(zlon, zlat, C[:, :, i].T, shading='auto', cmap='jet')
        ax.set_title('Rayleigh site effect period %d sec' %(1/f[i]))
        plt.colorbar(im)
        #plt.show()
        plt.savefig('site_effect_%dS.png'%(1/f[i]), dpi=80)

    

    
